import { UPDATE_DISCUSSION } from '../actions/currentDiscussion';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_DISCUSSION: {
      return Object.assign({}, state, action.discussion);
    }
    default:
      return state;
  }
}
