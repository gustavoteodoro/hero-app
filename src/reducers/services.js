import { UPDATE_SERVICES } from '../actions/services';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_SERVICES: {
      return action.services;
    }
    default:
      return state;
  }
}
