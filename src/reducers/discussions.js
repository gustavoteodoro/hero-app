import { UPDATE_DISCUSSIONS } from '../actions/discussions';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_DISCUSSIONS: {
      return action.discussions;
    }
    default:
      return state;
  }
}
