import { SET_EMAIL, CLEAR_EMAIL } from '../actions/authData';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case SET_EMAIL: {
      return Object.assign({}, state, action.email);
    }
    case CLEAR_EMAIL: {
      return {};
    }
    default:
      return state;
  }
}
