import { UPDATE_CAMPUS } from '../actions/campus';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_CAMPUS: {
      return action.campus;
    }
    default:
      return state;
  }
}
