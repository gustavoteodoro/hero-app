import { UPDATE_UNIT } from '../actions/currentUnit';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_UNIT: {
      return action.unit;
    }
    default:
      return state;
  }
}
