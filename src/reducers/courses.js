import { UPDATE_COURSES } from '../actions/courses';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_COURSES: {
      return action.courses;
    }
    default:
      return state;
  }
}
