import React, { Component } from 'react';
import { ActivityIndicator, BackHandler } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ListSortedDiscussionsByUnit } from '../../../services/discussions';
import { getUnit } from '../../../services/courses';
import { updateCurrentDiscussion } from '../../../actions/currentDiscussion';
import { updateCurrentUnit } from '../../../actions/currentUnit';
import UnitHeader from '../../modules/UnitHeader';
import FeedItem from '../../elements/FeedItem';
import CallToInput from '../../elements/CallToInput';
import { HeroIcon } from '../../../styles/fonts';
import Button from '../../elements/Button';

import {
  ThreadContainer,
  ThreadContentWrap,
  ThreadContent,
  FilterContent,
  FilterItem,
  FilterText,
  FeedHeaderFilter,
  UnitContainer,
  LoadingContainer,
} from './styles';

import {
  FeedClean,
  FeedCleanText,
} from '../Home/styles';

import { feedCleanText } from './data.json';

class Unit extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      state: PropTypes.object,
      goBack: PropTypes.func,
    }),
    currentUnit: PropTypes.shape({
      uuid: PropTypes.string,
    }),
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
    updateCurrentDiscussion: PropTypes.func,
    updateCurrentUnit: PropTypes.func,
  }

  static defaultProps = {
    navigation: null,
    currentUnit: null,
    user: null,
    updateCurrentDiscussion: null,
    updateCurrentUnit: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      unfollowed: false,
      isFire: false,
      currentUnitDiscussions: [],
      loadingDiscussions: false,
      headerMinified: false,
      refreshing: false,
      loadingStars: false,
    };

    this.handleScroll = this.handleScroll.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.handleItemClick = this.handleItemClick.bind(this);
  }

  componentWillMount() {
    const {
      user,
      navigation,
      currentUnit,
    } = this.props;

    const {
      isFire,
    } = this.state;

    this.setState({ loadingDiscussions: true });

    this.handleUpdateDiscussions();

    if (navigation.state.params) {
      if (navigation.state.params.unitID) {
        this.setState({
          loading: true,
        });
        getUnit(user.token, navigation.state.params.unitID)
          .then(res => res.json())
          .then((result) => {
            if (result.status) {
              this.props.updateCurrentUnit(result.data);
              if (result.data.followed) {
                this.setState({
                  unfollowed: false,
                });
              } else {
                this.setState({
                  unfollowed: true,
                });
              }
              const sort = isFire ? 'hot' : 'new';
              ListSortedDiscussionsByUnit(user.token, navigation.state.params.unitID, sort)
                .then(res => res.json())
                .then((resultDiscussion) => {
                  this.props.updateCurrentDiscussion(resultDiscussion.data);
                  this.setState({
                    currentUnitDiscussions: resultDiscussion.data,
                    refreshing: false,
                    loading: false,
                  });
                });
            }
          });
      }
    } else {
      this.setState({
        loadingStars: true,
      });

      getUnit(user.token, currentUnit.uuid)
        .then(res => res.json())
        .then((result) => {
          if (result.status) {
            this.props.updateCurrentUnit(result.data);
            this.setState({
              loadingStars: false,
            });
          }
        });
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => this.props.navigation.navigate('Home');

  handleItemClick(currentDiscussion) {
    this.props.updateCurrentDiscussion(currentDiscussion);
    this.props.navigation.navigate('Thread');
  }

  handleScroll(event) {
    if (event.nativeEvent) {
      const contentHeight = event.nativeEvent.contentSize.height;
      const layoutHeight = event.nativeEvent.layoutMeasurement.height;
      if (event.nativeEvent.contentOffset.y <= 0) {
        if (this.state.headerMinified) {
          this.setState({
            headerMinified: false,
          });
        }
      } else if (!this.state.headerMinified && (contentHeight > layoutHeight)) {
        this.setState({
          headerMinified: true,
        });
      }
    }
  }

  handleUpdateDiscussions(isFire) {
    this.setState({
      refreshing: false,
    });

    const {
      user,
      currentUnit,
    } = this.props;

    const sort = isFire ? 'hot' : 'new';

    ListSortedDiscussionsByUnit(user.token, currentUnit.uuid, sort)
      .then(res => res.json())
      .then((result) => {
        this.setState({
          currentUnitDiscussions: result.data,
          refreshing: false,
          loadingDiscussions: false,
        });
      });
  }

  handleSort() {
    const {
      isFire,
    } = this.state;

    if (isFire) {
      this.handleUpdateDiscussions(false);
      this.setState({ isFire: false });
    } else {
      this.handleUpdateDiscussions(true);
      this.setState({ isFire: true });
    }
  }

  render() {
    const {
      isFire,
      headerMinified,
      currentUnitDiscussions,
      unfollowed,
      loading,
      loadingDiscussions,
      loadingStars,
    } = this.state;

    const {
      user,
      navigation,
      currentUnit,
    } = this.props;

    return (
      <UnitContainer>
        {loading &&
          <LoadingContainer>
            <ActivityIndicator size="large" color="rgb(80,227,194)" />
          </LoadingContainer>
        }
        {!loading &&
          <ThreadContainer>
            <UnitHeader
              {...this.props}
              user={user}
              unit={currentUnit}
              minified={headerMinified}
              pageLink={navigation.state.params ? navigation.state.params.pageLink : ''}
              unfollowed={((currentUnit.followed === false) || unfollowed)}
              loadingStars={loadingStars}
            />
            <CallToInput
              onClick={() => this.props.navigation.navigate('AddPost', { unit: currentUnit, pageLink: 'Unit' })}
              label="Share feedback on your experience"
            />
            <FeedHeaderFilter>
              <FilterContent>
                <FilterItem onPress={() => this.handleSort()}>
                  <FilterText>
                    <HeroIcon name="fire" size={15} color={(isFire) ? 'rgb(58,210,176)' : 'rgb(74,74,74)'} />
                  </FilterText>
                </FilterItem>
                <FilterItem onPress={() => this.handleSort()}>
                  <FilterText>
                    <HeroIcon name="clock" size={15} color={(!isFire) ? 'rgb(58,210,176)' : 'rgb(74,74,74)'} />
                  </FilterText>
                </FilterItem>
              </FilterContent>
            </FeedHeaderFilter>
            {(!currentUnitDiscussions[0] && !loadingDiscussions) &&
              <FeedClean>
                <FeedCleanText>
                  {feedCleanText}
                </FeedCleanText>
                <Button
                  label="Tap here to post"
                  accessibilityLabel="Tap here to post"
                  onPress={() => this.props.navigation.navigate('AddPost', { unit: currentUnit, pageLink: 'Unit' })}
                />
              </FeedClean>
            }
            {currentUnitDiscussions[0] &&
              <ThreadContentWrap>
                <ThreadContent
                  onScroll={e => this.handleScroll(e)}
                  scrollEventThrottle={20}
                  data={currentUnitDiscussions}
                  keyExtractor={item => item.uuid}
                  onRefresh={() => this.handleUpdateDiscussions()}
                  refreshing={this.state.refreshing}
                  renderItem={({ item }) => (
                    <FeedItem
                      sugestion={item}
                      currentUser={user}
                      discussionId={currentUnit.uuid}
                      itemId={item.uuid}
                      update={() => this.handleUpdateDiscussions()}
                      onClick={() => this.handleItemClick(item)}
                      token={user.token}
                      limit={150}
                    />
                    )}
                />
              </ThreadContentWrap>
            }
          </ThreadContainer>
        }
      </UnitContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  currentUnit: state.currentUnit,
});

const mapDispatchToProps = dispatch => ({
  updateCurrentDiscussion(unit) {
    dispatch(updateCurrentDiscussion(unit));
  },
  updateCurrentUnit(unit) {
    dispatch(updateCurrentUnit(unit));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Unit);
