import styled, { css } from 'styled-components';
import { width, height } from 'react-native-dimension';
import { lightGray, white, tealish } from '../../../styles/colors';
import { NeuzeitBookHeavy } from '../../../styles/fonts';

export const UnitContainer = styled.View`
`;

export const LoadingContainer = styled.View`
  justify-content: center;
  height: 100%;
  background: ${white};
`;

export const ThreadContainer = styled.View`
  display: flex;
  background: ${white};
  height: 100%;
`;

export const ThreadHeader = styled.View`
  display: flex;
  background: ${lightGray};
`;

export const ThreadContentWrap = styled.View`
  padding-bottom: 179;
`;

export const ThreadContent = styled.FlatList`
  display: flex;
  width: ${width(100)};
  min-height: ${height(60)};
`;

export const FeedHeaderFilter = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  border-bottom-width: 1;
  border-bottom-color: rgb(231,223,223);
`;

export const FilterContent = styled.View`
  display: flex;
  flex-direction: row;
  margin-left: 30;
  margin-right: 10;
`;

export const FilterItem = styled.TouchableWithoutFeedback`
  display: flex;
`;

export const FilterText = styled.Text`
  font-family: ${NeuzeitBookHeavy};
  font-size: 12;
  margin-right: 20;
  padding-top: 15;
  padding-bottom: 13;

  ${props => props.active && css`
    color: ${tealish};
  `}
`;
