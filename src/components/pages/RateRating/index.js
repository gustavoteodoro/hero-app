import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '../../elements/Button';
import RateHeader from '../../modules/RateHeader';
import SelectRatingStars from '../../elements/SelectRatingStars';
import { updateCurrentUnit } from '../../../actions/currentUnit';
import { createEvaluation } from '../../../services/evaluations';
import { getUnit } from '../../../services/courses';

import {
  RateContainer,
  RateContentContainer,
  ButtonContainer,
  FormContainer,
  RateInput,
  RateHint,
  FormBottom,
} from './styles';

class RateRating extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      state: PropTypes.object,
    }),
    currentUnit: PropTypes.shape({
      attributes: PropTypes.object,
    }),
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
    updateCurrentUnit: PropTypes.func,
  }

  static defaultProps = {
    navigation: null,
    currentUnit: null,
    user: null,
    updateCurrentUnit: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      currentRating: 0,
      comment: '',
      loading: false,
      focused: false,
    };

    this.handleRate = this.handleRate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => this.props.navigation.goBack();

  onChangeComment(comment) {
    this.setState({
      comment,
    });
  }

  handleRate(i) {
    this.setState({
      currentRating: i,
    });
  }

  handleSubmit() {
    this.setState({
      loading: true,
    });

    const {
      currentRating,
      comment,
    } = this.state;

    const {
      user,
      navigation,
      currentUnit,
    } = this.props;

    const selectedCategories = navigation.state.params ? navigation.state.params.selectedCategories : [];

    createEvaluation(
      user.token,
      currentUnit.uuid,
      selectedCategories,
      currentRating,
      comment,
    )
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          getUnit(user.token, currentUnit.uuid)
            .then(res => res.json())
            .then((resultUnit) => {
              if (resultUnit.status) {
                this.props.updateCurrentUnit(resultUnit.data);
                this.props.navigation.navigate('PostConfirm', { rateConfirm: true });
              }
              this.setState({
                loading: false,
              });
            });
        }
      });
  }

  render() {
    const {
      currentRating,
      loading,
      focused,
    } = this.state;

    return (
      <RateContainer>
        <RateHeader
          {...this.props}
          title="Rate"
        />
        <RateContentContainer>
          <FormContainer>
            <SelectRatingStars currentRating={currentRating} onRate={i => this.handleRate(i)} />
            <RateInput
              name="comment"
              placeholder="Feel free to elaborate on your rating here"
              autoCapitalize="none"
              maxLength={200}
              multiline
              onChangeText={comment => this.onChangeComment(comment)}
              underlineColorAndroid="transparent"
              onFocus={() => this.setState({ focused: true })}
              onBlur={() => this.setState({ focused: true })}
            />
          </FormContainer>
          <FormBottom focused={focused}>
            <ButtonContainer>
              <Button
                label="Submit"
                accessibilityLabel="Submit"
                onPress={() => this.handleSubmit()}
                loading={loading}
                disabled={(currentRating === 0)}
              />
            </ButtonContainer>
            <RateHint>
              This feedback is anonymous.
            </RateHint>
          </FormBottom>
        </RateContentContainer>
      </RateContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  currentUnit: state.currentUnit,
});

const mapDispatchToProps = dispatch => ({
  updateCurrentUnit(unit) {
    dispatch(updateCurrentUnit(unit));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(RateRating);
