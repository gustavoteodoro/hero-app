import styled, { css } from 'styled-components';
import { width, height } from 'react-native-dimension';
import { white, greyishBrownTwo } from '../../../styles/colors';
import { OpenSansRegular } from '../../../styles/fonts';

export const RateContainer = styled.View`
  display: flex;
  background: ${white};
  height: ${height(100)};
  
`;

export const RateContentContainer = styled.ScrollView`
  padding-left: ${width(8)};
  padding-right: ${width(8)};
`;

export const ButtonContainer = styled.View`
  margin-top: 10;
  margin-bottom: 10;
`;

export const RateInput = styled.TextInput`
  min-height: ${height(20)};
`;

export const FormContainer = styled.View`
  min-height: ${height(20)};
`;

export const RateHint = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 12;
  text-align: center;
  color: ${greyishBrownTwo};
`;

export const FormBottom = styled.View`
  margin-top: ${height(40)};

  ${props => props.focused && css`
    margin-top: 0;
  `}
`;
