import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { white } from '../../../styles/colors';

export const RateContainer = styled.View`
  display: flex;
  background: ${white};
  height: ${height(100)};
`;

export const RateContentContainer = styled.ScrollView`
  padding-top: 25;
  padding-left: ${width(8)};
  padding-right: ${width(8)};
`;

export const ButtonContainer = styled.View`
  margin-top: 25;
  margin-bottom: 125;
`;

export const RateContentLoaded = styled.View`

`;
