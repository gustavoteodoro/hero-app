import React, { Component } from 'react';
import { ActivityIndicator, BackHandler } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '../../elements/Button';
import { RateContainer, RateContentContainer, ButtonContainer, RateContentLoaded } from './styles';
import { updateCurrentUnit } from '../../../actions/currentUnit';
import { getUnit } from '../../../services/courses';
import RateHeader from '../../modules/RateHeader';
import CourseButton from '../../elements/CourseButton';

class RateCategory extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      goBack: PropTypes.func,
    }),
    currentUnit: PropTypes.shape({
      attributes: PropTypes.object,
    }),
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
    updateCurrentUnit: PropTypes.func,
  }

  static defaultProps = {
    navigation: null,
    currentUnit: null,
    user: null,
    updateCurrentUnit: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      selectedCategories: [],
    };

    this.handleSelectCategory = this.handleSelectCategory.bind(this);
  }

  componentWillMount() {
    this.setState({ loading: true });
    const { user, currentUnit } = this.props;
    getUnit(user.token, currentUnit.uuid)
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          this.props.updateCurrentUnit(result.data);
          this.setState({
            loading: false,
          });
        }
      });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => this.props.navigation.goBack();

  handleSelectCategory(category) {
    const {
      selectedCategories,
    } = this.state;
    const isSelected = selectedCategories.find(selectedCategory => selectedCategory === category);
    if (isSelected) {
      const categories = _.cloneDeep(selectedCategories);
      const index = categories.indexOf(category);
      categories.splice(index, 1);
      this.setState({
        selectedCategories: categories,
      });
    } else {
      const categories = _.cloneDeep(selectedCategories);
      categories.push(category);
      this.setState({
        selectedCategories: categories,
      });
    }
  }

  render() {
    const {
      loading,
      selectedCategories,
    } = this.state;

    const {
      currentUnit,
    } = this.props;

    return (
      <RateContainer>
        <RateHeader
          {...this.props}
          title="Rate"
          unitTitle={currentUnit.attributes.name}
          description="What aspect(s) of the course are you rating now? Select one or more."
        />
        <RateContentContainer>
          {loading &&
            <ActivityIndicator size="large" color="rgb(80,227,194)" />
          }
          {!loading &&
            <RateContentLoaded>
              {currentUnit.evaluation_aspects.map(category => (
                <CourseButton
                  key={category.uuid}
                  titleCourse={category.name}
                  onPressButton={() => this.handleSelectCategory(category.uuid)}
                  /* eslint-disable-next-line */
                  selected={selectedCategories.find(selectedCategory => selectedCategory === category.uuid)}
                />
              ))}
            </RateContentLoaded>
          }
          <ButtonContainer>
            <Button
              label="Next"
              accessibilityLabel="Next"
              onPress={() => this.props.navigation.navigate('RateRating', { selectedCategories })}
              disabled={(!selectedCategories[0])}
            />
          </ButtonContainer>
        </RateContentContainer>
      </RateContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  currentUnit: state.currentUnit,
});

const mapDispatchToProps = dispatch => ({
  updateCurrentUnit(unit) {
    dispatch(updateCurrentUnit(unit));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(RateCategory);
