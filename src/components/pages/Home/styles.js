import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { white, brownishGrey } from '../../../styles/colors';

export const HomeContainer = styled.View`
  display: flex;
  background: ${white};
  height: ${height(100)};
`;

export const HomeText = styled.Text`
  text-align: center;
  margin-top: 20;
  margin-bottom 20;
`;

export const FeedContainerWrap = styled.View`
  padding-bottom: 189;
`;

export const FeedContainer = styled.FlatList`
  display: flex;
  width: ${width(100)};
  min-height: ${height(60)};
`;

export const ListContainerWrap = styled.View`
  padding-bottom: 189;
`;

export const ListContainer = styled.FlatList`
  display: flex;
  width: ${width(100)};
  min-height: ${height(60)};
`;

export const LoadingContainer = styled.View`
  height: ${height(60)};
  margin-bottom: 100;
  justify-content: center;
`;

export const FeedClean = styled.View`
  margin-left: ${width(8)};
  margin-right: ${width(8)};
`;

export const FeedCleanText = styled.Text`
  font-size: 12;
  line-height: 22;
  color: ${brownishGrey};
  text-align: center;
  padding-left: ${width(6)};
  padding-right: ${width(6)};
  margin-top: ${width(8)};
  margin-bottom: ${width(8)};
`;
