import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ActivityIndicator, BackHandler } from 'react-native';
import { listCoursesFollowed } from '../../../services/courses';
import { ListSortedDiscussionsBySlot } from '../../../services/discussions';
import { clearUser } from '../../../actions/user';
import { setCategory } from '../../../actions/category';
import { updateCourses } from '../../../actions/courses';
import { updateServices } from '../../../actions/services';
import { updateCampus } from '../../../actions/campus';
import { updateDiscussions } from '../../../actions/discussions';
import { updateCurrentDiscussion } from '../../../actions/currentDiscussion';
import { allCampusDepartments, allServicesDepartments } from '../../../services/departments';
import { updateCurrentUnit } from '../../../actions/currentUnit';
import FeedHeader from '../../modules/FeedHeader';
import FeedItem from '../../elements/FeedItem';
import ListItem from '../../elements/ListItem';
import Button from '../../elements/Button';

import {
  HomeContainer,
  FeedContainerWrap,
  FeedContainer,
  ListContainerWrap,
  ListContainer,
  LoadingContainer,
  FeedClean,
  FeedCleanText,
} from './styles';

import { feedCleanText } from './data.json';

class Home extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    user: PropTypes.shape({
      email: PropTypes.string,
      token: PropTypes.string,
    }),
    category: PropTypes.shape({
      category: PropTypes.string,
    }),
    courses: PropTypes.arrayOf(PropTypes.object),
    services: PropTypes.arrayOf(PropTypes.object),
    campus: PropTypes.arrayOf(PropTypes.object),
    setCategory: PropTypes.func,
    updateCourses: PropTypes.func,
    updateServices: PropTypes.func,
    updateCampus: PropTypes.func,
    updateDiscussions: PropTypes.func,
    updateCurrentDiscussion: PropTypes.func,
    discussions: PropTypes.arrayOf(PropTypes.object),
    updateCurrentUnit: PropTypes.func,
    screenProps: PropTypes.shape({
      goUnit: PropTypes.bool,
    }),
  }

  static defaultProps = {
    navigation: null,
    user: null,
    category: 'courses',
    courses: null,
    services: null,
    campus: null,
    setCategory: null,
    updateCourses: null,
    updateServices: null,
    updateCampus: null,
    updateDiscussions: null,
    updateCurrentDiscussion: null,
    discussions: null,
    updateCurrentUnit: null,
    screenProps: {
      goUnit: false,
    },
  }

  constructor(props) {
    super(props);

    this.state = {
      headerMinified: false,
      isFeed: false,
      isFire: false,
      loading: false,
      refreshing: false,
      currentUser: '',
    };

    this.handleItemClick = this.handleItemClick.bind(this);
    this.handleItemClickComment = this.handleItemClickComment.bind(this);
    this.handleItemClickUnit = this.handleItemClickUnit.bind(this);
    this.handleItemClickRate = this.handleItemClickRate.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
    this.onChangeVisualization = this.onChangeVisualization.bind(this);
    this.onChangeSort = this.onChangeSort.bind(this);
    this.updateCourses = this.updateCourses.bind(this);
    this.updateDuscussionsWithNoLoad = this.updateDuscussionsWithNoLoad.bind(this);
    this.handleGoUnit = this.handleGoUnit.bind(this);
  }

  componentWillMount() {
    this.setState({
      currentUser: this.props.user.email,
    });
    this.updateCourses();
    this.updateServices();
    this.updateCampus();
    this.updateDuscussions();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => true;

  onChangeCategory(category) {
    this.props.setCategory({ category });
    this.updateDuscussions(category);
  }

  onUpdateTagList() {
    const {
      category,
    } = this.props.category;

    if (category === 'courses') {
      this.updateCourses();
    } else if (category === 'services') {
      this.updateServices();
    } else if (category === 'campus') {
      this.updateCampus();
    }
  }

  onChangeVisualization(isFeed) {
    this.setState({ isFeed });
  }

  onChangeSort(isFire) {
    this.setState({ isFire }, () => this.updateDuscussions());
  }

  handleItemClick(currentDiscussion) {
    this.props.updateCurrentDiscussion(currentDiscussion);
    this.props.navigation.navigate('Thread', { update: () => this.updateDuscussions() });
  }

  handleItemClickComment(currentDiscussion) {
    this.props.updateCurrentDiscussion(currentDiscussion);
    this.props.navigation.navigate('AddComment');
  }

  handleItemClickPost(unit) {
    this.props.navigation.navigate('AddPost', { unit });
  }

  handleItemClickUnit(unit) {
    this.props.updateCurrentUnit(unit);
    this.props.navigation.navigate('Unit');
  }

  handleItemClickRate(unit) {
    const {
      type,
    } = unit.attributes;
    this.props.updateCurrentUnit(unit);
    if (type === 'course') {
      this.props.navigation.navigate('RateCategory');
    } else {
      this.props.navigation.navigate('RateRating');
    }
  }

  handleScroll(event) {
    if (event.nativeEvent) {
      const contentHeight = event.nativeEvent.contentSize.height;
      const layoutHeight = event.nativeEvent.layoutMeasurement.height;
      if ((event.nativeEvent.contentOffset.y) <= 0) {
        if (this.state.headerMinified) {
          this.setState({
            headerMinified: false,
          });
        }
      } else if (!this.state.headerMinified && (contentHeight > layoutHeight)) {
        this.setState({
          headerMinified: true,
        });
      }
    }
  }

  updateCourses() {
    this.setState({
      refreshing: true,
      currentUser: this.props.user.email,
    });
    listCoursesFollowed(this.props.user.token)
      .then(res => res.json())
      .then((result) => {
        const currentCourses = [];
        result.data.map((department) => {
          department.children.map(course => currentCourses.push(course));
        });
        this.props.updateCourses(currentCourses);
        setTimeout(() => {
          this.setState({
            loading: false,
            refreshing: false,
          });
        }, 1000);
      });
  }

  updateServices() {
    this.setState({
      refreshing: true,
    });

    allServicesDepartments(this.props.user.token)
      .then(res => res.json())
      .then((result) => {
        this.props.updateServices(result.data);
        setTimeout(() => {
          this.setState({
            loading: false,
            refreshing: false,
          });
        }, 1000);
      });
  }

  updateCampus() {
    this.setState({
      refreshing: true,
    });

    allCampusDepartments(this.props.user.token)
      .then(res => res.json())
      .then((result) => {
        this.props.updateCampus(result.data);
        setTimeout(() => {
          this.setState({
            loading: false,
            refreshing: false,
          });
        }, 1000);
      });
  }

  updateDuscussions(changedCategory) {
    this.setState({
      refreshing: false,
    });

    const {
      user,
      category,
    } = this.props;

    const {
      isFire,
    } = this.state;

    const currentCategory = changedCategory || category.category;

    const sort = isFire ? 'hot' : 'new';

    ListSortedDiscussionsBySlot(user.token, currentCategory, sort)
      .then(res => res.json())
      .then((result) => {
        this.props.updateDiscussions(result.data);
        setTimeout(() => {
          this.setState({
            loading: false,
            refreshing: false,
          });
        }, 1000);
      });
  }

  updateDuscussionsWithNoLoad(changedCategory) {
    const {
      user,
      category,
    } = this.props;

    const {
      isFire,
    } = this.state;

    const currentCategory = changedCategory || category.category;

    const sort = isFire ? 'hot' : 'new';

    ListSortedDiscussionsBySlot(user.token, currentCategory, sort)
      .then(res => res.json())
      .then((result) => {
        this.props.updateDiscussions(result.data);
      });
  }

  handleGoUnit = () => {
    setTimeout(() => this.props.navigation.navigate('Unit'), 200);
  }

  render() {
    const {
      headerMinified,
      isFeed,
      isFire,
      loading,
      currentUser,
    } = this.state;

    const {
      courses,
      services,
      campus,
      discussions,
      user,
      screenProps,
    } = this.props;

    const {
      category,
    } = this.props.category;

    const getCurrentTags = () => {
      if (category === 'courses') {
        return courses;
      } else if (category === 'services') {
        return services;
      } else if (category === 'campus') {
        return campus;
      }
      return false;
    };

    const currentTags = getCurrentTags();

    if (screenProps.goUnit) {
      this.handleGoUnit();
    }

    if (user.email && (currentUser !== user.email)) {
      this.updateCourses();
    }

    return (
      <HomeContainer>
        <FeedHeader
          minified={headerMinified}
          category={category}
          isFeed={isFeed}
          isFire={isFire}
          onChangeCategory={e => this.onChangeCategory(e)}
          onChangeVisualization={e => this.onChangeVisualization(e)}
          onChangeSort={e => this.onChangeSort(e)}
        />
        {loading &&
          <LoadingContainer>
            <ActivityIndicator size="large" color="#50e3c2" />
          </LoadingContainer>
        }
        {isFeed &&
          <FeedContainerWrap>
            {!discussions[0] &&
              <FeedClean>
                <FeedCleanText>
                  {feedCleanText}
                </FeedCleanText>
                <Button
                  label="Tap here to rate or post"
                  accessibilityLabel="Tap here to rate or post"
                  onPress={() => this.onChangeVisualization(false)}
                />
              </FeedClean>
            }
            {discussions[0] &&
              <FeedContainer
                onScroll={e => this.handleScroll(e)}
                scrollEventThrottle={20}
                data={discussions}
                keyExtractor={item => item.uuid}
                onRefresh={() => this.updateDuscussions()}
                refreshing={this.state.refreshing}
                renderItem={({ item }) => (
                  <FeedItem
                    sugestion={item}
                    key={item.uuid}
                    itemId={item.uuid}
                    currentUser={user}
                    liked={item.liked}
                    update={() => this.updateDuscussionsWithNoLoad()}
                    onClickComment={() => this.handleItemClickComment(item)}
                    onClick={() => this.handleItemClick(item)}
                    token={user.token}
                    limit={150}
                  />
                )}
              />
            }
          </FeedContainerWrap>
        }
        {!isFeed &&
          <ListContainerWrap>
            {currentTags[0] &&
              <ListContainer
                onScroll={e => this.handleScroll(e)}
                scrollEventThrottle={20}
                data={currentTags}
                keyExtractor={item => item.uuid}
                onRefresh={() => this.onUpdateTagList()}
                refreshing={this.state.refreshing}
                renderItem={({ item }) => (
                  <ListItem
                    course={item}
                    key={item.uuid}
                    onClickUnit={unit => this.handleItemClickUnit(unit)}
                    onClickRate={unit => this.handleItemClickRate(unit)}
                    onClickPost={unit => this.handleItemClickPost(unit)}
                  />
                )}
              />
            }
          </ListContainerWrap>
        }
      </HomeContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  category: state.category,
  courses: state.courses,
  services: state.services,
  campus: state.campus,
  discussions: state.discussions,
});

const mapDispatchToProps = dispatch => ({
  clearUser() {
    dispatch(clearUser());
  },
  setCategory(category) {
    dispatch(setCategory(category));
  },
  updateCourses(courses) {
    dispatch(updateCourses(courses));
  },
  updateServices(services) {
    dispatch(updateServices(services));
  },
  updateCampus(campus) {
    dispatch(updateCampus(campus));
  },
  updateDiscussions(discussions) {
    dispatch(updateDiscussions(discussions));
  },
  updateCurrentUnit(unit) {
    dispatch(updateCurrentUnit(unit));
  },
  updateCurrentDiscussion(unit) {
    dispatch(updateCurrentDiscussion(unit));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
