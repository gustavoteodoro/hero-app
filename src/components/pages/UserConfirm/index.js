import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator, BackHandler, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
// eslint-disable-next-line
import { Feather } from '@expo/vector-icons';
import Logo from '../../elements/Logo';
import { confirmAccount } from '../../../services/account';
import { setUser } from '../../../actions/user';
import { checkValidVerificationCode } from '../../../utils/validations/verificationCode';

import {
  UserConfirmContainer,
  TapAnywereText,
  MessageContainer,
  MessageIcon,
  MessageTitle,
  MessageText,
  MessageHint,
} from './styles';

// eslint-disable-next-line
class UserConfirm extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      state: PropTypes.shape({
        params: PropTypes.shape({
          verificationCode: PropTypes.string,
        }),
      }),
    }),
    user: PropTypes.shape({
      email: PropTypes.email,
    }),
  }

  static defaultProps = {
    navigation: null,
    user: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      error: null,
      loading: false,
      nextPage: 'Login',
    };
  }

  componentWillMount() {
    this.setState({
      loading: true,
    });

    if (this.props.navigation.state.params) {
      const {
        verificationCode,
      } = this.props.navigation.state.params;

      const { email } = this.props.user;
      const error = checkValidVerificationCode(verificationCode);
      this.setState({
        error,
      });
      if (!error) {
        this.sendData(email, verificationCode);
      } else {
        this.setState({
          loading: false,
        });
      }
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => true;

  sendData = (email, verificationCode) => {
    confirmAccount(email, verificationCode)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.props.setUser({
            email,
            verified: true,
            notifications: true,
            token: result.data.access_token,
          });
          this.setState({
            nextPage: 'Setup',
          });
        } else if (result.error.code === 400) {
          this.setState({
            error: result.error.message,
            nextPage: 'Login',
          });
        } else {
          this.setState({
            error: result.error.message ? result.error.message : 'Failed to confirm user.',
            nextPage: 'Signup',
          });
        }
        this.setState({
          loading: false,
        });
      });
  }

  render() {
    const {
      error,
      loading,
      nextPage,
    } = this.state;

    return (
      <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate(nextPage)}>
        <UserConfirmContainer>
          <Logo />
          {error &&
            <MessageContainer>
              <MessageText>{error}</MessageText>
            </MessageContainer>
          }
          {loading &&
            <ActivityIndicator size="large" color="#50e3c2" />
          }
          {(!error && !loading) &&
            <MessageContainer>
              <MessageIcon>
                <Feather name="check" size={30} color="black" />
              </MessageIcon>
              <MessageTitle>Thank you!</MessageTitle>
              <MessageText>
                Your e-mail address have been verified
              </MessageText>
              <MessageHint>
                To start rating, create your profile and add your courses.
              </MessageHint>
            </MessageContainer>
          }
          <TapAnywereText>
            {loading ? '' : 'Tap anywere'}
          </TapAnywereText>
        </UserConfirmContainer>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  setUser(userData) {
    dispatch(setUser(userData));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UserConfirm);
