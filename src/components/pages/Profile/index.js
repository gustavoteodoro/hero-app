import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ProfileMenu from '../../modules/ProfileMenu';
import { clearUser } from '../../../actions/user';
import { ProfileContainer, ProfileHeader } from './styles';

import { profileOptions } from './data.json';

class Profile extends Component {
  static propTypes = {
    user: PropTypes.shape({
      email: PropTypes.string,
      name: PropTypes.name,
      notifications: PropTypes.bool,
    }),
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      dispatch: PropTypes.func,
    }),
    clearUser: PropTypes.func,
  }

  static defaultProps = {
    clearUser: null,
    user: null,
    navigation: null,
  }

  constructor(props) {
    super(props);

    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout() {
    this.props.clearUser();
    this.props.navigation.navigate('Login');
  }

  render() {
    return (
      <ProfileContainer>
        <ProfileHeader>
          Profile
        </ProfileHeader>
        <ProfileMenu
          {...this.props}
          items={profileOptions}
          name={this.props.user.name}
          email={this.props.user.email}
          onLogout={this.handleLogout}
        />
      </ProfileContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  clearUser() {
    dispatch(clearUser());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
