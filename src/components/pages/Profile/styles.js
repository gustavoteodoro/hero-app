import styled from 'styled-components';
import { lightGray, black } from '../../../styles/colors';
import { OpenSansBold } from '../../../styles/fonts';

export const ProfileContainer = styled.View`
  display: flex;
  height: 100%;
  background: white;
`;

export const ProfileText = styled.Text`
  text-align: center;
  margin-top: 80;
  margin-bottom 20;
`;

export const ProfileHeader = styled.Text`
  font-family: ${OpenSansBold};
  text-align: center;
  background: ${lightGray};
  color: ${black};
  padding-top: 35;
  padding-bottom: 20;
  margin-bottom 20;
  box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.5);
`;
