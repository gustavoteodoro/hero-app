import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  CourseSelectContainer,
  ChooseDeparmentButton,
  TextRegular,
  IconPlus,
  ButtonPlus,
  ButtonContainer,
  CourseSelectHeader,
  CourseSelectContent,
  LoadingContainer,
} from './styles';
import { unfollowCourse, listCoursesFollowed } from '../../../services/courses';
import TitleBar from '../../modules/TitleBar';
import MyCourses from '../../modules/MyCourses';
import iconPlus from '../../../assets/plus_icon.png';
import Button from '../../elements/Button';

class CoursesAdded extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
  }

  static defaultProps = {
    navigation: null,
    user: {},
  }

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      followedCourses: [],
    };
  }

  componentDidMount() {
    this.handleUpdateFollowedDepartment();
  }

  handleUpdateFollowedDepartment() {
    this.setState({
      loading: true,
    });

    const {
      user,
    } = this.props;

    listCoursesFollowed(user.token)
      .then(res => res.json())
      .then((result) => {
        this.setState({
          loading: false,
          followedCourses: result.data,
        });
      });
  }

  handleUnfollowCourse(courseId) {
    const {
      user,
    } = this.props;

    const courses = [courseId];

    unfollowCourse(user.token, courses)
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          this.handleUpdateFollowedDepartment();
        }
      });
  }

  redirect = (page) => {
    this.props.navigation.navigate(page);
  }

  handleSubmit() {
    this.props.navigation.navigate('TabNavigation');
  }

  render() {
    const {
      navigation,
    } = this.props;

    const {
      loading,
      followedCourses,
    } = this.state;

    return (
      <CourseSelectContainer>
        <CourseSelectHeader>
          <TitleBar {...this.props} pageTitle="Courses added" />
        </CourseSelectHeader>
        {!loading &&
          <CourseSelectContent>
            <MyCourses
              courses={followedCourses}
              onUnfollow={course => this.handleUnfollowCourse(course)}
            />
            <ChooseDeparmentButton onPress={() => navigation.navigate('DepartmentSelect')}>
              <TextRegular>
                Add more courses
              </TextRegular>
              <ButtonPlus>
                <IconPlus source={iconPlus} />
              </ButtonPlus>
            </ChooseDeparmentButton>
            <ButtonContainer>
              <Button
                label="Save and get started"
                accessibilityLabel="Next"
                onPress={() => this.handleSubmit()}
              />
            </ButtonContainer>
          </CourseSelectContent>
        }
        {loading &&
          <LoadingContainer>
            <ActivityIndicator size="large" color="#50e3c2" />
          </LoadingContainer>
        }
      </CourseSelectContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(CoursesAdded);
