import styled from 'styled-components';
import { height } from 'react-native-dimension';
import { white } from '../../../styles/colors';

export const AddPostContainer = styled.View`
  display: flex;
  background: ${white};
  height: ${height(100)};
`;
