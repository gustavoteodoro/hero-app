import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { AddPostContainer } from './styles';
import AddCommentHeader from '../../modules/AddCommentHeader';
import AddCommentForm from '../../modules/AddCommentForm';
import { createDiscussion } from '../../../services/discussions';
import { updateCurrentDiscussion } from '../../../actions/currentDiscussion';

class AddPost extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      goBack: PropTypes.func,
      state: PropTypes.object,
    }),
    formReducer: PropTypes.shape({
      SignUp: PropTypes.object,
    }),
    setUser: PropTypes.func,
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
    updateCurrentDiscussion: PropTypes.func,
  }

  static defaultProps = {
    navigation: null,
    formReducer: null,
    setUser: null,
    user: null,
    updateCurrentDiscussion: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      error: '',
      comment: '',
      anonymous: false,
      loading: false,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCheckAnonymous = this.handleCheckAnonymous.bind(this);
    this.handleChangeComment = this.handleChangeComment.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => this.props.navigation.goBack();

  sendData = (token, unit, text, anonymous) => {
    const {
      params,
    } = this.props.navigation.state;

    this.setState({
      loading: true,
    });
    createDiscussion(token, unit, text, anonymous)
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          const resultData = Object.assign(result.data, {
            comments_cached: 0,
            likes_cached: 0,
            liked: false,
          });
          this.props.updateCurrentDiscussion(resultData);
          this.props.navigation.navigate('PostConfirm', { unitOrigin: (params.pageLink === 'Unit') });
        } else {
          this.setState({
            error: result.message ? result.message : 'Failed to connect to server.',
            loading: false,
          });
        }
      });
  }

  handleSubmit = () => {
    const {
      unit,
    } = this.props.navigation.state.params;

    const {
      token,
    } = this.props.user;

    const {
      comment,
      anonymous,
    } = this.state;

    if (comment === '') {
      this.setState({
        error: 'Please, fill the field above with your feedback.',
      });
    } else {
      this.sendData(token, unit, comment, anonymous);
    }
  }

  handleCheckAnonymous = (checked) => {
    this.setState({
      anonymous: checked,
    });
  }

  handleChangeComment = (comment) => {
    this.setState({
      comment,
    });
  }

  render() {
    const {
      error,
      loading,
    } = this.state;
    return (
      <AddPostContainer>
        <AddCommentHeader {...this.props} title="Post" />
        <AddCommentForm
          {...this.props}
          commentLabel="Share feedback on your experience"
          commentAnonymousLabel="Post as anonymous"
          commentSendLabel="Post"
          onChangeComment={comment => this.handleChangeComment(comment)}
          onCheckAnonymous={checked => this.handleCheckAnonymous(checked)}
          onSubmit={() => this.handleSubmit()}
          formError={error}
          loading={loading}
        />
      </AddPostContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  updateCurrentDiscussion(discussion) {
    dispatch(updateCurrentDiscussion(discussion));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(AddPost);
