import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';
import { CreateUsernameContainer } from './styles';
import TitleBar from '../../modules/TitleBar';
import UserUpdateForm from '../../modules/UserUpdateForm';

class UpdateUserName extends Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }
  // eslint-disable-next-line
  onBackButtonPressAndroid = () => this.props.navigation.goBack();

  render() {
    return (
      <CreateUsernameContainer>
        <TitleBar {...this.props} pageTitle="Name" />
        <UserUpdateForm
          {...this.props}
        />
      </CreateUsernameContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(UpdateUserName);
