import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TermsContainer,
  TitleContainer,
  ButtonContainer,
  TextLinkContainer,
  TermsBackground,
} from './styles';
import ScrollViewText from '../../elements/ScrollViewText';
import Logo from '../../elements/Logo';
import TitleMedium from '../../elements/TitleMedium';
import TextLink from '../../elements/TextLink';
import Button from '../../elements/Button';

import { terms } from './data.json';


export default class TermsAndConditions extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
  }

  static defaultProps = {
    navigation: null,
  }

  redirect = (page) => {
    this.props.navigation.navigate(page);
  }

  render() {
    return (
      <TermsBackground>
        <TermsContainer>
          <Logo />
          <TitleContainer>
            <TitleMedium titleText="Terms and Conditions" />
          </TitleContainer>
          <ScrollViewText text={terms} />
          <ButtonContainer>
            <Button
              label="I accept! Let’s get going"
              accessibilityLabel="I accept! Let’s get going"
              onPress={() => this.redirect('Signup')}
            />
          </ButtonContainer>
          <TextLinkContainer>
            <TextLink {...this.props} text="Sorry! I can’t do that" page="OnBoard" />
          </TextLinkContainer>
        </TermsContainer>
      </TermsBackground>
    );
  }
}
