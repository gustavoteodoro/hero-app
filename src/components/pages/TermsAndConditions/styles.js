import styled from 'styled-components';
import { lightGray } from '../../../styles/colors';

export const TermsBackground = styled.View`
  height: 100%;
  background-color: ${lightGray};
`;

export const TermsContainer = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${lightGray};
`;

export const TitleContainer = styled.View`
  margin-top: 25px;
`;

export const ButtonContainer = styled.View`
  margin-top: 25px;
`;

export const TextLinkContainer = styled.View`
  margin-top: 15px;
`;
