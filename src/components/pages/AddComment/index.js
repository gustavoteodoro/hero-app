import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { AddCommentContainer } from './styles';
import { createComment } from '../../../services/comments';
import AddCommentHeader from '../../modules/AddCommentHeader';
import AddCommentForm from '../../modules/AddCommentForm';
import { createAccount } from '../../../services/account';

class AddComment extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      goBack: PropTypes.func,
    }),
    formReducer: PropTypes.shape({
      SignUp: PropTypes.object,
    }),
    setUser: PropTypes.func,
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
    currentDiscussion: PropTypes.shape({
      uuid: PropTypes.string,
    }),
  }

  static defaultProps = {
    navigation: null,
    formReducer: null,
    setUser: null,
    user: null,
    currentDiscussion: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      error: '',
      comment: '',
      anonymous: false,
      loading: false,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCheckAnonymous = this.handleCheckAnonymous.bind(this);
    this.handleChangeComment = this.handleChangeComment.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => this.props.navigation.goBack();

  sendData = (email, password) => {
    this.setState({
      loading: true,
    });
    createAccount(email, password)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.props.setUser({ email: result.data.user.email, verified: false });
          this.props.navigation.navigate('VerifyEmail');
        } else {
          this.setState({
            error: result.message ? result.message : 'Failed to connect to server.',
            loading: false,
          });
        }
      });
  }

  handleSubmit = () => {
    const {
      comment,
      anonymous,
    } = this.state;

    const {
      user,
      currentDiscussion,
    } = this.props;

    if (comment !== '') {
      createComment(user.token, currentDiscussion.uuid, comment, anonymous)
        .then(res => res.json())
        .then((result) => {
          if (result.status) {
            this.props.navigation.navigate('Thread', {
              pageLink: 'Home',
              commentAdded: true,
            });
          }
        });
    } else {
      this.setState({
        error: 'Please, fill the field above with your comment.',
      });
    }
  }

  handleCheckAnonymous = (checked) => {
    this.setState({
      anonymous: checked,
    });
  }

  handleChangeComment = (comment) => {
    this.setState({
      comment,
    });
  }

  render() {
    const {
      error,
      loading,
    } = this.state;

    return (
      <AddCommentContainer>
        <AddCommentHeader {...this.props} title="Add comment" />
        <AddCommentForm
          {...this.props}
          commentLabel="Write your comment here"
          commentAnonymousLabel="Add as anonymous"
          commentSendLabel="Add"
          onChangeComment={comment => this.handleChangeComment(comment)}
          onCheckAnonymous={checked => this.handleCheckAnonymous(checked)}
          onSubmit={() => this.handleSubmit()}
          formError={error}
          loading={loading}
        />
      </AddCommentContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  currentDiscussion: state.currentDiscussion,
});

export default connect(mapStateToProps)(AddComment);
