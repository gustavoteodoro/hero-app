import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { black, lightGray, greyishBrown, greyishBrownTwo } from '../../../styles/colors';
import { NeuzeitBookHeavy, OpenSansRegular, OpenSansBold } from '../../../styles/fonts';

export const NotificationTouchable = styled.TouchableWithoutFeedback`

`;

export const NotificationContainer = styled.View`
  display: flex;
  height: 100%;
  background: ${lightGray};
`;

export const NotificationTitle = styled.Text`
  font-family: ${NeuzeitBookHeavy};
  font-size: 25;
  color: ${black};
  margin-top: 140;
  margin-left: ${width(8)};
`;

export const NotificationDescription = styled.View`
  margin-top: 20;
  margin-left: ${width(8)};
  margin-right: ${width(8)};
`;

export const NotificationDescriptionTitle = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 12;
  color: ${greyishBrown};
  line-height: 22;
`;

export const NotificationDescriptionText = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 12;
  color: ${greyishBrown};
  line-height: 22;
`;

export const TapAnywere = styled.Text`
  position: absolute;
  width: ${width(100)};
  bottom: 60;
  text-align: center;
  font-family: ${OpenSansBold};
  font-size: 12;
  color: ${greyishBrownTwo};
  line-height: 22;
`;
