import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  NotificationTouchable,
  NotificationContainer,
  NotificationTitle,
  NotificationDescription,
  NotificationDescriptionTitle,
  NotificationDescriptionText,
  TapAnywere,
} from './styles';

class Notification extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      state: PropTypes.object,
    }),
  }

  static defaultProps = {
    navigation: null,
  }

  render() {
    const {
      message,
    } = this.props.navigation.state.params;

    return (
      <NotificationTouchable onPress={() => this.props.navigation.navigate('App')}>
        <NotificationContainer>
          <NotificationTitle>Have we improved?</NotificationTitle>
          <NotificationDescription>
            <NotificationDescriptionText>
              {message}
            </NotificationDescriptionText>
          </NotificationDescription>
          <NotificationDescription>
            <NotificationDescriptionTitle>
              Nofitifications
            </NotificationDescriptionTitle>
            <NotificationDescriptionText>
              You get this message because you have
              notifications switched on in settings.
              Go to ptofile to change settings.
            </NotificationDescriptionText>
          </NotificationDescription>
          <TapAnywere>Tap anywere</TapAnywere>
        </NotificationContainer>
      </NotificationTouchable>
    );
  }
}

export default Notification;
