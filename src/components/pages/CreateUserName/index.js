import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, ActivityIndicator } from 'react-native';
import { CreateUsernameContainer, LoadingContainer } from './styles';
import UniversityGreeting from '../../modules/UniversityGreeting';
import UserSetupForm from '../../modules/UserSetupForm';
import { universityByDomain } from '../../../services/university';

class CreateUserName extends Component {
  static propTypes = {
    user: PropTypes.shape({
      token: PropTypes.string,
      email: PropTypes.string,
    }),
  }

  static defaultProps = {
    user: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      universityName: '',
      universityLogo: '',
    };
  }

  componentWillMount() {
    this.setState({ loading: true });
    const { token, email } = this.props.user;
    const atIndex = email.indexOf('@');
    const domain = email.slice(atIndex + 1, email.lenght);

    universityByDomain(token, domain)
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          this.setState({
            universityName: result.data[0].name,
            universityLogo: result.data[0].logo ? result.data[0].logo : '',
            loading: false,
          });
        }
      });
  }
  render() {
    const {
      loading,
      universityName,
      universityLogo,
    } = this.state;

    return (
      <CreateUsernameContainer>
        {loading ?
          <LoadingContainer>
            <ActivityIndicator size="large" color="#50e3c2" />
          </LoadingContainer>
          : null
        }
        {!loading ?
          <View>
            <UniversityGreeting
              universityName={universityName}
              universityLogo={universityLogo}
            />
            <UserSetupForm
              {...this.props}
            />
          </View>
          : null
        }
      </CreateUsernameContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(CreateUserName);
