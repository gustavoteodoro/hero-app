import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { lightGray } from '../../../styles/colors';

export const CreateUsernameContainer = styled.View`
  display: flex;
  align-items: center;
  width: ${width(100)};
  background-color: ${lightGray};
  height: 100%;
`;

export const LoadingContainer = styled.View`
  height: 100%;
  justify-content: center;
`;
