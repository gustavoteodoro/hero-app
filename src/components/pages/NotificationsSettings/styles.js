import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { lightGray, white, whiteFour } from '../../../styles/colors';
import { OpenSansRegular, OpenSansBold } from '../../../styles/fonts';

export const NotificationsContainer = styled.View`
  display: flex;
  align-items: center;
  width: ${width(100)};
  background-color: ${lightGray};
  height: 100%;
`;

export const NotificationField = styled.View`
  background: ${white};
  width: ${width(84)};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-left: ${width(8)};
  margin-right: ${width(8)};
  border-width: 1;
  border-color: ${whiteFour};
  margin-top: 40;
  padding-left: 25;
  padding-right: 25;
`;

export const NotificationLabel = styled.View`
  margin-top: 10;
  margin-bottom: 10;
`;

export const NotificationLabelTitle = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 11;
`;

export const NotificationLabelText = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
`;

export const SwitchContainer = styled.View`
  padding-top: 10;
  padding-bottom: 10;
`;

export const SwitchButton = styled.TouchableWithoutFeedback`

`;
