import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TitleBar from '../../modules/TitleBar';
import Switch from '../../elements/Switch';
import { setUser } from '../../../actions/user';
import { switchNotifications } from '../../../services/profile';

import {
  NotificationsContainer,
  NotificationField,
  NotificationLabel,
  NotificationLabelTitle,
  NotificationLabelText,
  SwitchContainer,
  SwitchButton,
} from './styles';

class NotificationsSettings extends Component {
  static propTypes = {
    user: PropTypes.shape({
      token: PropTypes.string,
      notifications: PropTypes.bool,
    }),
    setUser: PropTypes.func,
  }

  static defaultProps = {
    user: {
      token: null,
      notifications: true,
    },
    setUser: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      enabled: true,
    };

    this.handleEnabled = this.handleEnabled.bind(this);
  }

  componentWillMount() {
    const {
      notifications,
    } = this.props.user;

    if (!notifications) {
      this.setState({
        enabled: false,
      });
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }
  // eslint-disable-next-line
  onBackButtonPressAndroid = () => this.props.navigation.goBack();

  handleEnabled = () => {
    const {
      token,
    } = this.props.user;

    switchNotifications(token);

    if (this.state.enabled) {
      this.setState({
        enabled: false,
      });
      this.props.setUser({ notifications: false });
    } else {
      this.setState({
        enabled: true,
      });
      this.props.setUser({ notifications: true });
    }
  };

  render() {
    return (
      <NotificationsContainer>
        <TitleBar {...this.props} pageTitle="Notification settings" />
        <NotificationField>
          <NotificationLabel>
            <NotificationLabelTitle>
              Notifications
            </NotificationLabelTitle>
            <NotificationLabelText>
              Enable
            </NotificationLabelText>
          </NotificationLabel>
          <SwitchButton onPress={() => this.handleEnabled()}>
            <SwitchContainer>
              <Switch checked={this.state.enabled} />
            </SwitchContainer>
          </SwitchButton>
        </NotificationField>
      </NotificationsContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  setUser(userData) {
    dispatch(setUser(userData));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsSettings);
