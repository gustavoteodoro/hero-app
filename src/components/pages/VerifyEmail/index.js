import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { VerifyEmailContainer } from './styles';
import VerifyEmailContent from '../../modules/VerifyEmailContent';
import Logo from '../../elements/Logo';
import TextLink from '../../elements/TextLink';

// eslint-disable-next-line
class VerifyEmail extends Component {
  static propTypes = {
    user: PropTypes.shape({
      email: PropTypes.string,
    }),
  }

  static defaultProps = {
    user: null,
  }

  render() {
    return (
      <VerifyEmailContainer>
        <Logo />
        <VerifyEmailContent email={this.props.user.email} />
        <TextLink {...this.props} text="Back to login" page="Login" />
      </VerifyEmailContainer>
    );
  }
}

const propsFromStore = state => ({
  user: state.user,
});

export default connect(propsFromStore)(VerifyEmail);
