import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { lightGray } from '../../../styles/colors';

export const VerifyEmailContainer = styled.View`
  display: flex;
  flex-direction: column;
  width: ${width(100)};
  height: ${height(100)};
  padding-left: ${width(10)};
  padding-right: ${width(10)};
  justify-content: space-between;
  padding-bottom: 40;
  background: ${lightGray};
`;
