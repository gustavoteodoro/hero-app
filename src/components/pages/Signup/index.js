import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { SignupContainer } from './styles';
import SignupForm from '../../modules/SignupForm';
import Logo from '../../elements/Logo';

class SignUp extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    formReducer: PropTypes.shape({
      SignUp: PropTypes.object,
    }),
    setUser: PropTypes.func,
  }

  static defaultProps = {
    navigation: null,
    formReducer: null,
    setUser: null,
  }

  render() {
    return (
      <SignupContainer>
        <Logo />
        <SignupForm
          {...this.props}
        />
      </SignupContainer>
    );
  }
}

export default SignUp;
