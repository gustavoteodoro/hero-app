import styled from 'styled-components';
import { lightGray } from '../../../styles/colors';

export const SignupContainer = styled.View`
  display: flex;
  flex-direction: column;
  background-color: ${lightGray};
  height: 100%;
`;

