import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { lightGray } from '../../../styles/colors';
import { OpenSansRegular, OpenSansBold } from '../../../styles/fonts';

export const RequireFollowContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: ${width(100)};
  background-color: ${lightGray};
  height: 100%;
`;

export const RequireFollowContent = styled.View`
  width: ${width(84)};
  margin-left: ${width(8)};
  margin-right: ${width(8)};
`;

export const RequireFollowCourse = styled.View`
`;

export const RequireFollowCourseNameBox = styled.View`
  padding-top: 18;
  padding-bottom: 18;
  padding-left: 18;
  padding-right: 18;
  background: rgba(255,255,255,0.34);
  border-color: rgba(155,155,155,0.34);
  border-width: .5;
  border-radius: 4;
  margin-bottom: 80;
`;

export const RequireFollowText = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 16;
  width: ${width(40)};
  margin-bottom: 30;
`;

export const RequireFollowCourseName = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
`;

export const RequireFollowButtons = styled.View`
  width: ${width(100)};
  padding-left: ${width(8)};
  padding-right: ${width(8)};
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 48;
`;

export const RequireFollowButton = styled.View`
  width: 46%;
`;
