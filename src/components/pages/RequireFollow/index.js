import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { followCourse } from '../../../services/courses';
import TitleBar from '../../modules/TitleBar';
import Button from '../../elements/Button';

import {
  RequireFollowContainer,
  RequireFollowContent,
  RequireFollowCourse,
  RequireFollowText,
  RequireFollowCourseName,
  RequireFollowButtons,
  RequireFollowButton,
  RequireFollowCourseNameBox,
} from './styles';

class RequireFollow extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      goBack: PropTypes.func,
      state: PropTypes.object,
    }),
    currentUnit: PropTypes.shape({
      attributes: PropTypes.object,
    }),
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
  }

  static defaultProps = {
    navigation: null,
    currentUnit: null,
    user: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.handleFollow = this.handleFollow.bind(this);
  }

  handleCancel = () => {
    this.props.navigation.goBack();
  }

  handleFollow = () => {
    this.setState({
      loading: true,
    });

    const {
      user,
      currentUnit,
    } = this.props;

    const unitArray = [currentUnit];

    followCourse(user.token, unitArray)
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          this.setState({ loading: false });
          if (currentUnit.attributes.type === 'course') {
            this.props.navigation.navigate('RateCategory', { pageLink: 'Unit' });
          } else {
            this.props.navigation.navigate('RateRating', { pageLink: 'Unit' });
          }
        }
      });
  }

  render() {
    const {
      currentUnit,
    } = this.props;

    const {
      loading,
    } = this.state;

    return (
      <RequireFollowContainer>
        <TitleBar {...this.props} pageTitle="Add course to rate" />
        <RequireFollowContent>
          <RequireFollowCourse>
            <RequireFollowText>You need to follow the course to rate.</RequireFollowText>
            <RequireFollowCourseNameBox>
              <RequireFollowCourseName>
                {currentUnit.attributes.name}
              </RequireFollowCourseName>
            </RequireFollowCourseNameBox>
          </RequireFollowCourse>
        </RequireFollowContent>
        <RequireFollowButtons>
          <RequireFollowButton>
            <Button
              label="Cancel"
              accessibilityLabel="Cancel"
              ghost
              onPress={() => this.handleCancel()}
            />
          </RequireFollowButton>
          <RequireFollowButton>
            <Button
              label="Follow"
              accessibilityLabel="Follow"
              onPress={() => this.handleFollow()}
              loading={loading}
            />
          </RequireFollowButton>
        </RequireFollowButtons>
      </RequireFollowContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  currentUnit: state.currentUnit,
});

export default connect(mapStateToProps)(RequireFollow);
