import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { mediumGray, lightGray, whiteTwo, borderGrey } from '../../../styles/colors';
import { OpenSansRegular, OpenSansBold } from '../../../styles/fonts';

export const CourseSelectContainer = styled.View`
  display: flex;
  align-items: center;
  width: ${width(100)};
  background-color: ${lightGray};
  height: 100%;
`;

export const TextContainer = styled.View`
  width: ${width(80)};
  display: flex;
  flex-direction: column;
  margin-top: 100;
`;

export const SmallText = styled.Text`
  font-size: 14;
  line-height: 30;
  font-family: ${OpenSansRegular};
  color: ${mediumGray};
  width: 176;
`;

export const TitleBig = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 20;
  width: 176;
`;

export const ChooseDeparmentButton = styled.TouchableOpacity`
  width: ${width(80)};
  height: 55;
  background-color: ${whiteTwo};
  border: .5px solid ${borderGrey};
  border-radius: 4;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  margin-top: 100px;
`;

export const TextRegular = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 14;
  text-align: center;
  width: ${width(60)};
  margin-right: 20;
`;

export const TextBold = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
`;

export const ButtonPlus = styled.TouchableOpacity`
  display: flex;
`;

export const IconPlus = styled.Image`
  display: flex;
`;

export const ExplainingTextContainer = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 70;
`;

export const ExplainingTextRegular = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 12;
  text-align: center;
  width: ${width(60)};
  margin-right: 20;
  line-height: 22;
`;

export const ExplainingTextBold = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 12;
  line-height: 22;
`;

