import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  CourseSelectContainer,
  SmallText,
  TextContainer,
  TitleBig,
  ChooseDeparmentButton,
  TextRegular,
  IconPlus,
  ButtonPlus,
  ExplainingTextContainer,
  ExplainingTextRegular,
  ExplainingTextBold,
} from './styles';
import TitleBar from '../../modules/TitleBar';
import iconPlus from '../../../assets/plus_icon.png';

class AddCourse extends Component {
  static propTypes = {
    user: PropTypes.shape({
      name: PropTypes.string,
    }),
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
  }

  static defaultProps = {
    navigation: null,
    user: null,
  }

  redirect = (page) => {
    this.props.navigation.navigate(page);
  }

  render() {
    const {
      user,
      navigation,
    } = this.props;

    return (
      <CourseSelectContainer>
        <TitleBar {...this.props} pageTitle="Add course" pageLink="CreateUserName" />
        <TextContainer>
          <SmallText>Hi,</SmallText>
          <TitleBig>{user.name}</TitleBig>
        </TextContainer>
        <ChooseDeparmentButton onPress={() => navigation.navigate('DepartmentSelect')}>
          <TextRegular>
            Choose your primary department
          </TextRegular>
          <ButtonPlus>
            <IconPlus source={iconPlus} />
          </ButtonPlus>
        </ChooseDeparmentButton>
        <ExplainingTextContainer>
          <ExplainingTextBold>Why do we ask this?</ExplainingTextBold>
          <ExplainingTextRegular>
            The courses you add here are the courses you will be provide feedback on.
            This is a way for you to improve the course while your enrolled in it.
          </ExplainingTextRegular>
        </ExplainingTextContainer>
      </CourseSelectContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(AddCourse);
