import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BackHandler, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { ThreadContainer, ThreadContentWrap, ThreadContent, ThreadScroll, ThreadHeaderBar } from './styles';
import TitleBar from '../../modules/TitleBar';
import { listComments } from '../../../services/comments';
import { updateCurrentDiscussion } from '../../../actions/currentDiscussion';
import ThreadHeader from '../../modules/ThreadHeader';
import FeedItem from '../../elements/FeedItem';
import CallToInput from '../../elements/CallToInput';

class Thread extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      state: PropTypes.object,
      goBack: PropTypes.func,
    }),
    currentDiscussion: PropTypes.shape({
      uuid: PropTypes.string,
    }),
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
    updateCurrentDiscussion: PropTypes.func,
  }

  static defaultProps = {
    navigation: null,
    currentDiscussion: null,
    user: null,
    updateCurrentDiscussion: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      comments: [],
      headerMinified: false,
      refreshing: false,
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentWillMount() {
    this.handleUpdateComments();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => this.props.navigation.navigate('Home');

  handleScroll(event) {
    if (event.nativeEvent) {
      const contentHeight = event.nativeEvent.contentSize.height;
      const layoutHeight = event.nativeEvent.layoutMeasurement.height;
      if (event.nativeEvent.contentOffset.y <= 0) {
        if (this.state.headerMinified) {
          this.setState({
            headerMinified: false,
          });
        }
      } else if (!this.state.headerMinified && (contentHeight > layoutHeight)) {
        this.setState({
          headerMinified: true,
        });
      }
    }
  }

  handleUpdateComments() {
    this.setState({
      refreshing: true,
    });

    const {
      user,
      currentDiscussion,
      navigation,
    } = this.props;

    listComments(user.token, currentDiscussion.uuid)
      .then(res => res.json())
      .then((result) => {
        const finalResult = result.data.reverse();
        if (navigation.state.params) {
          if (navigation.state.params.commentAdded) {
            const commentAdded = finalResult.length ? [Object.assign(finalResult.slice(-1)[0], { newComment: true })] : [];
            const ohterComments = finalResult.length ? finalResult.slice(0, finalResult.length - 1) : [];
            this.setState({
              comments: commentAdded.concat(ohterComments),
              refreshing: false,
            });
          } else {
            this.setState({
              comments: finalResult,
              refreshing: false,
            });
          }
        } else {
          this.setState({
            comments: finalResult,
            refreshing: false,
          });
        }
      });
  }

  render() {
    const {
      headerMinified,
      comments,
    } = this.state;

    const {
      user,
      navigation,
      currentDiscussion,
    } = this.props;

    return (
      <ThreadContainer>
        <ThreadHeaderBar>
          <TitleBar {...this.props} pageTitle="Feed" pageLink={navigation.state.params ? navigation.state.params.pageLink : ''} />
        </ThreadHeaderBar>
        <ThreadScroll
          refreshControl={
            <RefreshControl
              onRefresh={() => this.handleUpdateComments()}
              refreshing={this.state.refreshing}
            />
          }
        >
          <ThreadHeader
            {...this.props}
            user={user}
            sugestion={currentDiscussion}
            minified={headerMinified}
            update={navigation.state.params ? navigation.state.params.update : null}
            onUpdate={discussion => this.props.updateCurrentDiscussion(discussion)}
          />
          <CallToInput
            onClick={() => this.props.navigation.navigate('AddComment')}
            label="Add a comment"
          />
          <ThreadContentWrap>
            <ThreadContent
              onScroll={e => this.handleScroll(e)}
              scrollEventThrottle={20}
              data={comments}
              keyExtractor={item => item.uuid}
              renderItem={({ item }) => (
                <FeedItem
                  isComment
                  sugestion={item}
                  discussionId={currentDiscussion.uuid}
                  itemId={item.uuid}
                  liked={item.liked}
                  currentUser={user}
                  token={user.token}
                  update={() => this.handleUpdateComments()}
                  // eslint-disable-next-line
                  onLike={liked => this.handleLike(currentDiscussion.uuid, liked, currentDiscussion.uuid, item.uuid)}
                />
                )}
            />
          </ThreadContentWrap>
        </ThreadScroll>
      </ThreadContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  currentDiscussion: state.currentDiscussion,
});

const mapDispatchToProps = dispatch => ({
  updateCurrentDiscussion(unit) {
    dispatch(updateCurrentDiscussion(unit));
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(Thread);
