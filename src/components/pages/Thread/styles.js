import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { lightGray, white } from '../../../styles/colors';

export const ThreadContainer = styled.View`
  display: flex;
  background: ${lightGray};
  height: ${height(100)};
`;

export const ThreadScroll = styled.ScrollView`

`;

export const ThreadHeaderBar = styled.View`
  background: ${lightGray};
  padding-bottom: 15;
  z-index: 20;
  box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.5);
`;

export const ThreadHeader = styled.View`
  display: flex;
  background: ${lightGray};
`;

export const ThreadContentWrap = styled.View`
  padding-bottom: 50;
  background: ${white};
`;

export const ThreadContent = styled.FlatList`
  display: flex;
  width: ${width(100)};
  background: ${white};
`;
