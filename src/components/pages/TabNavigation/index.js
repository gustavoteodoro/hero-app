// eslint-disable-next-line
import { FontAwesome, MaterialCommunityIcons } from 'react-native-vector-icons'; 
import React, { Component } from 'react';
import { Platform } from 'react-native';
import { TabNavigator, TabBarBottom, Animated, Easing, NavigationActions } from 'react-navigation';
import ContentNavigator from '../ContentNavigator';
import ProfileNavigator from '../ProfileNavigator';
import Qrscanner from '../Qrscanner';
import { HeroIcon } from '../../../styles/fonts';

const prefix = Platform.OS === 'android' ? 'heroapp://heroapp/+' : 'heroapp://+';

function resetStack(navigation, routes) {
  if (routes) {
    if (routes.length > 1) {
      const { routeName } = routes[0];
      navigation.dispatch(NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName })],
      }));
    }
  }
}

const TabNavigationWrapper = TabNavigator(
  {
    ContentNavigator: {
      screen: ContentNavigator,
    },
    ProfileNavigator: {
      screen: ProfileNavigator,
    },
    Qrscanner: {
      screen: Qrscanner,
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      // eslint-disable-next-line
      tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'ContentNavigator') {
          iconName = 'panel';
        } else if (routeName === 'ProfileNavigator') {
          iconName = 'profile';
        } else if (routeName === 'Qrscanner') {
          iconName = 'qr-code';
        }

        return <HeroIcon name={iconName} size={25} color={tintColor} />;
      },
      tabBarOnPress: ({ previousScene, scene, jumpToIndex }) => {
        resetStack(navigation, previousScene.routes);
        jumpToIndex(scene.index);
      },
    }),
    tabBarOptions: {
      activeTintColor: '#5a534a',
      inactiveTintColor: '#a5a5a5',
      showLabel: false,
      style: {
        height: 50,
        borderTopWidth: 0,
        backgroundColor: '#efe9e2',
      },
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    initialRouteName: 'ContentNavigator',
    swipeEnabled: false,
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0,
      },
    }),
  },
);

class TabNavigation extends Component {
  render() {
    return (
      <TabNavigationWrapper
        uriPrefix={prefix}
      />
    );
  }
}

export default TabNavigation;
