import { StackNavigator } from 'react-navigation';
import Home from '../Home';
import Thread from '../Thread';
import Unit from '../Unit';
import AddComment from '../AddComment';
import AddPost from '../AddPost';
import RateCategory from '../RateCategory';
import RateRating from '../RateRating';
import PostConfirm from '../PostConfirm';
import RequireFollow from '../RequireFollow';

const ContentNavigator = StackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        header: null,
      },
    },
    Thread: {
      screen: Thread,
      navigationOptions: {
        header: null,
      },
    },
    Unit: {
      screen: Unit,
      path: 'unit/:unitID',
      navigationOptions: {
        header: null,
      },
    },
    AddComment: {
      screen: AddComment,
      navigationOptions: {
        header: null,
      },
    },
    AddPost: {
      screen: AddPost,
      navigationOptions: {
        header: null,
      },
    },
    RateCategory: {
      screen: RateCategory,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    RateRating: {
      screen: RateRating,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    PostConfirm: {
      screen: PostConfirm,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    RequireFollow: {
      screen: RequireFollow,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
  },
  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerMode: 'screen',
    },
  },
);

export default ContentNavigator;
