import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// eslint-disable-next-line
import { Feather } from '@expo/vector-icons';
import Logo from '../../elements/Logo';
import Button from '../../elements/Button';
import { HeroIcon } from '../../../styles/fonts';
import { deleteUser } from '../../../services/account';
import { clearUser } from '../../../actions/user';

import {
  UserConfirmContainer,
  MessageContainer,
  MessageIcon,
  MessageTitle,
  MessageText,
  ButtonContainer,
  ButtonItem,
} from './styles';

class DeleteProfileConfirm extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      dispatch: PropTypes.func,
      navigate: PropTypes.func,
      goBack: PropTypes.func,
      state: PropTypes.shape({
        params: PropTypes.shape({
          verificationCode: PropTypes.string,
          rateConfirm: PropTypes.bool,
        }),
      }),
    }),
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
    clearUser: PropTypes.func,
  }

  static defaultProps = {
    navigation: null,
    user: null,
    clearUser: null,
  }

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleSubmit() {
    const {
      token,
    } = this.props.user;

    deleteUser(token)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.props.clearUser();
          this.props.navigation.navigate('Signup');
        }
      });
  }

  render() {
    return (
      <UserConfirmContainer>
        <Logo />
        <MessageContainer>
          <MessageIcon>
            <HeroIcon name="delete" size={18} color="black" />
          </MessageIcon>
          <MessageTitle>Are you sure?</MessageTitle>
          <MessageText>This action is irreversible.</MessageText>
        </MessageContainer>
        <ButtonContainer>
          <ButtonItem>
            <Button
              label="Yes"
              accessibilityLabel="Yes"
              large
              ghost
              onPress={() => this.handleSubmit()}
            />
          </ButtonItem>
          <Button
            label="No"
            accessibilityLabel="No"
            large
            onPress={() => this.props.navigation.goBack()}
          />
        </ButtonContainer>
      </UserConfirmContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  clearUser() {
    dispatch(clearUser());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteProfileConfirm);
