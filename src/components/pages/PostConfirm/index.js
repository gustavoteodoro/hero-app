import React, { Component } from 'react';
import { BackHandler, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
// eslint-disable-next-line
import { Feather } from '@expo/vector-icons';
import Logo from '../../elements/Logo';

import {
  UserConfirmContainer,
  TapAnywereText,
  MessageContainer,
  MessageIcon,
  MessageTitle,
  MessageText,
} from './styles';

class UserConfirm extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      state: PropTypes.shape({
        params: PropTypes.shape({
          verificationCode: PropTypes.string,
          rateConfirm: PropTypes.bool,
        }),
      }),
    }),
  }

  static defaultProps = {
    navigation: null,
  }

  constructor(props) {
    super(props);

    this.handlePress = this.handlePress.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => true;

  handlePress() {
    const {
      state,
    } = this.props.navigation;

    if (state.params) {
      if (state.params.rateConfirm) {
        this.props.navigation.navigate('Unit', {
          pageLink: 'Home',
        });
      } else if (state.params.unitOrigin) {
        this.props.navigation.navigate('Thread', {
          pageLink: 'Unit',
        });
      } else {
        this.props.navigation.navigate('Thread', {
          pageLink: 'Home',
        });
      }
    } else {
      this.props.navigation.navigate('Thread', {
        pageLink: 'Home',
      });
    }
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={() => this.handlePress()}>
        <UserConfirmContainer>
          <Logo />
          <MessageContainer>
            <MessageIcon>
              <Feather name="check" size={30} color="black" />
            </MessageIcon>
            <MessageTitle>Thank you!</MessageTitle>
            <MessageText>
              Every bit of feedback helps.
            </MessageText>
          </MessageContainer>
          <TapAnywereText>
            Tap anywere
          </TapAnywereText>
        </UserConfirmContainer>
      </TouchableWithoutFeedback>
    );
  }
}

export default UserConfirm;
