import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { greyishBrownTwo, aquaMarine, lightGray } from '../../../styles/colors';
import { OpenSansBold, NeuzeitBook } from '../../../styles/fonts';

export const UserConfirmContainer = styled.View`
  display: flex;
  flex-direction: column;
  width: ${width(100)};
  height: ${height(100)};
  padding-left: ${width(10)};
  padding-right: ${width(10)};
  justify-content: space-between;
  padding-bottom: 40;
  background: ${lightGray};
`;

export const TapAnywereText = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 12px;
  color: ${greyishBrownTwo};
  text-align: center;
  margin-bottom: 45;
`;

export const MessageContainer = styled.View`
  display: flex;
  align-items: center;
  margin-bottom: 40;
`;

export const MessageIcon = styled.View`
  width: ${width(20)};
  height: ${width(20)};
  align-items: center;
  justify-content: center;
  border-color: ${aquaMarine};
  border-width: 4;
  border-radius: ${width(50)};
  margin-bottom: 20;
`;

export const MessageTitle = styled.Text`
  font-size: 21;
  font-family: ${OpenSansBold};
  margin-bottom: 20;
`;

export const MessageText = styled.Text`
  font-size: 21;
  line-height: 27;
  font-family: ${NeuzeitBook};
  text-align: center;
  padding-left: 20;
  padding-right: 20;
`;
