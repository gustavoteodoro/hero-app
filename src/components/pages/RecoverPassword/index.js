import React, { Component } from 'react';
import { RecoverContainer } from './styles';
import RecoverForm from '../../modules/RecoverForm';
import TitleBar from '../../modules/TitleBar';

class RecoverPassword extends Component {
  render() {
    return (
      <RecoverContainer>
        <TitleBar {...this.props} pageTitle="Change the password" pageLink="Login" />
        <RecoverForm
          {...this.props}
        />
      </RecoverContainer>
    );
  }
}

export default RecoverPassword;
