import styled from 'styled-components';
import { lightGray } from '../../../styles/colors';

export const RecoverContainer = styled.View`
  display: flex;
  background-color: ${lightGray};
  height: 100%;
`;
