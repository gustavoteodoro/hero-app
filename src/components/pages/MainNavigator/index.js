import React, { Component } from 'react';
import { Platform } from 'react-native';
import { StackNavigator, SwitchNavigator } from 'react-navigation';
import TabNavigation from '../TabNavigation';
import Signup from '../Signup';
import VerifyEmail from '../VerifyEmail';
import Login from '../Login';
import RecoverPassword from '../RecoverPassword';
import AddCourse from '../AddCourse';
import CoursesAdded from '../CoursesAdded';
import DepartmentSelect from '../DepartmentSelect';
import CourseSelect from '../CourseSelect';
import VerifyEmailPassword from '../VerifyEmailPassword';
import OnBoard from '../OnBoard';
import UserConfirm from '../UserConfirm';
import AuthLoading from '../AuthLoading';
import CreateUserName from '../CreateUserName';
import TermsAndConditions from '../TermsAndConditions';
import Notification from '../Notification';

const prefix = Platform.OS === 'android' ? 'heroapp://heroapp/+' : 'heroapp://+';

class AuthNavigationWrapper extends Component {
  render() {
    return (
      <AuthNavigation
        uriPrefix={prefix}
      />
    );
  }
}

const SetupNavigation = StackNavigator(
  {
    AddCourse: {
      screen: AddCourse,
      navigationOptions: {
        header: null,
      },
    },
    CourseSelect: {
      screen: CourseSelect,
      navigationOptions: {
        header: null,
      },
    },
    DepartmentSelect: {
      screen: DepartmentSelect,
      navigationOptions: {
        header: null,
      },
    },
    CreateUserName: {
      screen: CreateUserName,
      navigationOptions: {
        header: null,
      },
    },
    CoursesAdded: {
      screen: CoursesAdded,
      navigationOptions: {
        header: null,
      },
    },
    TabNavigation: {
      screen: TabNavigation,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'CreateUserName',
    navigationOptions: {
      headerMode: 'screen',
    },
  },
);

const AuthNavigation = StackNavigator(
  {
    TabNavigation: {
      screen: TabNavigation,
      navigationOptions: {
        header: null,
      },
    },
    Setup: {
      screen: SetupNavigation,
    },
    Signup: {
      screen: Signup,
      navigationOptions: {
        header: null,
      },
    },
    VerifyEmail: {
      screen: VerifyEmail,
      navigationOptions: {
        header: null,
      },
    },
    RecoverPassword: {
      screen: RecoverPassword,
      navigationOptions: {
        gesturesEnabled: false,
        header: null,
      },
    },
    VerifyEmailPassword: {
      screen: VerifyEmailPassword,
      navigationOptions: {
        gesturesEnabled: false,
        header: null,
      },
    },
    Login: {
      screen: Login,
      navigationOptions: {
        gesturesEnabled: false,
        header: null,
      },
    },
    OnBoard: {
      screen: OnBoard,
      navigationOptions: {
        header: null,
      },
    },
    TermsAndConditions: {
      screen: TermsAndConditions,
      navigationOptions: {
        header: null,
      },
    },
    UserConfirm: {
      screen: UserConfirm,
      path: 'userconfirm/:verificationCode',
      navigationOptions: {
        gesturesEnabled: false,
        header: null,
      },
    },
  },
  {
    initialRouteName: 'OnBoard',
    navigationOptions: {
      headerMode: 'screen',
    },
  },
);


const MainNavigator = SwitchNavigator(
  {
    AuthLoading,
    App: TabNavigation,
    Auth: AuthNavigationWrapper,
    Setup: SetupNavigation,
    Notification,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

export default MainNavigator;
