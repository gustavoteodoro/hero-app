import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import PropTypes from 'prop-types';
import Swiper from 'react-native-swiper';

import styles from './styles';
import Logo from '../../elements/Logo';
import Button from '../../elements/Button';

import card1 from '../../../assets/onboard_card1.png';
import card2 from '../../../assets/onboard_card2.png';
import card3 from '../../../assets/onboard_card3.png';
import baloon1 from '../../../assets/onboard_balloon1.png';
import baloon2 from '../../../assets/onboard_balloon2.png';

export default class OnBoard extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
  }

  static defaultProps = {
    navigation: null,
  }

  redirect = (page) => {
    this.props.navigation.navigate(page);
  }

  renderDots = () => (
    <View style={styles.dots} />
  )

  renderActiveDots = () => (
    <View style={styles.activeDots} />
  )

  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.logo}>
          <Logo />
        </View>
        <View style={styles.swiperContainer}>
          <Swiper
            dot={this.renderDots()}
            activeDot={this.renderActiveDots()}
            paginationStyle={styles.pagination}
            removeClippedSubviews={false}
            containerStyle={styles.swiper}
            loop={false}
          >
            <View style={styles.slide}>
              <Text style={styles.text}>Evaluate courses and your university experice.</Text>
              <Image style={styles.boards} source={card1} />
              <Image style={styles.boards} source={card2} />
              <Image style={styles.boards} source={card3} />
            </View>
            <View style={styles.slide}>
              <Text style={styles.text}>
                Make sugestions, discuss and up vote,
                to improve your university experice.
              </Text>
              <Image style={styles.balloons} source={baloon1} />
              <Image style={styles.balloonsRight} source={baloon2} />
              <Image style={styles.balloons} source={baloon1} />
            </View>
          </Swiper>
        </View>
        <Button
          style={styles.button}
          label="Get started"
          accessibilityLabel="Get started"
          onPress={() => this.redirect('TermsAndConditions')}
        />
      </View>
    );
  }
}

