import { StyleSheet, Dimensions } from 'react-native';
import { lightGray } from '../../../styles/colors';

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: 'center',
  },
  images1: {
    width: 200,
    marginBottom: 10,
  },
  balloons: {
    marginBottom: 10,
    flex: 0,
    height: window.height * 0.12,
    resizeMode: 'contain',
  },
  balloonsRight: {
    marginBottom: 10,
    marginLeft: 35,
  },
  boards: {
    marginBottom: 10,
    flex: 0,
    height: window.height * 0.12,
    resizeMode: 'contain',
  },
  logo: {
    marginBottom: window.height * 0.035,
  },
  text: {
    marginBottom: 30,
    width: 215,
    textAlign: 'center',
    fontSize: 16,
  },
  button: {
    backgroundColor: 'rgba(80,227,194,1)',
    width: 200,
    marginBottom: 10,
  },
  dots: {
    backgroundColor: 'rgba(155,155,155,1)',
    width: 11,
    height: 11,
    borderRadius: 10,
    margin: 8,
  },
  activeDots: {
    backgroundColor: 'rgba(80,227,194,1)',
    width: 11,
    height: 11,
    borderRadius: 10,
    margin: 8,
  },
  pagination: {
    height: 11,
  },
  wrapper: {
    height: window.height,
    width: window.width,
    flex: 1,
    alignItems: 'center',
    backgroundColor: lightGray,
  },
  swiper: {
    width: window.width,
  },
  swiperContainer: {
    maxHeight: window.height * 0.65,
  },
});

export default styles;
