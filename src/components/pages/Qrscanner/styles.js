import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { aquaMarine, warmGreyTwo, black, white } from '../../../styles/colors';
import { OpenSansBold, OpenSansRegular } from '../../../styles/fonts';

export const LoadingContainer = styled.View`
  justify-content: center;
  height: 100%;
  background: ${white};
`;

export const QRContainer = styled.View`
  height: ${height(100)};
`;

export const QrScannerContainer = styled.View`
  height: ${height(100)};
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: ${black};
`;

export const QrscannerHeader = styled.View`
  position: absolute;
  top: 0;
  width: 100%;
  background: ${aquaMarine};
  z-index: 30;
  box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.5);
`;

export const HeaderTitle = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 12;
  color: ${black};
  text-align: center;
  margin-top: 40;
`;
export const HeaderDescription = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 12;
  color: ${warmGreyTwo};
  text-align: center;
  margin-top: 10;
  margin-bottom: 20;
`;

export const QrScannerSample = styled.View`
  position: absolute;
  top: ${width(60)};
  width: ${width(56)};
  height: ${width(56)};
  background: ${white};
  border-radius: 4;
  opacity: 0.6;
  z-index: 30;
  align-items: center;
  justify-content: center;
`;

export const QrScannerSampleImage = styled.Image`
  width: ${width(36)};
  height: ${width(36)};
`;
