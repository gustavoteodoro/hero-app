import React, { Component } from 'react';
import { BarCodeScanner, Permissions } from 'expo';
import { connect } from 'react-redux';
import {
  ActivityIndicator,
  Alert,
  Linking,
  Dimensions,
  Text,
  StyleSheet,
} from 'react-native';
import QrCodeSample from '../../../assets/qrcode-sample.png';
import { updateCurrentUnit } from '../../../actions/currentUnit';
import { unitByDeeplink } from '../../../services/courses';

import {
  QRContainer,
  LoadingContainer,
  QrScannerContainer,
  QrscannerHeader,
  HeaderTitle,
  HeaderDescription,
  QrScannerSample,
  QrScannerSampleImage,
} from './styles';

/* eslint-disable */

class Qrscanner extends Component {
  state = {
    hasCameraPermission: null,
    lastScannedUrl: null,
  };

  componentDidMount() {
    this._requestCameraPermission();
  }

  _requestCameraPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  };

  _handleBarCodeRead = (result) => {
    if (result.data !== this.state.lastScannedUrl) {
      this.setState({ lastScannedUrl: result.data });
      if(this.props.navigation.isFocused()) {
        unitByDeeplink(this.props.user.token, result.data)
        .then(res => res.json())
        .then(resultFetch => {
          if(resultFetch.status){
            this.props.updateCurrentUnit(resultFetch.data);
            setTimeout(() => this.props.navigation.navigate('Unit', { pageLink: 'Home'} ), 2000);
            setTimeout(() => this.setState({ lastScannedUrl: null}), 5000);
          }
        })
      }
    }
  };

  _maybeRenderUrl = () => {
    if (!this.state.lastScannedUrl) {
      return;
    }
  };

  _handlePressUrl = () => {
    Alert.alert(
      'Open this URL?',
      this.state.lastScannedUrl,
      [
        {
          text: 'Yes',
          onPress: () => Linking.openURL(this.state.lastScannedUrl),
        },
        { text: 'No', onPress: () => {} },
      ],
      { cancellable: false },
    );
  };

  _handlePressCancel = () => {
    this.setState({ lastScannedUrl: null });
  };
  render() {
    const {
      key,
    } = this.props.navigation.state;

    return (
      <QRContainer>
        {!(key === 'Qrscanner') &&
          <LoadingContainer>
            <ActivityIndicator size="large" color="rgb(80,227,194)" />
          </LoadingContainer>        
        }
        {(key === 'Qrscanner') &&
          <QrScannerContainer>
            <QrscannerHeader>
              <HeaderTitle>Scan QR code to rate or post</HeaderTitle>
              <HeaderDescription>Thank you for the feedback!</HeaderDescription>
            </QrscannerHeader>
            <QrScannerSample>
              <QrScannerSampleImage source={QrCodeSample} />
            </QrScannerSample>
            {this.state.hasCameraPermission === null
              ? <Text>Requesting for camera permission</Text>
              : this.state.hasCameraPermission === false
                  ? <Text style={{ color: '#fff' }}>
                      Camera permission is not granted
                    </Text>
                  : <BarCodeScanner
                    onBarCodeRead={this._handleBarCodeRead}
                    style={{
                        height: Dimensions.get('window').height,
                        width: Dimensions.get('window').width,
                      }}
                  />}

            {this._maybeRenderUrl()}
          </QrScannerContainer>
        }
      </QRContainer>
    );
  }
}

const styles = StyleSheet.create({
  bottomBar: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0.5)',
    padding: 15,
    flexDirection: 'row',
  },
  url: {
    flex: 1,
  },
  urlText: {
    color: '#fff',
    fontSize: 20,
  },
  cancelButton: {
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cancelButtonText: {
    color: 'rgba(255,255,255,0.8)',
    fontSize: 18,
  },
});


const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  updateCurrentUnit(unit) {
    dispatch(updateCurrentUnit(unit));
  },
});

export default connect(mapStateToProps,mapDispatchToProps)(Qrscanner);
