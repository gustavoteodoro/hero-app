import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import PropTypes from 'prop-types';
import {
  TermsContainer,
  TitleContainer,
  TermsContent,
  TermsBackground,
  LogoContainer,
} from './styles';
import ScrollViewText from '../../elements/ScrollViewText';
import Logo from '../../elements/Logo';
import TitleMedium from '../../elements/TitleMedium';
import TextLink from '../../elements/TextLink';
import Button from '../../elements/Button';
import TitleBar from '../../modules/TitleBar';

import { terms } from '../TermsAndConditions/data.json';

export default class ProfileTermsAndConditions extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
  }

  static defaultProps = {
    navigation: null,
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }
  // eslint-disable-next-line
  onBackButtonPressAndroid = () => this.props.navigation.goBack();

  redirect = (page) => {
    this.props.navigation.navigate(page);
  }

  render() {
    return (
      <TermsBackground>
        <TermsContainer>
          <TitleBar {...this.props} />
          <LogoContainer>
            <Logo />
          </LogoContainer>
          <TitleContainer>
            <TitleMedium titleText="Terms and Conditions" />
          </TitleContainer>
          <TermsContent>
            {terms}
          </TermsContent>
        </TermsContainer>
      </TermsBackground>
    );
  }
}
