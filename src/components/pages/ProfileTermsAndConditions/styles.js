import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { lightGray, greyishBrown } from '../../../styles/colors';
import { OpenSansRegular } from '../../../styles/fonts';

export const TermsBackground = styled.ScrollView`
  height: 100%;
  background-color: ${lightGray};
`;

export const TermsContainer = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${lightGray};
`;

export const LogoContainer = styled.View`
  margin-top: -50;
`;

export const TitleContainer = styled.View`
  margin-top: 25;
`;

export const TermsContent = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 12;
  line-height: 22;
  text-align: center;
  color: ${greyishBrown};
  padding-left: ${width(13)};
  padding-right: ${width(13)};
  margin-top: 25;
  margin-bottom: 50;
`;
