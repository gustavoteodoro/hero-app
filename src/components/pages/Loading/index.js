import React, { Component } from 'react';
import { Animated } from 'react-native';
import { LoadingContainer, LoadingImage } from './styles';
import Splash from '../../../assets/hero-splash.gif';

class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      opacity: new Animated.Value(0),
    };
  }
  render() {
    const {
      opacity,
    } = this.state;

    Animated.timing(
      this.state.opacity,
      {
        toValue: opacity ? 1 : 0,
        duration: 200,
      },
    ).start();

    return (
      <LoadingContainer>
        <Animated.View
          style={{
            opacity,
          }}
        >
          <LoadingImage
            style={{ width: 96, height: 96 }}
            source={Splash}
          />
        </Animated.View>
      </LoadingContainer>
    );
  }
}

export default Loading;
