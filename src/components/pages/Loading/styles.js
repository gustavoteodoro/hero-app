import styled from 'styled-components';

import { height } from 'react-native-dimension';

export const LoadingContainer = styled.View`
  height: ${height(100)};
  background: #dbd5ce;
  align-items: center;
  justify-content: center;
`;

export const LoadingImage = styled.Image`

`;
