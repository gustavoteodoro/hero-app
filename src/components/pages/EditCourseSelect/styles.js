import styled, { css } from 'styled-components';
import { width, height } from 'react-native-dimension';
import { warmGrey, mediumGray, lightGray, white } from '../../../styles/colors';
import { OpenSansBold } from '../../../styles/fonts';


export const CourseSelectContainer = styled.View`
  display: flex;
  width: ${width(100)};
  height: ${height(100)};
  align-items: center;
  background-color: ${white};
`;

export const HeaderContainer = styled.View`
  display: flex;
  width: ${width(100)};
  background-color: ${lightGray};
  height: ${height(24)};
  align-items: center;
  box-shadow: 0px 1px 8px ${mediumGray};
`;

export const SearchContainer = styled.View`
  width: ${width(80)};
`;

export const ChooseTableContainer = styled.ScrollView`
  width: ${width(100)};
  padding-left: ${width(10)};
  padding-right: ${width(10)};
  padding-top: 50;
`;

export const DeparmentContainer = styled.View`
  width: ${width(80)};
  margin-bottom: 200;
`;

export const TitleDeparment = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
  color: ${warmGrey};
  height: 44;
`;

export const ButtonContainer = styled.View`
  display: none;
  width: ${width(100)};
  position: absolute;
  bottom: 0;
  background: ${white};
  padding-left: ${width(10)};
  padding-right: ${width(10)};
  padding-top: 15;
  padding-bottom: 15;
  box-shadow: 0px 1px 4px ${mediumGray};

  ${props => props.visible && css`
    display: flex;
  `}
`;

export const LoadingContainer = styled.View`
  width: ${width(100)};
  height: ${height(76)};
  background: ${white};
  justify-content: center;
`;

