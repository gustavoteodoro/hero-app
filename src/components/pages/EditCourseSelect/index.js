import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  CourseSelectContainer,
  HeaderContainer,
  SearchContainer,
  DeparmentContainer,
  TitleDeparment,
  ChooseTableContainer,
  ButtonContainer,
  LoadingContainer,
} from './styles';
import { followCourse, unfollowCourse, coursesByDepartment } from '../../../services/courses';
import TitleBar from '../../modules/TitleBar';
import Button from '../../elements/Button';
import PureInput from '../../elements/PureInput';
import CourseButton from '../../elements/CourseButton';

class EditCourseSelect extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      state: PropTypes.object,
    }),
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
  }

  static defaultProps = {
    navigation: null,
    user: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedCourses: [],
      allCourses: {},
      initialCourses: {},
    };
    this.handleCheckSelected = this.handleCheckSelected.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.selectCoursesFollowed = this.selectCoursesFollowed.bind(this);
  }

  componentWillMount() {
    this.setState({
      loading: true,
    });

    const {
      department,
      selectedCourses,
    } = this.props.navigation.state.params;

    if (selectedCourses[0]) {
      this.setState({ selectedCourses });
    }

    coursesByDepartment(this.props.user.token, department.uuid)
      .then(res => res.json())
      .then((result) => {
        this.setState({
          allCourses: result.data,
          initialCourses: _.cloneDeep(result.data),
          loading: false,
        });
        this.selectCoursesFollowed(_.cloneDeep(result.data));
      });
  }

  onPressButton = (buttonId) => {
    if (this.isSelected(buttonId)) {
      const index = this.state.selectedCourses.indexOf(buttonId);
      const selectedCourses = _.cloneDeep(this.state.selectedCourses);
      selectedCourses.splice(index, 1);
      this.handleUnfollowCourse(buttonId);
      this.setState({ selectedCourses });
    } else {
      const newArray = _.cloneDeep(this.state.selectedCourses);
      newArray.push(buttonId);
      this.setState({ selectedCourses: newArray });
    }
  }

  selectCoursesFollowed(department) {
    const newArray = _.cloneDeep(this.state.selectedCourses);
    department.map((course) => {
      if (course.followed) {
        newArray.push(course.uuid);
      }
    });
    this.setState({ selectedCourses: newArray });
  }

  isSelected = (buttonId) => {
    const index = this.state.selectedCourses.indexOf(buttonId);
    if (index > -1) {
      return true;
    }
    return false;
  }

  handleSearchChange(event) {
    const {
      allCourses,
      initialCourses,
    } = this.state;

    if (event === '') {
      this.setState({ allCourses: initialCourses });
    } else {
      allCourses.map((course, index) => {
        const courseName = course.attributes.name.toLowerCase();
        if (courseName.indexOf(event) < 0) {
          const newArrayCourses = _.cloneDeep(allCourses);
          newArrayCourses.splice(index);
          const newGeneralCourses = _.cloneDeep(allCourses);
          this.setState({ allCourses: newGeneralCourses });
        }
      });
    }
  }

  handleUnfollowCourse(courseId) {
    const {
      user,
    } = this.props;

    const courses = [courseId];

    unfollowCourse(user.token, courses);
  }

  handleCheckSelected = (courseToCheck, courseId) => courseToCheck === courseId;

  handleSubmit() {
    const {
      selectedCourses,
    } = this.state;

    const {
      token,
    } = this.props.user;

    followCourse(token, selectedCourses)
      .then(res => res.json())
      .then((result) => {
        if (result.status || result.error.code === 400) {
          this.props.navigation.navigate('EditCourses');
        }
      });
  }

  render() {
    const {
      allCourses,
      selectedCourses,
      loading,
    } = this.state;

    const {
      department,
    } = this.props.navigation.state.params;

    return (
      <CourseSelectContainer>
        <HeaderContainer>
          <TitleBar {...this.props} pageTitle="Add your course (s)" />
          <SearchContainer>
            <PureInput
              name="email"
              label="Search"
              autoCapitalize="none"
              onChangeText={this.handleSearchChange}
            />
          </SearchContainer>
        </HeaderContainer>
        {loading &&
          <LoadingContainer>
            <ActivityIndicator size="large" color="#50e3c2" />
          </LoadingContainer>
        }
        {!loading &&
          <ChooseTableContainer>
            <DeparmentContainer>
              <TitleDeparment>{department.attributes.name}</TitleDeparment>
              {allCourses.map(course => (
                <CourseButton
                  key={course.uuid}
                  titleCourse={course.attributes.name}
                  onPressButton={() => this.onPressButton(course.uuid)}
                  /* eslint-disable-next-line */
                  selected={selectedCourses.find(courseToCheck => this.handleCheckSelected(courseToCheck, course.uuid))}
                />
              ))}
            </DeparmentContainer>
          </ChooseTableContainer>
        }
        <ButtonContainer visible={selectedCourses[0]}>
          <Button
            label="Next"
            accessibilityLabel="Next"
            onPress={() => this.handleSubmit()}
          />
        </ButtonContainer>
      </CourseSelectContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(EditCourseSelect);
