const response = [
  {
    status: true,
    data: [
      {
        uuid: '0188b335-2494-4c9f-a394-e19f72f23ae5',
        attributes: {
          type: 'department',
          name: 'Faculty of Health and Medical Sciences',
        },
        created_at: '2018-03-19 02:30:54',
        updated_at: '2018-03-19 02:30:54',
        children: [
          {
            uuid: '45dad71b-d36f-4ce6-8a07-81df27aa0095',
            attributes: {
              type: 'department',
              name: 'BRIC',
            },
            created_at: '2018-03-19 02:30:54',
            updated_at: '2018-03-19 02:30:54',
            children: [
              {
                uuid: '1d889557-d6ac-4be2-91f4-4d925e5051d9',
                attributes: {
                  type: 'course',
                  name: 'Commercialisation of Biomedical Innovations',
                },
                created_at: '2018-03-19 02:30:54',
                updated_at: '2018-03-19 02:30:54',
                children: [],
                isUnit: true,
                university: '0d56aee8-80ae-40d7-bb65-c2a9ab85d757',
                parent: '45dad71b-d36f-4ce6-8a07-81df27aa0095',
              },
              {
                uuid: '6eaf196d-a22d-4ba9-90c9-a4f9aba69cf4',
                attributes: {
                  type: 'course',
                  name: 'Course in General Pathology',
                },
                created_at: '2018-03-19 02:30:54',
                updated_at: '2018-03-19 02:30:54',
                children: [],
                isUnit: true,
                university: '0d56aee8-80ae-40d7-bb65-c2a9ab85d757',
                parent: '45dad71b-d36f-4ce6-8a07-81df27aa0095',
              },
            ],
            isUnit: false,
            university: '0d56aee8-80ae-40d7-bb65-c2a9ab85d757',
            parent: '0188b335-2494-4c9f-a394-e19f72f23ae5',
          },
        ],
        isUnit: false,
        university: '0d56aee8-80ae-40d7-bb65-c2a9ab85d757',
        parent: null,
      },
    ],
  },
];

export default response;
