import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { lightGray, whiteTwo, borderGrey, mediumGray } from '../../../styles/colors';
import { OpenSansRegular, OpenSansBold } from '../../../styles/fonts';

export const CourseSelectContainer = styled.View`
  display: flex;
  align-items: center;
  width: ${width(100)};
  background-color: ${lightGray};
  height: 100%;
`;

export const CourseSelectHeader = styled.View`
  box-shadow: 0px 1px 8px ${mediumGray};
  padding-bottom: 15;
  z-index: 99;
`;

export const CourseSelectContent = styled.ScrollView`
  width: ${width(100)};
`;

export const ChooseDeparmentButton = styled.TouchableOpacity`
  width: ${width(80)};
  margin-left: ${width(10)};
  margin-right: ${width(10)};
  height: 55;
  background-color: ${whiteTwo};
  border: .5px solid ${borderGrey};
  border-radius: 4;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  margin-top:0;
`;

export const TextRegular = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 14;
  text-align: left;
  width: ${width(60)};
  margin-right: 20;
`;

export const TextBold = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
  max-width: 200px;
`;

export const ButtonPlus = styled.TouchableOpacity`
  display: flex;
`;

export const IconPlus = styled.Image`
  display: flex;
`;

export const ButtonContainer = styled.View`
  width: ${width(80)};
  margin-left: ${width(10)};
  margin-top: 40;
  margin-bottom: 40;
`;

export const LoadingContainer = styled.View`
  height: ${height(80)};
  margin-bottom: 100;
  justify-content: center;
`;
