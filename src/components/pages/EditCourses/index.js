import React, { Component } from 'react';
import { ActivityIndicator, BackHandler } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  CourseSelectContainer,
  ChooseDeparmentButton,
  TextRegular,
  IconPlus,
  ButtonPlus,
  ButtonContainer,
  CourseSelectHeader,
  CourseSelectContent,
  LoadingContainer,
} from './styles';
import { updateCourses } from '../../../actions/courses';
import { unfollowCourse, listCoursesFollowed } from '../../../services/courses';
import TitleBar from '../../modules/TitleBar';
import MyCourses from '../../modules/MyCourses';
import iconPlus from '../../../assets/plus_icon.png';
import Button from '../../elements/Button';

class EditCourses extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    courses: PropTypes.arrayOf(PropTypes.object),
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
    updateCourses: PropTypes.func,
  }

  static defaultProps = {
    navigation: null,
    user: {},
    courses: null,
    updateCourses: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      departments: [],
    };
  }

  componentDidMount() {
    this.handleUpdateFollowedDepartment();
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => this.props.navigation.navigate('Profile');

  handleUpdateFollowedDepartment(noLoad = false) {
    if (!noLoad) {
      this.setState({
        loading: true,
      });
    }

    const {
      user,
    } = this.props;

    listCoursesFollowed(user.token)
      .then(res => res.json())
      .then((result) => {
        const currentCourses = [];
        result.data.map((department) => {
          department.children.map(course => currentCourses.push(course));
        });
        this.props.updateCourses(currentCourses);
        this.setState({
          loading: false,
          departments: result.data,
        });
      });
  }

  handleUnfollowCourse(courseId) {
    const {
      user,
    } = this.props;

    const courses = [courseId];

    unfollowCourse(user.token, courses)
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          this.handleUpdateFollowedDepartment(true);
        }
      });
  }

  redirect = (page) => {
    this.props.navigation.navigate(page);
  }

  handleSubmit() {
    this.props.navigation.navigate('Profile');
  }

  render() {
    const {
      navigation,
    } = this.props;

    const {
      loading,
      departments,
    } = this.state;

    return (
      <CourseSelectContainer>
        <CourseSelectHeader>
          <TitleBar {...this.props} pageTitle="Edit courses" pageLink="Profile" />
        </CourseSelectHeader>
        {!loading &&
          <CourseSelectContent>
            <MyCourses
              departments={departments}
              onUnfollow={course => this.handleUnfollowCourse(course)}
            />
            <ChooseDeparmentButton onPress={() => navigation.navigate('EditDepartmentSelect')}>
              <TextRegular>
                Add more courses
              </TextRegular>
              <ButtonPlus>
                <IconPlus source={iconPlus} />
              </ButtonPlus>
            </ChooseDeparmentButton>
            <ButtonContainer>
              <Button
                label="Save"
                accessibilityLabel="Save"
                onPress={() => this.handleSubmit()}
              />
            </ButtonContainer>
          </CourseSelectContent>
        }
        {loading &&
          <LoadingContainer>
            <ActivityIndicator size="large" color="#50e3c2" />
          </LoadingContainer>
        }
      </CourseSelectContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  courses: state.courses,
});

const mapDispatchToProps = dispatch => ({
  updateCourses(courses) {
    dispatch(updateCourses(courses));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(EditCourses);
