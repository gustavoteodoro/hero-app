import { StackNavigator } from 'react-navigation';
import Profile from '../Profile';
import EditCourses from '../EditCourses';
import EditCourseSelect from '../EditCourseSelect';
import EditDepartmentSelect from '../EditDepartmentSelect';
import UpdateUserName from '../UpdateUserName';
import UpdatePassword from '../UpdatePassword';
import ProfileTermsAndConditions from '../ProfileTermsAndConditions';
import DeleteProfileConfirm from '../DeleteProfileConfirm';
import NotificationsSettings from '../NotificationsSettings';
import RecoverPassword from '../RecoverPassword';
import VerifyEmail from '../VerifyEmail';
import VerifyEmailPassword from '../VerifyEmailPassword';
import Signup from '../Signup';
import Login from '../Login';

const ProfileNavigator = StackNavigator(
  {
    Profile: {
      screen: Profile,
      navigationOptions: {
        header: null,
      },
    },
    EditCourses: {
      screen: EditCourses,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    EditCourseSelect: {
      screen: EditCourseSelect,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    EditDepartmentSelect: {
      screen: EditDepartmentSelect,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    UpdateUserName: {
      screen: UpdateUserName,
      navigationOptions: {
        header: null,
      },
    },
    UpdatePassword: {
      screen: UpdatePassword,
      navigationOptions: {
        header: null,
      },
    },
    ProfileTermsAndConditions: {
      screen: ProfileTermsAndConditions,
      navigationOptions: {
        header: null,
      },
    },
    DeleteProfileConfirm: {
      screen: DeleteProfileConfirm,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    NotificationsSettings: {
      screen: NotificationsSettings,
      navigationOptions: {
        header: null,
      },
    },
    Signup: {
      screen: Signup,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    Login: {
      screen: Login,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    RecoverPassword: {
      screen: RecoverPassword,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    VerifyEmail: {
      screen: VerifyEmail,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    VerifyEmailPassword: {
      screen: VerifyEmailPassword,
      navigationOptions: {
        header: null,
        tabBarVisible: false,
      },
    },
    TabNavigation: {
      screen: Profile,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'Profile',
    navigationOptions: {
      headerMode: 'screen',
    },
  },
);

export default ProfileNavigator;
