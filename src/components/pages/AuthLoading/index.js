import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { userProfile, userNotifications } from '../../../services/profile';
import Loading from '../Loading';

class AuthLoading extends Component {
  static propTypes = {
    user: PropTypes.shape({
      token: PropTypes.string,
      name: PropTypes.string,
    }),
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
  }

  static defaultProps = {
    user: null,
    navigation: null,
  }

  componentWillMount() {
    if (this.props.user) {
      if (this.props.user.token) {
        userProfile(this.props.user.token)
          .then(res => res.json())
          .then((result) => {
            if (result.status) {
              if (this.props.user.name) {
                userNotifications(this.props.user.token)
                  .then(res => res.json())
                  .then((resultNotification) => {
                    if (resultNotification.data[0]) {
                      setTimeout(() => this.props.navigation.navigate('Notification', { message: resultNotification.data[0].message }), 5000);
                    } else {
                      setTimeout(() => this.props.navigation.navigate('App'), 5000);
                    }
                  });
              } else {
                setTimeout(() => this.props.navigation.navigate('Setup'), 5000);
              }
            } else {
              setTimeout(() => this.props.navigation.navigate('Auth'), 5000);
            }
          });
      } else {
        setTimeout(() => this.props.navigation.navigate('Auth'), 5000);
      }
    }
  }

  render() {
    return (
      <Loading />
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(AuthLoading);
