import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { aquaMarine } from '../../../styles/colors';
import { OpenSansBold, OpenSansRegular, NeuzeitBook } from '../../../styles/fonts';

export const UserConfirmContainer = styled.View`
  display: flex;
  flex-direction: column;
  width: ${width(80)};
  height: ${height(100)};
  margin-left: ${width(10)};
  margin-right: ${width(10)};
  justify-content: space-between;
  padding-bottom: 40;
`;

export const MessageContainer = styled.View`
  display: flex;
  align-items: center;
  margin-bottom: 40;
`;

export const MessageIcon = styled.View`
  width: ${width(20)};
  height: ${width(20)};
  align-items: center;
  justify-content: center;
  border-color: ${aquaMarine};
  border-width: 4;
  border-radius: ${width(50)};
  margin-bottom: 20;
`;

export const MessageEmail = styled.Text`
  font-size: 15;
  font-family: ${OpenSansBold};
  margin-bottom: 25;
`;

export const MessageTitle = styled.Text`
  font-size: 21;
  font-family: ${NeuzeitBook};
  margin-bottom: 20;
  width: ${width(50)}
  text-align: center;
`;

export const MessageText = styled.Text`
  font-size: 15;
  line-height: 27;
  font-family: ${OpenSansRegular};
  text-align: center;
  padding-left: 20;
  padding-right: 20;
`;
