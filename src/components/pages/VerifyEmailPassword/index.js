import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import PropTypes from 'prop-types';
// eslint-disable-next-line
import { MaterialIcons } from '@expo/vector-icons';
import Logo from '../../elements/Logo';
import Button from '../../elements/Button';
import {
  UserConfirmContainer,
  MessageContainer,
  MessageIcon,
  MessageTitle,
  MessageEmail,
  MessageText,
} from './styles';

class VerifyEmailPassword extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      state: PropTypes.shape({
        params: PropTypes.shape({
          email: PropTypes.string,
        }),
      }),
    }),
  }

  static defaultProps = {
    navigation: null,
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => true;

  render() {
    const { email } = this.props.navigation.state.params;
    return (
      <UserConfirmContainer>
        <Logo />
        <MessageContainer>
          <MessageIcon>
            <MaterialIcons name="email" size={30} color="black" />
          </MessageIcon>
          <MessageTitle>You need to verify your e-mail</MessageTitle>
          {email &&
            <MessageEmail>{email}</MessageEmail>
          }
          <MessageText>
            An e-mail has been sent with your new password. Check you e-mail and login.
          </MessageText>
        </MessageContainer>
        <Button
          label="Login"
          onPress={() => this.props.navigation.navigate('Login')}
        />
      </UserConfirmContainer>
    );
  }
}

export default VerifyEmailPassword;
