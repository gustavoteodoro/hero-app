import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import { ActivityIndicator } from 'react-native';
import {
  DepartmentSelectContainer,
  HeaderContainer,
  SearchContainer,
  ChooseTableContainer,
  TitleFaculty,
  TitleDepartment,
  DepartmentContainer,
  PathIcon,
  ButtonChoose,
  FalcultyContainer,
  DepartmentPadding,
  LoadingContainer,
  ItemContainer,
  ButtonContainer,
} from './styles';
import { allDepartments } from '../../../services/departments';
import { followCourse, unfollowCourse } from '../../../services/courses';
import TitleBar from '../../modules/TitleBar';
import Button from '../../elements/Button';
import PureInput from '../../elements/PureInput';
import CourseButton from '../../elements/CourseButton';
import path from '../../../assets/path.png';

class DepartmentSelect extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
  }

  static defaultProps = {
    navigation: null,
    user: [],
  }

  constructor(props) {
    super(props);

    this.state = {
      departments: [],
      selectedCourses: [],
      loading: false,
    };
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  componentDidMount() {
    this.handleUpdateDepartments();
  }

  onPressButton = (buttonId) => {
    if (this.isSelected(buttonId)) {
      const index = this.state.selectedCourses.indexOf(buttonId);
      const selectedCourses = _.cloneDeep(this.state.selectedCourses);
      selectedCourses.splice(index, 1);
      this.handleUnfollowCourse(buttonId);
      this.setState({ selectedCourses });
    } else {
      const newArray = _.cloneDeep(this.state.selectedCourses);
      newArray.push(buttonId);
      this.setState({ selectedCourses: newArray });
    }
  }

  isSelected = (buttonId) => {
    const index = this.state.selectedCourses.indexOf(buttonId);
    if (index > -1) {
      return true;
    }
    return false;
  }

  handleUpdateDepartments() {
    const {
      user,
    } = this.props;

    this.setState({
      loading: true,
    });

    allDepartments(user.token)
      .then(res => res.json())
      .then((result) => {
        this.setState({
          departments: result.data,
          initialDepartments: _.cloneDeep(result.data),
          loading: false,
        });
      });
  }

  handleCheckSelected = (courseToCheck, courseId) => courseToCheck === courseId;

  handleSearchChange = (event) => {
    const {
      departments,
      initialDepartments,
    } = this.state;

    if (event === '') {
      this.setState({
        departments: initialDepartments,
      });
    } else {
      departments.map((department, index) => {
        department.children.map((departmentChild, indexChild) => {
          const depart = departmentChild.attributes.name.toLowerCase();
          if (depart.indexOf(event) < 0) {
            const newArrayDepartments = _.cloneDeep(department.children);
            newArrayDepartments.splice(indexChild, 1);
            const newGeneralDepartments = _.cloneDeep(departments);
            if (newArrayDepartments.length === 0) {
              newGeneralDepartments.splice(index, 1);
            } else {
              newGeneralDepartments[index].children = _.cloneDeep(newArrayDepartments);
            }
            this.setState({
              departments: newGeneralDepartments,
            });
          }
        });
      });
    }
  }

  handleUnfollowCourse(courseId) {
    const {
      user,
    } = this.props;

    const courses = [courseId];

    unfollowCourse(user.token, courses);
  }

  handleSubmit() {
    const {
      selectedCourses,
    } = this.state;

    const {
      token,
    } = this.props.user;

    followCourse(token, selectedCourses)
      .then(res => res.json())
      .then((result) => {
        if (result.status || result.error.code === 400) {
          this.props.navigation.navigate('CoursesAdded');
        }
      });
  }

  render() {
    const {
      departments,
      loading,
      selectedCourses,
    } = this.state;

    return (
      <DepartmentSelectContainer>
        <HeaderContainer>
          <TitleBar {...this.props} pageTitle="Find courses by department" />
          <SearchContainer>
            <PureInput
              name="email"
              label="Search"
              autoCapitalize="none"
              onChangeText={this.handleSearchChange}
            />
          </SearchContainer>
        </HeaderContainer>
        {loading &&
          <LoadingContainer>
            <ActivityIndicator size="large" color="#50e3c2" />
          </LoadingContainer>
        }
        {!loading &&
          <ChooseTableContainer>
            {departments.map(falculty => (
              <FalcultyContainer key={falculty.uuid}>
                <TitleFaculty>{falculty.attributes.name}</TitleFaculty>
                {falculty.children.map(department => (
                  <ItemContainer key={department.uuid}>
                    {(department.attributes.type === 'course') &&
                      <CourseButton
                        titleCourse={department.attributes.name}
                        onPressButton={() => this.onPressButton(department.uuid)}
                        /* eslint-disable-next-line */
                        selected={selectedCourses.find(courseToCheck => this.handleCheckSelected(courseToCheck, department.uuid))}
                      />
                    }
                    {!(department.attributes.type === 'course') &&
                      <ButtonChoose
                        key={department.uuid}
                        onPress={() => this.props.navigation.navigate('CourseSelect', { department, selectedCourses })}
                      >
                        <DepartmentContainer>
                          <TitleDepartment>{department.attributes.name}</TitleDepartment>
                          <PathIcon source={path} />
                        </DepartmentContainer>
                      </ButtonChoose>
                    }
                  </ItemContainer>
                  ))
                }
              </FalcultyContainer>
            ))}
            <DepartmentPadding />
          </ChooseTableContainer>
        }
        <ButtonContainer visible={selectedCourses[0]}>
          <Button
            label="Next"
            accessibilityLabel="Next"
            onPress={() => this.handleSubmit()}
          />
        </ButtonContainer>
      </DepartmentSelectContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(DepartmentSelect);
