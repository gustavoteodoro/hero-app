import styled, { css } from 'styled-components';
import { width, height } from 'react-native-dimension';
import { lightGray, mediumGray, whiteFour, greyishBrown, warmGrey, white } from '../../../styles/colors';
import { OpenSansBold } from '../../../styles/fonts';

export const DepartmentSelectContainer = styled.View`
  display: flex;
  width: ${width(100)};
  height: ${height(100)};
  align-items: center;
  background-color: ${white};
`;

export const HeaderContainer = styled.View`
  display: flex;
  width: ${width(100)};
  background-color: ${lightGray};
  height: ${height(24)};
  align-items: center;
  box-shadow: 0px 1px 8px ${mediumGray};
`;

export const SearchContainer = styled.View`
  width: ${width(80)};
`;

export const ChooseTableContainer = styled.ScrollView`
  width: ${width(100)};
  padding-left: ${width(10)};
  padding-right: ${width(10)};
  padding-top: 50;
`;

export const TitleContainer = styled.View`
  width: ${width(80)};
  margin-bottom: 16;
`;

export const FalcultyContainer = styled.View`
  width: ${width(80)};
  padding-bottom: 35;
`;

export const DepartmentContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: ${width(80)};
  border: .5px solid ${whiteFour};
  min-height: 55;
  justify-content: space-around;
`;

export const ButtonChoose = styled.TouchableWithoutFeedback`
  display: flex;
`;

export const TitleFaculty = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
  color: ${warmGrey};
  height: 44;
`;

export const TitleDepartment = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
  color: ${greyishBrown};
  width: 200px;
  padding-top: 5px;
  padding-bottom: 5px;
`;

export const PathIcon = styled.Image`
  display: flex;
  transform: translateY(3px);
`;

export const DepartmentPadding = styled.View`
  height: 150;
`;

export const LoadingContainer = styled.View`
  width: ${width(100)};
  height: ${height(76)};
  background: ${white};
  justify-content: center;
`;

export const ButtonContainer = styled.View`
  display: none;
  width: ${width(100)};
  position: absolute;
  bottom: 0;
  background: ${white};
  padding-left: ${width(10)};
  padding-right: ${width(10)};
  padding-top: 15;
  padding-bottom: 15;
  box-shadow: 0px 1px 4px ${mediumGray};

  ${props => props.visible && css`
    display: flex;
  `}
`;

export const ItemContainer = styled.View`

`;
