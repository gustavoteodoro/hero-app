import React, { Component } from 'react';
import { LoginContainer } from './styles';
import LoginForm from '../../modules/LoginForm';
import Logo from '../../elements/Logo';
import TitleBar from '../../modules/TitleBar';

class Login extends Component {
  render() {
    return (
      <LoginContainer>
        <TitleBar {...this.props} pageLink="Signup" />
        <Logo />
        <LoginForm
          {...this.props}
        />
      </LoginContainer>
    );
  }
}

export default Login;
