import React, { Component } from 'react';
import { Animated } from 'react-native';
import { PropTypes } from 'prop-types';
import { SwitchContainer } from './styles';

class Switch extends Component {
  static propTypes = {
    checked: PropTypes.bool,
  }

  static defaultProps = {
    checked: true,
  }

  constructor(props) {
    super(props);
    this.state = {
      check: new Animated.Value(2.5),
    };
  }

  render() {
    const {
      checked,
    } = this.props;

    const {
      check,
    } = this.state;

    Animated.timing(
      this.state.check,
      {
        toValue: checked ? 14 : 2.5,
        duration: 200,
      },
    ).start();

    return (
      <SwitchContainer checked={checked}>
        <Animated.View
          style={{
              width: 14,
              height: 14,
              backgroundColor: 'rgb(255,255,255)',
              borderRadius: 7,
              shadowColor: 'rgba(0, 0, 0, 0.15)',
              shadowOffset: {
                width: 0,
                height: 1.5,
              },
              shadowRadius: 4,
              shadowOpacity: 1,
              borderStyle: 'solid',
              borderWidth: 0.3,
              borderColor: 'rgba(0, 0, 0, 0.04)',
              marginLeft: check,
            }}
        />
      </SwitchContainer>
    );
  }
}

export default Switch;
