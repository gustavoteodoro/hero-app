import styled, { css } from 'styled-components';
import { lightishGreen, borderGrey } from '../../../styles/colors';

export const SwitchContainer = styled.View`
  width: 30;
  height: 18;
  background: ${borderGrey};
  border-radius: 15;
  justify-content: center;

  ${props => props.checked && css`
    background: ${lightishGreen};
  `}
`;
