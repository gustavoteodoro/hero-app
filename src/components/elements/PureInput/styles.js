import styled from 'styled-components';

export const InputContainer = styled.View`
  display: flex;
`;

export const TextInput = styled.View`
  font-family: 'open-sans-bold';
  font-size: 14;
  font-style: italic;
`;
