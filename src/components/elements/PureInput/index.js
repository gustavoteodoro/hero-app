import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'react-native-material-textfield';
import { InputContainer } from './styles';

export default class PureInput extends Component {
  static propTypes = {
    validate: PropTypes.string,
    required: PropTypes.bool,
    onChange: PropTypes.func,
  }

  static defaultProps = {
    validate: null,
    required: false,
    onChange: null,
  }
  render() {
    return (
      <InputContainer>
        <TextField
          {...this.props}
          labelTextStyle={{ fontFamily: 'open-sans-regular' }}
          textColor="rgb(74,74,74)"
          tintColor="rgb(74,74,74)"
          fontSize={14}
          baseColor="rgb(74,74,74)"
          activeLineWidth={1}
        />
      </InputContainer>
    );
  }
}
