import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { OpenSansBold } from '../../../styles/fonts';

export const ListItemContainer = styled.View`
  display: flex;
  width: ${width(100)};
  padding-left: ${width(8)};
  padding-right: ${width(8)};
  border-bottom-width: 1;
  border-bottom-color: rgb(231,223,223);
  flex-direction: row;
  align-items: center;
  justify-content:space-between; 
`;

export const ListItemClickable = styled.TouchableWithoutFeedback`
  display: flex;
`;

export const ListItemTitle = styled.View`
  padding-top: ${width(6.5)};
  padding-bottom: ${width(6)};
  flex: .5;
`;

export const ListItemTitleText = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 12;
`;

export const ListItemButton = styled.View`
  align-items: flex-end;
  flex: .25;
`;
