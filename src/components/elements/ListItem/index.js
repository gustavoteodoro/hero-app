import React, { Component } from 'react';
/* eslint-disable-next-line */
import { Ionicons, Foundation } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import Button from '../../elements/Button';
import {
  ListItemContainer,
  ListItemClickable,
  ListItemTitle,
  ListItemTitleText,
  ListItemButton,
} from './styles';

class ListItem extends Component {
  static propTypes = {
    course: PropTypes.shape({
      attributes: PropTypes.object,
    }),
    onClickPost: PropTypes.func,
    onClickUnit: PropTypes.func,
    onClickRate: PropTypes.func,
  }

  static defaultProps = {
    course: null,
    onClickPost: null,
    onClickUnit: null,
    onClickRate: null,
  }
  render() {
    const {
      course,
      onClickPost,
      onClickRate,
      onClickUnit,
    } = this.props;

    return (
      <ListItemContainer>
        <ListItemClickable onPress={() => onClickUnit(course)}>
          <ListItemTitle>
            <ListItemTitleText>
              {course.attributes.name}
            </ListItemTitleText>
          </ListItemTitle>
        </ListItemClickable>
        <ListItemButton>
          <Button
            label="Rate"
            accessibilityLabel="Rate"
            onPress={() => onClickRate(course)}
            small
          />
        </ListItemButton>
        <ListItemButton>
          <Button
            label="Post"
            accessibilityLabel="Post"
            small
            onPress={() => onClickPost(course.uuid)}
          />
        </ListItemButton>
      </ListItemContainer>
    );
  }
}

export default ListItem;
