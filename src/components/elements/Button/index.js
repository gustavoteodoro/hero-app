import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native';
import { ButtonContainer, ButtonLabel } from './styles';

export default class Button extends PureComponent {
  static propTypes = {
    label: PropTypes.string,
    small: PropTypes.bool,
    loading: PropTypes.bool,
    ghost: PropTypes.bool,
    large: PropTypes.bool,
  }

  static defaultProps = {
    label: null,
    small: false,
    loading: false,
    ghost: false,
    large: false,
  }

  render() {
    const {
      label,
      small,
      loading,
      ghost,
      large,
    } = this.props;
    return (
      <ButtonContainer {...this.props} small={small} ghost={ghost} large={large}>
        {!loading &&
          <ButtonLabel small={small} ghost={ghost}>{label}</ButtonLabel>
        }
        {loading &&
          <ActivityIndicator size="small" color="#fff" />
        }
      </ButtonContainer>
    );
  }
}
