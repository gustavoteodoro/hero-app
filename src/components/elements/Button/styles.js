import styled, { css } from 'styled-components';
import {
  white,
  aquaMarine,
  greyishBrown,
  warmGrey,
} from '../../../styles/colors';
import { OpenSansBold } from '../../../styles/fonts';

export const ButtonContainer = styled.TouchableOpacity`
  backgroundColor: ${aquaMarine};
  height: 41;
  padding-left: 10;
  padding-right: 10;
  display: flex;
  border-radius: 5.5;
  align-items: center;
  justify-content: center;
  ${props => props.small && css`
    width: 55;
    height: 26;
    padding-left: 10;
    padding-right: 10;
  `}
  ${props => props.large && css`
    height: 51;
  `}
  ${props => props.ghost && css`
    backgroundColor: transparent;
    border-top-color: ${greyishBrown};
    border-top-width: 1;
    border-right-color: ${greyishBrown};
    border-right-width: 1;
    border-bottom-color: ${greyishBrown};
    border-bottom-width: 1;
    border-left-color: ${greyishBrown};
    border-left-width: 1;
  `}
  ${props => props.disabled && css`
    backgroundColor: ${warmGrey};
  `}
`;

export const ButtonLabel = styled.Text`
  font-family: ${OpenSansBold};
  color: ${white};
  font-size: 13.5;
  ${props => props.small && css`
    font-size: 11;
  `}
  ${props => props.ghost && css`
    color: ${greyishBrown};
  `}
`;
