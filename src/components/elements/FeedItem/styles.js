import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import {
  warmGreyTwo,
  greyishBrown,
  black,
  tealish,
} from '../../../styles/colors';
import {
  OpenSansRegular,
  OpenSansBold,
} from '../../../styles/fonts';

export const FeedItemContainer = styled.View`
  display: flex;
  position: relative;
  width: ${width(100)};
  padding-left: ${width(8)};
  padding-right: ${width(8)};
  padding-top: ${width(5.3)};
  border-bottom-width: 1;
  border-bottom-color: rgb(231,223,223);
  overflow: hidden;

  ${props => props.newComment && css`
    background: #e5f9f5;
  `}
`;

export const FeedItemClickable = styled.TouchableWithoutFeedback`
  display: flex;
`;

export const FeedItemHeader = styled.View`
  flex-direction: row;
`;

export const FeedItemFooter = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const FeedItemFooterSide = styled.View`
  flex-direction: row;
`;

export const FeedItemTitle = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
  margin-top: 10;
  margin-bottom: 10;
`;

export const FeedItemContent = styled.View`
  padding-top: 25;
`;

export const FeedItemContentText = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 12;
  color: ${greyishBrown};
`;

export const FeedItemContentMoreButton = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 12;
  color: ${tealish};
`;


export const FeedItemAuthor = styled.Text`
  font-size: 11;
`;

export const FeedItemModerator = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 11;
  color: ${black};
  margin-top: -1;
`;

export const FeedItemUnitPosted = styled.Text`
  font-size: 11;
  color: ${warmGreyTwo};
`;

export const FeedItemUnitText = styled.Text`
`;

export const FeedItemUnitTextComment = styled.View`
  width: ${width(84)};
  flex-direction: row;
  justify-content: space-between;
`;

export const FeedItemUnit = styled.Text`
  font-size: 11;
  color: ${black};
`;

export const FeedItemText = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
  margin-left: 10;
`;

export const FooterItem = styled.View`
  align-items: center;
  flex-direction: row;
  padding-right: 25;
  padding-top: 26;
  padding-bottom: ${width(4.5)};
`;

export const FeedItemDelete = styled.TouchableWithoutFeedback`
`;

