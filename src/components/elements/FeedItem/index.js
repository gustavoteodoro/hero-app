import React, { Component } from 'react';
import { Alert, TouchableWithoutFeedback, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types';
import { likeDiscussion, dislikeDiscussion, deleteDiscussion } from '../../../services/discussions';
import { likeComment, dislikeComment, deleteComment } from '../../../services/comments';
/* eslint-disable-next-line */
import { Ionicons, Foundation } from '@expo/vector-icons';
import { HeroIcon } from '../../../styles/fonts';

import {
  FeedItemContainer,
  FeedItemHeader,
  FeedItemContent,
  FeedItemContentText,
  FeedItemAuthor,
  FeedItemModerator,
  FeedItemUnitPosted,
  FeedItemUnitText,
  FeedItemUnitTextComment,
  FeedItemUnit,
  FeedItemText,
  FeedItemFooter,
  FeedItemFooterSide,
  FooterItem,
  FeedItemClickable,
  FeedItemDelete,
  FeedItemContentMoreButton,
} from './styles';

class FeedItem extends Component {
  static propTypes = {
    sugestion: PropTypes.shape({
      text: PropTypes.string,
      comment: PropTypes.string,
      author: PropTypes.object,
      unit: PropTypes.object,
      created_at: PropTypes.string,
      likes_cached: PropTypes.number,
      comments_cached: PropTypes.number,
      uuid: PropTypes.string,
      newComment: PropTypes.bool,
    }),
    onClick: PropTypes.func,
    onClickComment: PropTypes.func,
    isComment: PropTypes.bool,
    currentUser: PropTypes.shape({
      moderator: PropTypes.bool,
    }),
    token: PropTypes.string,
    discussionId: PropTypes.string,
    itemId: PropTypes.string,
    isHeader: PropTypes.bool,
    update: PropTypes.func,
    liked: PropTypes.bool,
    limit: PropTypes.number,
    enableSeeMore: PropTypes.bool,
  }

  static defaultProps = {
    sugestion: {
      text: '',
      author: {
        moderator: false,
      },
      unit: '',
      created_at: '',
      likes_cached: 0,
      comments_cached: 0,
      uuid: '',
      newComment: false,
    },
    onClick: () => {},
    onClickComment: () => {},
    isComment: false,
    currentUser: null,
    token: '',
    discussionId: '',
    itemId: '',
    isHeader: false,
    update: null,
    liked: false,
    limit: null,
    enableSeeMore: false,
  }


  constructor(props) {
    super(props);

    this.state = {
      seeMore: false,
      clickedLike: false,
      clickedUnlike: false,
      fadeIn: new Animated.Value(0),
    };
    this.handleDeleteAction = this.handleDeleteAction.bind(this);
    this.handleDeleteClickPost = this.handleDeleteClickPost.bind(this);
    this.handleSeeMore = this.handleSeeMore.bind(this);
    this.handleSeeLess = this.handleSeeLess.bind(this);
  }

  handleLike() {
    const {
      token, discussionId, itemId, update, liked,
    } = this.props;

    if (!liked) {
      this.setState({
        clickedLike: true,
        clickedUnlike: false,
      });
    } else {
      this.setState({
        clickedLike: false,
        clickedUnlike: true,
      });
    }

    setTimeout(() => {
      this.setState({
        clickedLike: false,
        clickedUnlike: false,
      });
    }, 10000);

    if (discussionId) {
      likeComment(token, discussionId, itemId)
        .then(res => res.json())
        .then((result) => {
          if (result.status) {
            update();
          } else {
            dislikeComment(token, discussionId, itemId)
              .then(() => {
                update();
              });
          }
          this.setState({
            clickedLike: false,
            clickedUnlike: false,
          });
        });
    } else {
      likeDiscussion(token, itemId)
        .then(res => res.json())
        .then((result) => {
          if (result.status) {
            update();
          } else {
            dislikeDiscussion(token, itemId)
              .then(() => {
                update();
              });
          }
        });
    }
  }

  humanize = (millis, fallback) => {
    if (millis < 1000) return 'a few sec';
    else if (millis < 60 * 1000) return `${Math.round(millis / 1000)}S`;
    else if (millis < 60 * 60 * 1000) return `${Math.round(millis / 1000 / 60)}M`;
    else if (millis < 24 * 60 * 60 * 1000) return `${Math.round(millis / 1000 / 60 / 60)}H`;
    else if (millis < 7 * 24 * 60 * 60 * 1000) return `${Math.floor(millis / 1000 / 60 / 60 / 24)}D`;
    return fallback;
  }

  currentTime() {
    const {
      created_at,
    } = this.props.sugestion;

    const millis = new Date(new Date().getTime() - new Date(created_at.replace(' ', 'T')).getTime());


    return this.humanize(millis, created_at);
  }

  handleDeleteAction(uuid) {
    const {
      token,
      discussionId,
      update,
      isComment,
    } = this.props;

    if (isComment) {
      deleteComment(token, discussionId, uuid)
        .then(res => res.json())
        .then((result) => {
          if (result.status) {
            update();
          }
        });
    } else {
      deleteDiscussion(token, uuid)
        .then(res => res.json())
        .then((result) => {
          if (result.status) {
            update();
          }
        });
    }
  }

  handleDeleteClick = (commentId) => {
    Alert.alert(
      ' ',
      'Are you sure you want to delete your comment?',
      [
        { text: 'Keep', style: 'cancel' },
        { text: 'Delete', onPress: () => this.handleDeleteAction(commentId) },
      ],
      { cancelable: false },
    );
  }

  handleDeleteClickPost = (discussionId) => {
    Alert.alert(
      ' ',
      'Are you sure you want to delete your post?',
      [
        { text: 'Keep', style: 'cancel' },
        { text: 'Delete', onPress: () => this.handleDeleteAction(discussionId) },
      ],
      { cancelable: false },
    );
  }

  handleSeeMore = () => {
    this.setState({
      seeMore: true,
    });
  }

  handleSeeLess = () => {
    this.setState({
      seeMore: false,
    });
  }


  render() {
    const {
      isComment,
      currentUser,
      isHeader,
      liked,
      limit,
      enableSeeMore,
    } = this.props;

    const {
      /* eslint-disable-next-line */
      anonymous,
      text,
      comment,
      author,
      unit,
      likes_cached,
      comments_cached,
      uuid,
      newComment,
    } = this.props.sugestion;

    const {
      seeMore,
      clickedLike,
      clickedUnlike,
      fadeIn,
    } = this.state;

    const showDelete = () => {
      if (currentUser) {
        if (author && !isHeader) {
          if (currentUser.moderator || (isComment && (currentUser.email === author.email))) {
            return true;
          }
          return false;
        } else if (currentUser.moderator && !isHeader) {
          return true;
        }
        return false;
      }
      return false;
    };

    if (newComment) {
      Animated.timing(
        this.state.fadeIn,
        {
          toValue: 0.2,
          duration: 1000,
          easing: Easing.bezier(0.17, 0.67, 0.49, 1.5),
          delay: 300,
        },
      ).start();
    }
    return (
      <FeedItemContainer>
        {newComment &&
          <Animated.View
            style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '200%',
                height: '200%',
                backgroundColor: 'rgb(80,227,194)',
                opacity: fadeIn,
              }}
          />
        }
        <FeedItemHeader>
          {!isComment &&
            <FeedItemUnitText>
              <FeedItemAuthor>{anonymous ? 'Anonymous' : author.name} </FeedItemAuthor>
              <FeedItemUnitPosted>posted about</FeedItemUnitPosted>
              <FeedItemUnit> {unit.attributes.name}</FeedItemUnit>
            </FeedItemUnitText>
          }
          {isComment &&
            <FeedItemUnitTextComment>
              <FeedItemAuthor>{anonymous ? 'Anonymous' : author.name}</FeedItemAuthor>
              {author &&
                <FeedItemModerator>{author.moderator ? 'Moderator' : ''}</FeedItemModerator>
              }
            </FeedItemUnitTextComment>
          }
        </FeedItemHeader>
        <FeedItemClickable onPress={() => this.props.onClick()}>
          <FeedItemContent>
            {(!limit || seeMore) &&
              <FeedItemContentText>
                {!isComment ? text : comment}
                {(enableSeeMore && (text.length > limit)) &&
                  <TouchableWithoutFeedback onPress={() => this.handleSeeLess()}>
                    <FeedItemContentMoreButton> see less</FeedItemContentMoreButton>
                  </TouchableWithoutFeedback>
                }
              </FeedItemContentText>
            }
            {(limit && !seeMore) &&
              <FeedItemContentText>
                {(text.length > limit) ? `${text.substring(0, limit)}...` : text}
                {((enableSeeMore && !seeMore) && (text.length > limit)) &&
                  <TouchableWithoutFeedback onPress={() => this.handleSeeMore()}>
                    <FeedItemContentMoreButton> see more</FeedItemContentMoreButton>
                  </TouchableWithoutFeedback>
                }
              </FeedItemContentText>
            }
          </FeedItemContent>
        </FeedItemClickable>
        <FeedItemFooter>
          <FeedItemFooterSide>
            <FooterItem>
              <Ionicons name="ios-time" size={15} color="rgb(155,155,155)" />
              <FeedItemText>{this.currentTime()}</FeedItemText>
            </FooterItem>
            <TouchableWithoutFeedback onPress={() => this.handleLike()}>
              <FooterItem>
                <Ionicons name="md-thumbs-up" size={15} color={((!liked && clickedLike) || (liked && !clickedUnlike)) ? 'rgb(80,227,194)' : 'rgb(155,155,155)'} />
                {liked &&
                  <FeedItemText>{(clickedUnlike) ? likes_cached - 1 : likes_cached}</FeedItemText>
                }
                {!liked &&
                  // eslint-disable-next-line
                  <FeedItemText>{(!liked && clickedLike) ? likes_cached + 1 : likes_cached}</FeedItemText>
                }
              </FooterItem>
            </TouchableWithoutFeedback>
            {!isComment &&
              <TouchableWithoutFeedback onPress={() => this.props.onClickComment()}>
                <FooterItem>
                  <Foundation name="comment" size={15} color="rgb(155,155,155)" />
                  <FeedItemText>{comments_cached}</FeedItemText>
                </FooterItem>
              </TouchableWithoutFeedback>
            }
          </FeedItemFooterSide>
          {showDelete() &&
            <FeedItemFooterSide>
              {/* eslint-disable-next-line */}
              <FeedItemDelete onPress={isComment ? () => this.handleDeleteClick(uuid) : () => this.handleDeleteClickPost(uuid)}>
                <HeroIcon name="trash" size={15} color="rgb(155,155,155)" />
              </FeedItemDelete>
            </FeedItemFooterSide>
          }
        </FeedItemFooter>
      </FeedItemContainer>
    );
  }
}

export default FeedItem;
