import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  CourseContainer,
  TitleCourse,
  PathIcon,
  ButtonChoose,
} from './styles';
import iconPlus from '../../../assets/plus_icon.png';
import iconMinus from '../../../assets/icon_minus.png';

export default class CourseButton extends PureComponent {
  static propTypes = {
    titleCourse: PropTypes.string,
    onPressButton: PropTypes.func,
  }

  static defaultProps = {
    titleCourse: null,
    onPressButton: false,
  }

  render() {
    const {
      titleCourse,
      selected,
    } = this.props;

    return (
      <ButtonChoose onPress={() => this.props.onPressButton()}>
        <CourseContainer selected={selected}>
          <TitleCourse>{titleCourse}</TitleCourse>
          <PathIcon source={selected ? iconMinus : iconPlus} />
        </CourseContainer>
      </ButtonChoose>
    );
  }
}
