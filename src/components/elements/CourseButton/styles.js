import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { greyishBrown, whiteFour, whiteTwo } from '../../../styles/colors';
import { OpenSansBold } from '../../../styles/fonts';

export const CourseContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: ${width(80)};
  border: .5px solid ${whiteFour};
  min-height: 55;
  justify-content: space-around;
  ${props => props.selected && css`
    background-color: ${whiteTwo};
  `}
`;

export const TitleCourse = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
  color: ${greyishBrown};
  width: 200px;
  padding-top: 5px;
  padding-bottom: 5px;
`;

export const PathIcon = styled.Image`
  display: flex;
  transform: translateY(3px);
`;

export const ButtonChoose = styled.TouchableWithoutFeedback`
  display: flex;
`;
