/* eslint-disable */
import React, { PureComponent } from 'react';
import { Ionicons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import { ButtonContainer, ButtonImage } from './styles';


export default class BackArrowButton extends PureComponent {
  static propTypes = {
    page: PropTypes.string,
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
  }

  static defaultProps = {
    page: null,
    navigation: null,
  }

  redirect = (_page) => {
    if (_page !== '') {
      this.props.navigation.navigate(_page, { pageLink: 'Home' });
    } else {
      this.props.navigation.goBack();
    }
  }

  render() {
    const {
      page,
    } = this.props;
    return (
      <ButtonContainer onPress={() => this.redirect(page)}>
        <Ionicons name="md-arrow-back" size={30} color="#50e3c2" />
      </ButtonContainer>
    );
  }
}
