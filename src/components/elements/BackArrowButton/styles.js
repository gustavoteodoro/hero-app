import styled from 'styled-components';
import { width } from 'react-native-dimension';

export const ButtonContainer = styled.TouchableOpacity`
  position: absolute;
  left: ${width(5)};
  width: 40;
  height: 40;
  display: flex;
  align-items: center;
  z-index: 999;
`;

export const ButtonImage = styled.Image`
  width: 10;
  z-index: -999;
`;
