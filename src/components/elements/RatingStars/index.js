import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { RatingStarsContainer, RatingStarsLabel, RatingStar, RatingStarContent, RatingStarContentBack } from './styles';
import { HeroIcon } from '../../../styles/fonts';

export default class RatingStars extends PureComponent {
  static propTypes = {
    label: PropTypes.string,
    currentRating: PropTypes.string,
  }

  static defaultProps = {
    label: null,
    currentRating: 0,
  }

  render() {
    const {
      label,
      currentRating,
    } = this.props;

    return (
      <RatingStarsContainer {...this.props}>
        <RatingStarsLabel>{label}</RatingStarsLabel>
        <RatingStarContent>
          {[1, 2, 3, 4, 5].map(i => (
            <RatingStar
              key={i}
            >
              {((Number(currentRating) < i) && (Number(currentRating) >= (i - 0.5))) &&
                <HeroIcon
                  name="star-half"
                  size={16}
                  color="#4a4a4a"
                />
              }
              {!((Number(currentRating) < i) && (Number(currentRating) >= (i - 0.5))) &&
                <HeroIcon
                  name="star"
                  size={16}
                  color={(Number(currentRating) >= i) ? '#4a4a4a' : '#b6b3af'}
                />
              }

            </RatingStar>
          ))}
        </RatingStarContent>
        <RatingStarContentBack>
          {[1, 2, 3, 4, 5].map(i => (
            <RatingStar
              key={i}
            >
              <HeroIcon
                name="star"
                size={16}
                color="#b6b3af"
              />
            </RatingStar>
          ))}
        </RatingStarContentBack>
      </RatingStarsContainer>
    );
  }
}
