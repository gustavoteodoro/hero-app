import styled, { css } from 'styled-components';
import { white } from '../../../styles/colors';
import { OpenSansBold } from '../../../styles/fonts';

export const RatingStarsContainer = styled.View`
  display: flex;
  flex-direction: row;
  margin-left: -5;
  position: relative;
`;

export const RatingStarsLabel = styled.Text`
  font-family: ${OpenSansBold};
  color: ${white};
  font-size: 13.5;
  ${props => props.small && css`
    font-size: 11;
  `}
`;

export const RatingStar = styled.View`
  margin-left: 5;
  margin-right: 5;
`;

export const RatingStarContent = styled.View`
  position: absolute;
  flex-direction: row;
  z-index: 10;
`;

export const RatingStarContentBack = styled.View`
  position: absolute;
  flex-direction: row;
  z-index: 5;
`;
