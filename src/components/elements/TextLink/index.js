import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text } from './styles';

export default class TextLink extends PureComponent {
  static propTypes = {
    page: PropTypes.string,
    text: PropTypes.string,
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
  }

  static defaultProps = {
    page: null,
    text: null,
    navigation: null,
  }

  redirect = (_page) => {
    this.props.navigation.navigate(_page);
  }

  render() {
    const {
      page,
      text,
    } = this.props;
    return (
      <Text onPress={() => this.redirect(page)}>{text}</Text>
    );
  }
}
