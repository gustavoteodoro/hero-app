import styled from 'styled-components';
import { greyishBrown } from '../../../styles/colors';
import { OpenSansBold } from '../../../styles/fonts';

export const Text = styled.Text`
  font-family: ${OpenSansBold};
  color: ${greyishBrown};
  font-size: 12px;
  text-align: center;
`;
