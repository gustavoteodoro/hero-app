import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { TouchableWithoutFeedback } from 'react-native';
import { CallToInputContainer, CallToInputLabel } from './styles';

export default class CallToInput extends PureComponent {
  static propTypes = {
    label: PropTypes.string,
    onClick: PropTypes.func,
  }

  static defaultProps = {
    label: null,
    onClick: null,
  }

  render() {
    const {
      label,
      onClick,
    } = this.props;
    return (
      <TouchableWithoutFeedback onPress={onClick}>
        <CallToInputContainer {...this.props}>
          <CallToInputLabel>
            {label}
          </CallToInputLabel>
        </CallToInputContainer>
      </TouchableWithoutFeedback>
    );
  }
}
