import styled from 'styled-components';
import {
  white,
  warmGrey,
  whiteFour,
} from '../../../styles/colors';
import { OpenSansRegular } from '../../../styles/fonts';

export const CallToInputContainer = styled.View`
  backgroundColor: ${white};
  height: 60;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  padding-left: 48;
  border-bottom-width: 1;
  border-bottom-color: ${whiteFour};
  z-index: -1;
`;

export const CallToInputLabel = styled.Text`
  font-family: ${OpenSansRegular};
  color: ${warmGrey};
  font-size: 14;
`;
