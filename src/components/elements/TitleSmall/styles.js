import styled from 'styled-components';
import { OpenSansBold } from '../../../styles/fonts';

export const Title = styled.Text`
  font-size: 13;
  text-align: center;
  font-family: ${OpenSansBold};
`;

export const Text = styled.Text`
  font-size: 15;
`;

