import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native';
import { ButtonRemove, IconRemove } from './styles';
import iconClose from '../../../assets/icon_close.png';

export default class Button extends PureComponent {
  static propTypes = {
    onPress: PropTypes.func,
  }

  static defaultProps = {
    onPress: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    };
  }

  handleOnPress() {
    this.props.onPress();
    this.setState({
      loading: true,
    });
  }

  render() {
    const {
      loading,
    } = this.state;

    return (
      <ButtonRemove onPress={() => this.handleOnPress()}>
        {!loading &&
          <IconRemove source={iconClose} />
        }
        {loading &&
          <ActivityIndicator size="small" color="#eda93b" />
        }
      </ButtonRemove>
    );
  }
}
