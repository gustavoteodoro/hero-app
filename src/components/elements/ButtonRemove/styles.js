import styled from 'styled-components';

export const ButtonRemove = styled.TouchableOpacity`
  width: 30;
  height: 30;
  z-index: 999;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const IconRemove = styled.Image`
  display: flex;
  width: 10;
  height: 10;
`;
