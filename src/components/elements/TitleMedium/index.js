import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Title } from './styles';

export default class TitleMedium extends PureComponent {
  static propTypes = {
    titleText: PropTypes.string,
  }

  static defaultProps = {
    titleText: null,
  }

  render() {
    const {
      titleText,
    } = this.props;
    return (
      <Title>{titleText}</Title>
    );
  }
}
