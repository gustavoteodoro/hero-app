import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { RatingStarsContainer, RatingStar, TouchRatingStar } from './styles';
import { HeroIcon } from '../../../styles/fonts';

export default class SelectRatingStars extends PureComponent {
  static propTypes = {
    currentRating: PropTypes.number,
    onRate: PropTypes.func,
  }

  static defaultProps = {
    currentRating: 0,
    onRate: null,
  }

  render() {
    const {
      currentRating,
      onRate,
    } = this.props;

    return (
      <RatingStarsContainer {...this.props}>
        {[1, 2, 3, 4, 5].map(i => (
          <TouchRatingStar
            key={i}
            onPress={() => onRate(i)}
          >
            <RatingStar>
              <HeroIcon
                name="star"
                size={22}
                color={(currentRating >= i) ? 'rgb(80,227,194)' : '#b6b3af'}
              />
            </RatingStar>
          </TouchRatingStar>
        ))}

      </RatingStarsContainer>
    );
  }
}
