import styled, { css } from 'styled-components';
import { white } from '../../../styles/colors';
import { OpenSansBold } from '../../../styles/fonts';

export const RatingStarsContainer = styled.View`
  display: flex;
  margin-top: 25;
  margin-bottom: 25;
  flex-direction: row;
  justify-content: center;
`;

export const RatingStarsLabel = styled.Text`
  font-family: ${OpenSansBold};
  color: ${white};
  font-size: 13.5;
  ${props => props.small && css`
    font-size: 11;
  `}
`;

export const TouchRatingStar = styled.TouchableWithoutFeedback`
`;

export const RatingStar = styled.View`
  padding-left: 10;
  padding-right: 10;
`;
