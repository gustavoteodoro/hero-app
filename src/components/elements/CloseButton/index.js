import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ButtonCloseContainer, ButtonImage } from './styles';
import buttonClose from '../../../assets/icon_close.png';

export default class CloseButton extends PureComponent {
  static propTypes = {
    page: PropTypes.string,
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
  }

  static defaultProps = {
    page: null,
    navigation: null,
  }

  redirect = (_page) => {
    this.props.navigation.navigate(_page);
  }

  render() {
    const {
      page,
    } = this.props;
    return (
      <ButtonCloseContainer onPress={() => this.redirect(page)}>
        <ButtonImage source={buttonClose} />
      </ButtonCloseContainer>
    );
  }
}
