import styled from 'styled-components';

export const ButtonCloseContainer = styled.TouchableOpacity`
  height: 14;
  width: 14;
`;

export const ButtonImage = styled.Image`
  height: 14;
  width: 14;
`;
