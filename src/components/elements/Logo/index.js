import React from 'react';
import {
  LogoContainer,
  LogoContent,
} from './styles';
import heroFeedbackLogo from '../../../assets/hero-feedback.png';

const Logo = () => (
  <LogoContainer>
    <LogoContent
      source={heroFeedbackLogo}
    />
  </LogoContainer>
);

export default Logo;
