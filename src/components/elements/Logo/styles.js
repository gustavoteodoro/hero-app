import styled from 'styled-components';
import { width } from 'react-native-dimension';

export const LogoContainer = styled.View`
  margin-top: 57;
  align-items: center;
`;

export const LogoContent = styled.Image`
  width: ${width(35)};
  height: ${width(13)};
`;
