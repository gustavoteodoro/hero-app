import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'react-native-material-textfield';
import { InputContainer } from './styles';

export default class Input extends Component {
  static propTypes = {
    validate: PropTypes.string,
    required: PropTypes.bool,
    input: PropTypes.shape({
      onChange: PropTypes.func,
    }),
  }

  static defaultProps = {
    validate: null,
    required: false,
    input: null,
  }

  render() {
    return (
      <InputContainer>
        <TextField
          {...this.props}
          labelTextStyle={{ fontFamily: 'open-sans-bold' }}
          style={{ fontFamily: 'open-sans-regular' }}
          textColor="rgb(74,74,74)"
          tintColor="rgb(74,74,74)"
          errorColor="rgb(247,167,0)"
          fontSize={14}
          baseColor="rgb(74,74,74)"
          activeLineWidth={1}
        />
      </InputContainer>
    );
  }
}
