import styled from 'styled-components';
import { Dimensions } from 'react-native';

const { height } = Dimensions.get('window');

export const ScrollTextContainer = styled.ScrollView`
  display: flex;
  margin:  20px 30px 0px;
  height: ${height * 0.5};
  background-color: #F8F7F5;
  border-radius: 4.5px;
  padding: 20px 25px;
`;

export const Text = styled.Text`
  font-size: 15;
  padding-bottom: 50px;
`;
