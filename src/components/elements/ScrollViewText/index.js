import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { ScrollTextContainer, Text } from './styles';

export default class ScrollViewText extends PureComponent {
  static propTypes = {
    text: PropTypes.string,
  }

  static defaultProps = {
    text: null,
  }

  render() {
    const {
      text,
    } = this.props;
    return (
      <ScrollTextContainer>
        <Text>{text}</Text>
      </ScrollTextContainer>
    );
  }
}
