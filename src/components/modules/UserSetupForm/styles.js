import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { mediumGray, warmGrey } from '../../../styles/colors';
import { OpenSansRegular } from '../../../styles/fonts';

export const UserFormContainer = styled.View`
  margin-top: 50;
  width: ${width(80)};
  margin-left: ${width(10)};
`;

export const TextLabel = styled.Text`
  color: ${mediumGray};
  font-family: ${OpenSansRegular};
  font-size: 16;
`;

export const DotsImage = styled.Image`
  width: 66;
  align-self: center;
  margin-top: 30px;
  margin-bottom: 30px;
`;

export const FormError = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 14;
  color: ${warmGrey};
  width: ${width(60)};
  margin-left: ${width(5)};
`;

export const FormErrorIcon = styled.Image`
  display: flex;
  width: ${width(2.5)};
  height: ${height(5)};
  margin-left: ${width(1)};
  margin-top: 2;
`;

export const FormErrorContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const StatusContainer = styled.View`
  margin-top: 10;
  min-height: ${height(5)};
  margin-bottom: 15;
`;
