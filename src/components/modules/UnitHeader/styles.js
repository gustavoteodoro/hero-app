import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { lightGray, warmGrey, black, aquaMarine, greyishBrownThree } from '../../../styles/colors';
import { NeuzeitBookHeavy, OpenSansBold } from '../../../styles/fonts';

export const UnitHeaderContainer = styled.View`
  display: flex;
  width: ${width(100)};
  background: ${lightGray};
  box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.5);
  padding-bottom: 30;
  ${props => props.minified && css`
    padding-bottom: 15;
  `}
`;

export const UnitHeaderTitle = styled.Text`
  font-family: ${NeuzeitBookHeavy};
  font-size: 22.5;
  margin-top: 20;
  margin-bottom: 20;
  margin-left: ${width(8)};
  margin-right: ${width(8)};
`;

export const TouchFollow = styled.TouchableWithoutFeedback`
`;

export const UnitHeaderFollow = styled.View`
  position: absolute;
  top: 35;
  right: ${width(8)};
  flex-direction: row;
  align-items: center;

  ${props => props.followed && css`
    opacity: 0.6;
  `}
`;

export const AddText = styled.Text`
  display: none;
  font-family: ${OpenSansBold};
  color: ${greyishBrownThree};
  font-size: 13;
  margin-right: 10;
  ${props => props.show && css`
    display: flex;
  `};
`;

export const AddButton = styled.View`
  background: ${aquaMarine};
  width: 25;
  height: 25;
  border-radius: 100;
  align-items: center;
  justify-content: center;
`;

export const UnitHeaderRating = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-left: ${width(8)};
  margin-right: ${width(8)};
`;

export const RatingContainer = styled.View`

`;

export const LoadingStarsContainer = styled.View`
`;

export const AddButtonIcon = styled.View`
  display: none;
  ${props => props.show && css`
    display: flex;
  `};
  ${props => props.loading && css`
    transform: scale(0.8);
  `};
`;

export const RatingText = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 13;
  margin-top: 10;
  color: ${warmGrey};
`;

export const RatingTextCurrent = styled.Text`
  color: ${black};
`;

export const ButtonContainer = styled.View`
  width: ${width(32)};
`;
