import React, { Component } from 'react';
import { Animated, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { HeroIcon } from '../../../styles/fonts';
import { followCourse } from '../../../services/courses';
import Button from '../../elements/Button';
import RatingStars from '../../elements/RatingStars';
import TitleBar from '../TitleBar';

import {
  UnitHeaderContainer,
  UnitHeaderTitle,
  UnitHeaderRating,
  RatingContainer,
  RatingText,
  ButtonContainer,
  RatingTextCurrent,
  UnitHeaderFollow,
  AddButton,
  AddText,
  TouchFollow,
  AddButtonIcon,
  LoadingStarsContainer,
} from './styles';

class UnitHeader extends Component {
  static propTypes = {
    minified: PropTypes.bool,
    /* eslint-disable-next-line */
    unit: PropTypes.object,
    pageLink: PropTypes.string,
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    unfollowed: PropTypes.bool,
    loadingStars: PropTypes.bool,
  }

  static defaultProps = {
    minified: null,
    unit: null,
    pageLink: '',
    user: null,
    navigation: null,
    unfollowed: false,
    loadingStars: false,
  }

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      followed: false,
      fadeIn: new Animated.Value(1),
      slideHeight: new Animated.Value(37),
    };

    this.handleRate = this.handleRate.bind(this);
    this.handleFollow = this.handleFollow.bind(this);
  }

  handleRate() {
    const {
      type,
    } = this.props.unit.attributes;

    if (type === 'course') {
      if (this.props.unfollowed) {
        this.props.navigation.navigate('RequireFollow');
      } else {
        this.props.navigation.navigate('RateCategory');
      }
    } else {
      this.props.navigation.navigate('RateRating');
    }
  }

  handleFollow() {
    const {
      user,
      unit,
    } = this.props;

    const unitArray = [unit];

    this.setState({ loading: true });
    followCourse(user.token, unitArray)
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          this.setState({ followed: true, loading: false });
        }
      });
  }

  render() {
    const {
      minified,
      pageLink,
      unit,
      unfollowed,
      loadingStars,
    } = this.props;

    const {
      slideHeight,
      fadeIn,
      followed,
      loading,
    } = this.state;

    Animated.timing(
      this.state.slideHeight,
      {
        toValue: minified ? 0 : 400,
        duration: 600,
      },
    ).start();

    Animated.timing(
      this.state.fadeIn,
      {
        toValue: minified ? 0 : 1,
        duration: 300,
      },
    ).start();

    return (
      <UnitHeaderContainer minified={minified}>
        <TitleBar {...this.props} pageLink={pageLink} />
        {unfollowed &&
          <TouchFollow onPress={() => this.handleFollow()}>
            <UnitHeaderFollow followed={followed}>
              <AddText show={!followed}>Add course</AddText>
              <AddText show={followed}>Course added</AddText>
              <AddButton>
                <AddButtonIcon show={(!followed && !loading)}>
                  <HeroIcon name="add" size={15} color="rgb(255,255,255)" />
                </AddButtonIcon>
                <AddButtonIcon show={(followed && !loading)}>
                  <HeroIcon name="checkmark" size={10} color="rgb(255,255,255)" />
                </AddButtonIcon>
                <AddButtonIcon show={loading} loading>
                  <ActivityIndicator size="small" color="#fff" />
                </AddButtonIcon>
              </AddButton>
            </UnitHeaderFollow>
          </TouchFollow>
        }
        <Animated.View
          style={{
              overflow: 'hidden',
              maxHeight: slideHeight,
              opacity: fadeIn,
            }}
        >
          <UnitHeaderTitle>{unit.attributes.name}</UnitHeaderTitle>
          <UnitHeaderRating>
            {loadingStars &&
              <LoadingStarsContainer>
                <ActivityIndicator size="small" color="rgb(170,170,170)" />
              </LoadingStarsContainer>
            }
            {!loadingStars &&
              <RatingContainer>
                <RatingStars currentRating={unit.evaluation_average} />
                <RatingText>
                  {/* eslint-disable-next-line */}
                    <RatingTextCurrent>{unit.evaluation_average}</RatingTextCurrent> / {unit.evaluation_count} ratings
                </RatingText>
              </RatingContainer>
            }
            <ButtonContainer>
              <Button
                label="Rate"
                accessibilityLabel="Rate"
                onPress={() => this.handleRate()}
              />
            </ButtonContainer>
          </UnitHeaderRating>
        </Animated.View>
      </UnitHeaderContainer>
    );
  }
}

export default UnitHeader;
