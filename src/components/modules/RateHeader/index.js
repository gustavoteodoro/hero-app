import React from 'react';
import TitleBar from '../TitleBar';
import { RateHeaderContainer, RateHeaderTitle, RateHeaderDescription } from './styles';

const RateHeader = props => (
  <RateHeaderContainer>
    <TitleBar {...props} pageTitle={props.title} />
    {props.unitTitle &&
      <RateHeaderTitle>{props.unitTitle}</RateHeaderTitle>
    }
    {props.description &&
      <RateHeaderDescription>{props.description}</RateHeaderDescription>
    }
  </RateHeaderContainer>
);

export default RateHeader;
