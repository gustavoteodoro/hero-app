import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { lightGray, greyishBrown } from '../../../styles/colors';
import { NeuzeitBookHeavy, OpenSansRegular } from '../../../styles/fonts';

export const RateHeaderContainer = styled.View`
  display: flex;
  width: ${width(100)};
  background: ${lightGray};
  box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.5);
  padding-bottom: 10;
  z-index: 30;
`;

export const RateHeaderTitle = styled.Text`
  font-family: ${NeuzeitBookHeavy};
  font-size: 22.5;
  margin-top: 20;
  margin-bottom: 10;
  margin-left: ${width(8)};
  margin-right: ${width(8)};
`;

export const RateHeaderDescription = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 12;
  color: ${greyishBrown};
  margin-left: ${width(8)};
  margin-right: ${width(15)};
  margin-bottom: 20;
`;
