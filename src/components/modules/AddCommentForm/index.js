import React, { Component } from 'react';
import { Keyboard } from 'react-native';
import PropTypes from 'prop-types';
import CheckBox from 'react-native-checkbox';
import Button from '../../elements/Button';
import CheckboxChecked from '../../../assets/checkbox-checked.png';
import CheckboxUnchecked from '../../../assets/checkbox-unchecked.png';

import {
  AddCommentContainer,
  CommentInput,
  FormFooter,
  FormFooterLeft,
  FormFooterRight,
  checkboxStyles,
  ErrorContainer,
  ErrorText,
} from './styles';

class AddComment extends Component {
  static propTypes = {
    onCheckAnonymous: PropTypes.func,
    onChangeComment: PropTypes.func,
    onSubmit: PropTypes.func,
    commentLabel: PropTypes.string,
    commentAnonymousLabel: PropTypes.string,
    commentSendLabel: PropTypes.string,
    formError: PropTypes.string,
    loading: PropTypes.bool,
  }

  static defaultProps = {
    onCheckAnonymous: null,
    onChangeComment: null,
    onSubmit: null,
    commentLabel: null,
    commentAnonymousLabel: null,
    commentSendLabel: null,
    formError: '',
    loading: false,
  }

  constructor(props) {
    super(props);
    this.state = {
      focused: false,
    };

    this.handlePress = this.handlePress.bind(this);
  }

  handlePress() {
    Keyboard.dismiss();
    this.props.onSubmit();
    this.setState({
      focused: false,
    });
  }

  handleFocused() {
    this.setState({
      focused: true,
    });
  }

  render() {
    const {
      onChangeComment,
      onCheckAnonymous,
      commentLabel,
      commentAnonymousLabel,
      commentSendLabel,
      formError,
      loading,
    } = this.props;

    const {
      focused,
    } = this.state;

    return (
      <AddCommentContainer focused={focused}>
        <CommentInput
          autoFocus
          name="comment"
          placeholder={commentLabel}
          autoCapitalize="none"
          maxLength={1000}
          multiline
          onFocus={() => this.handleFocused()}
          onChangeText={e => onChangeComment(e)}
          underlineColorAndroid="transparent"
          textAlignVertical="top"
        />
        {(formError !== '') &&
          <ErrorContainer>
            <ErrorText>
              {formError}
            </ErrorText>
          </ErrorContainer>
        }
        <FormFooter>
          <FormFooterLeft>
            <CheckBox
              label={commentAnonymousLabel}
              checkedImage={CheckboxChecked}
              uncheckedImage={CheckboxUnchecked}
              labelStyle={checkboxStyles.labelStyle}
              onChange={checked => onCheckAnonymous(checked)}
            />
          </FormFooterLeft>
          <FormFooterRight>
            <Button
              label={commentSendLabel}
              accessibilityLabel={commentSendLabel}
              onPress={() => this.handlePress()}
              loading={loading}
            />
          </FormFooterRight>
        </FormFooter>
      </AddCommentContainer>
    );
  }
}

export default AddComment;
