import { StyleSheet } from 'react-native';
import styled, { css } from 'styled-components';
import { width, height } from 'react-native-dimension';
import { white, greyishBrown } from '../../../styles/colors';
import { OpenSansRegular } from '../../../styles/fonts';

export const AddCommentContainer = styled.ScrollView`
  display: flex;
  width: ${width(100)};
  height: ${height(65)};
  padding-left: ${width(8)};
  padding-right: ${width(8)};
  padding-top: 32;
  background-color: ${white};
  ${props => props.focused && css`
    height: auto;
  `}
`;

export const CommentInput = styled.TextInput`
  height: ${height(35)};
  padding-bottom: 20;
`;

export const FormFooter = styled.View`
  display: flex;
  width: ${width(84)};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 340;
`;

export const FormFooterLeft = styled.View`
  flex: .6;
`;

export const FormFooterRight = styled.View`
  flex: .4;
`;

export const ErrorContainer = styled.View`
  padding-top: 10;
  padding-bottom: 10;
`;

export const ErrorText = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 12;
  color: red;
`;

export const checkboxStyles = StyleSheet.create({
  labelStyle: {
    fontFamily: OpenSansRegular,
    fontSize: 13,
    color: greyishBrown,
  },
});
