import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { mediumGray } from '../../../styles/colors';
import { OpenSansRegular, OpenSansBold } from '../../../styles/fonts';

export const UniversityGreetingContainer = styled.View`
  width: ${width(100)};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
  margin-top: 60;
`;

export const TextContainer = styled.View`
  width: 176;
  margin-left: ${width(10)};
  display: flex;
  flex-direction: column;
`;

export const SmallText = styled.Text`
  font-size: 14;
  line-height: 30;
  font-family: ${OpenSansRegular};
  color: ${mediumGray};
  width: 176;
`;

export const TitleBig = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 20;
  width: 176;
`;

export const UniversityLogo = styled.Image`
  width: 63;
  border-radius: 30;
  margin-right: ${width(10)};
`;
