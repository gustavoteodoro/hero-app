/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  UniversityGreetingContainer,
  SmallText,
  TitleBig,
  UniversityLogo,
  TextContainer
} from './styles';

class UniversityGreeting extends Component {
  static propTypes = {
    universityName: PropTypes.string,
    universityLogo: PropTypes.string,
    pageLink: PropTypes.string,
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
  }

  static defaultProps = {
    universityName: '',
    universityLogo: '',
    navigation: null,
  }

  render(){
    const {
      universityName,
      universityLogo
    } = this.props;

    return(
      <UniversityGreetingContainer>
        <TextContainer>
          <SmallText>welcome to </SmallText>
          <TitleBig>{universityName}</TitleBig>
        </TextContainer>
        {universityLogo ?
          <UniversityLogo style={{width: 63, height: 63}} source={{uri: universityLogo}} />
          : null
        }
      </UniversityGreetingContainer>
    )
  }
}

export default UniversityGreeting;
