import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native';
import {
  UserFormContainer,
  StatusContainer,
} from './styles';
import Input from '../../elements/Input';
import Button from '../../elements/Button';
import { setUser } from '../../../actions/user';
import { changePassword } from '../../../services/account';
import { checkRequired } from '../../../utils/validations/required';

class PasswordUpdateForm extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    setUser: PropTypes.func,
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
  }

  static defaultProps = {
    navigation: null,
    setUser: null,
    user: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      password: null,
      error: null,
      loading: false,
    };

    this.sendData = this.sendData.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
  }

  sendData = (token, password) => {
    this.setState({
      loading: true,
    });
    changePassword(token, password)
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          this.props.navigation.navigate('Profile');
        } else {
          this.setState({
            error: result.message ? result.message : 'Failed to connect to server.',
            loading: false,
          });
        }
      });
  }

  handleSubmit = () => {
    const { token } = this.props.user;
    if (this.state.password) {
      const { password } = this.state;
      const error = checkRequired(password);
      this.setState({
        error,
      });
      if (!error) {
        this.sendData(token, password);
      }
    } else {
      this.setState({
        error: 'You need to fill the form.',
      });
    }
  }

  handleChangeName(password) {
    this.setState({
      password,
    });
  }

  render() {
    const {
      error,
      loading,
    } = this.state;

    return (
      <UserFormContainer>
        <Input
          name="name"
          error={error}
          label="New password"
          secureTextEntry
          autoCapitalize="none"
          onChangeText={e => this.handleChangeName(e)}
        />
        <StatusContainer>
          {loading &&
            (<ActivityIndicator size="large" color="#50e3c2" />)
          }
        </StatusContainer>
        <Button
          label="Save"
          accessibilityLabel="Save"
          onPress={() => this.handleSubmit()}
        />
      </UserFormContainer>
    );
  }
}

const mapStateToProps = state => ({
  formReducer: state.formReducer,
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  setUser(userData) {
    dispatch(setUser(userData));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PasswordUpdateForm);
