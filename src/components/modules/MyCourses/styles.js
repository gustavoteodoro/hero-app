import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { white, borderGrey } from '../../../styles/colors';
import { OpenSansRegular, OpenSansBold } from '../../../styles/fonts';

export const TextContainer = styled.View`
  width: ${width(80)};
  display: flex;
  flex-direction: column;
  margin-top: 100;
`;

export const TextRegular = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 14;
  text-align: center;
  width: ${width(60)};
  margin-right: 20;
`;

export const TextBold = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
  max-width: 200px;
`;

export const IconRemove = styled.Image`
  display: flex;
  width: 10;
  height: 10;
`;

export const ButtonRemove = styled.TouchableOpacity`
  width: 30;
  height: 30;
  z-index: 999;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const AddedCoursesContainer = styled.View`
  background-color: ${white}
  width: ${width(100)};
  padding-top: 20;
  padding-bottom: 20;
  padding-left: 30;
  padding-right: 30;
  margin-bottom: 20;
`;

export const TitleDepartment = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 11;
`;
export const TitleCourse = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 11;
`;

export const TitleContainer = styled.View`
  display: flex;
  justify-content: flex-start;
`;

export const TextContent = styled.View`
  display: flex;
  justify-content: flex-start;
`;

export const BreakLine = styled.View`
  width: 42px;
  height: 1px;
  border-bottom-width: 1px;
  border-bottom-color: ${borderGrey};
  margin-top: 16;
  margin-bottom: 12;
`;

export const CourseContainer = styled.View`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
`;

export const DeparmentContainer = styled.View`

`;

export const CourseTextContainer = styled.View`

`;

export const CourseWrapper = styled.View`

`;
