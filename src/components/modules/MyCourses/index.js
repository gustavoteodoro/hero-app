
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ButtonRemove from '../../elements/ButtonRemove';
import {
  AddedCoursesContainer,
  CourseContainer,
  TitleCourse,
  TextBold,
  TitleDepartment,
  TitleContainer,
  BreakLine,
  DeparmentContainer,
  CourseWrapper,
  CourseTextContainer,
} from './styles';

class MyCourses extends Component {
  static propTypes = {
    departments: PropTypes.arrayOf(PropTypes.object),
    onUnfollow: PropTypes.func,
  }

  static defaultProps = {
    departments: [],
    onUnfollow: null,
  }

  render() {
    const {
      departments,
      onUnfollow,
    } = this.props;

    return (
      <AddedCoursesContainer>
        {departments.map(department => (
          <DeparmentContainer key={department.ancestors[0].uuid}>
            <TitleContainer>
              <TitleDepartment>
                Courses of
              </TitleDepartment>
              <TextBold>{department.ancestors[0].attributes.name}</TextBold>
              <BreakLine />
            </TitleContainer>
            {department.children.map(course => (
              <CourseWrapper key={course.uuid}>
                <CourseContainer>
                  <CourseTextContainer>
                    <TitleCourse>
                      Course
                    </TitleCourse>
                    <TextBold>{course.attributes.name}</TextBold>
                  </CourseTextContainer>
                  <ButtonRemove onPress={() => onUnfollow(course.uuid)} />
                </CourseContainer>
                <BreakLine />
              </CourseWrapper>
            ))}
          </DeparmentContainer>
        ))
        }

      </AddedCoursesContainer>
    );
  }
}

export default MyCourses;
