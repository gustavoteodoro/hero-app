/* eslint-disable */
import React, { Component } from 'react';
import { TitleBarContainer, TitleSmallContainer } from './styles';
import PropTypes from 'prop-types';
import TitleSmall from '../../elements/TitleSmall';
import BackArrowButton from '../../elements/BackArrowButton';

class TitleBar extends Component {
  static propTypes = {
    pageTitle: PropTypes.string,
    pageLink: PropTypes.string,
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
  }

  static defaultProps = {
    pageTitle: '',
    pageLink: '',
    navigation: null,
  }

  render(){
    const {
      pageTitle,
      pageLink,
      navigation,
    } = this.props;
    return(
      <TitleBarContainer>
        <BackArrowButton {...this.props} page={pageLink} />
        <TitleSmallContainer>
          <TitleSmall titleText={pageTitle} />
        </TitleSmallContainer>
      </TitleBarContainer>
    )
  }
}

export default TitleBar;
