import styled from 'styled-components';
import { width } from 'react-native-dimension';

export const TitleBarContainer = styled.View`
  margin-top: 30;
  height: 30;
  width: ${width(100)};
`;

export const TitleSmallContainer = styled.View`
  margin-top: 6;
`;
