import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { lightGray } from '../../../styles/colors';

export const AddCommentHeaderContainer = styled.View`
  display: flex;
  width: ${width(100)};
  background: ${lightGray};
  box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.5);
  padding-bottom: 15;
  z-index: 20;
`;

