import React from 'react';
import TitleBar from '../TitleBar';
import { AddCommentHeaderContainer } from './styles';

const AddCommentHeader = props => (
  <AddCommentHeaderContainer>
    <TitleBar {...props} pageTitle={props.title} />
  </AddCommentHeaderContainer>
);

export default AddCommentHeader;
