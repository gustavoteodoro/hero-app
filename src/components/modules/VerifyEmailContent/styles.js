import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { aquaMarine, black, brownishGrey } from '../../../styles/colors';
import { NeuzeitBook, OpenSansBold, OpenSansRegular } from '../../../styles/fonts';

export const VerifyEmailContainer = styled.View`
  align-items: center;
  margin-top: 0;
`;

export const VerifyEmailInstructions = styled.View`
  width: ${width(80)};
  margin-left: ${width(10)};
  margin-right: ${width(10)};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const VerifyEmailTitle = styled.Text`
  width: ${width(70)};
  margin-left: ${width(15)};
  margin-right: ${width(15)};
  align-self: center;
  font-family: ${NeuzeitBook};
  font-size: 21;
  line-height: 23;
  text-align: center;
  margin-top: 25;
`;

export const VerifyEmailIcon = styled.View`
  width: ${width(17)};
  height: ${width(17)};
  align-items: center;
  justify-content: center;
  border-color: ${aquaMarine};
  border-width: 4;
  border-radius: ${width(50)};
`;

export const VerifyEmailAddress = styled.Text`
  width: ${width(80)};
  margin-left: ${width(10)};
  margin-right: ${width(10)};
  margin-top: 15;
  text-align: center;
  font-family: ${OpenSansBold};
  font-size: 15;
  color: ${black};
`;

export const VerifyEmailHelpText = styled.Text`
  width: ${width(80)};
  margin-left: ${width(10)};
  margin-right: ${width(10)};
  text-align: center;
  font-family: ${OpenSansRegular};
  font-size: 12;
  line-height: 20;
  margin-top: 15;
  color: ${brownishGrey};
`;

export const VerifyEmailHelpAlert = styled.Text`
  width: ${width(50)};
  margin-left: ${width(25)};
  margin-right: ${width(25)};
  text-align: center;
  font-family: ${OpenSansRegular};
  font-size: 12;
  line-height: 20;
  margin-top: 25;
  color: ${brownishGrey};
`;
