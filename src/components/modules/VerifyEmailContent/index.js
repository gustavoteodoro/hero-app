import React from 'react';
/* eslint-disable-next-line */
import { MaterialIcons } from '@expo/vector-icons';
import {
  VerifyEmailContainer,
  VerifyEmailTitle,
  VerifyEmailIcon,
  VerifyEmailAddress,
  VerifyEmailHelpAlert,
} from './styles';

const VerifyEmailContent = props => (
  <VerifyEmailContainer>
    <VerifyEmailIcon>
      <MaterialIcons name="email" size={25} color="black" />
    </VerifyEmailIcon>
    <VerifyEmailTitle>
      {'Almost there - let\'s just verify your email.'}
    </VerifyEmailTitle>
    <VerifyEmailAddress>
      {/* eslint-disable-next-line */}
      {props.email}
    </VerifyEmailAddress>
    <VerifyEmailHelpAlert>
      Click the link within 72 hours to activate your account.
    </VerifyEmailHelpAlert>
  </VerifyEmailContainer>
);

export default VerifyEmailContent;
