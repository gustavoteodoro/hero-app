import React, { Component } from 'react';
import { Animated } from 'react-native';
import PropTypes from 'prop-types';
import FeedItem from '../../elements/FeedItem';
import { getDiscussion } from '../../../services/discussions';

import { ThreadHeaderContainer } from './styles';

class ThreadHeader extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    minified: PropTypes.bool,
    /* eslint-disable-next-line */
    sugestion: PropTypes.object,
    pageLink: PropTypes.string,
    user: PropTypes.shape({
      token: PropTypes.string,
    }),
    update: PropTypes.func,
    onUpdate: PropTypes.func,
  }

  static defaultProps = {
    navigation: null,
    minified: null,
    sugestion: null,
    pageLink: '',
    user: null,
    update: null,
    onUpdate: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      fadeIn: new Animated.Value(1),
      slideHeight: new Animated.Value(37),
    };

    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate() {
    const {
      update,
      sugestion,
      user,
      onUpdate,
    } = this.props;
    getDiscussion(user.token, sugestion.uuid)
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          onUpdate(result.data);
        }
      });
    update();
  }

  render() {
    const {
      minified,
      sugestion,
      user,
    } = this.props;

    const {
      slideHeight,
      fadeIn,
    } = this.state;

    Animated.timing(
      this.state.slideHeight,
      {
        toValue: minified ? 0 : 600,
        duration: 600,
      },
    ).start();

    Animated.timing(
      this.state.fadeIn,
      {
        toValue: minified ? 0 : 1,
        duration: 300,
      },
    ).start();

    return (
      <ThreadHeaderContainer minified={minified}>
        <Animated.View
          style={{
              overflow: 'hidden',
              maxHeight: slideHeight,
              opacity: fadeIn,
            }}
        >
          <FeedItem
            sugestion={sugestion}
            itemId={sugestion.uuid}
            token={user.token}
            liked={sugestion.liked}
            update={() => this.handleUpdate()}
            onClickComment={() => this.props.navigation.navigate('AddComment')}
            isHeader
            limit={150}
            enableSeeMore
          />
        </Animated.View>
      </ThreadHeaderContainer>
    );
  }
}

export default ThreadHeader;
