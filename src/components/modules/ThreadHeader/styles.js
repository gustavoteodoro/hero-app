import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { lightGray } from '../../../styles/colors';

export const ThreadHeaderContainer = styled.View`
  display: flex;
  width: ${width(100)};
  background: ${lightGray};
  box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.5);
  ${props => props.minified && css`
    padding-bottom: 15;
  `}
`;

