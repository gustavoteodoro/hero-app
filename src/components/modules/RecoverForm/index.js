import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator, Keyboard } from 'react-native';
import PropTypes from 'prop-types';
import {
  RecoverFormContainer,
  StatusContainer,
} from './styles';
import Input from '../../elements/Input';
import Button from '../../elements/Button';
import { recoverPassword } from '../../../services/account';
import { setEmail } from '../../../actions/authData';
import { checkValidEmail } from '../../../utils/validations/email';

class RecoverForm extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    setEmail: PropTypes.func,
    authdata: PropTypes.shape({
      email: PropTypes.string,
    }),
  }

  static defaultProps = {
    navigation: null,
    setEmail: null,
    authdata: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      email: null,
      error: null,
      loading: false,
    };

    this.sendData = this.sendData.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
  }

  componentWillMount() {
    const {
      authdata,
    } = this.props;

    if (authdata) {
      this.setState({
        email: authdata.email,
      });
    }
  }

  sendData = (email) => {
    recoverPassword(email)
      .then(res => res.json())
      .then((result) => {
        if (result.status) {
          this.props.navigation.navigate('VerifyEmailPassword', { email });
        } else {
          this.setState({
            error: result.error ? result.error.message : 'Failed to connect to server.',
          });
        }
      });
  }

  handleSubmit = () => {
    Keyboard.dismiss();
    if (this.state.email) {
      const { email } = this.state;
      const error = checkValidEmail(email);
      this.setState({
        error,
      });
      if (!error) {
        this.props.setEmail({ email });
        this.sendData(email);
      }
    } else {
      this.setState({
        error: 'You need to fill the form.',
      });
    }
  }

  handleChangeEmail(email) {
    this.setState({
      email,
    });
  }

  render() {
    const {
      email,
      error,
      loading,
    } = this.state;

    return (
      <RecoverFormContainer>
        <Input
          name="email"
          error={error}
          value={email}
          label="Type in your email"
          keyboardType="email-address"
          autoCapitalize="none"
          onChangeText={e => this.handleChangeEmail(e)}
        />
        <StatusContainer>
          {loading &&
            (<ActivityIndicator size="large" color="#50e3c2" />)
          }
        </StatusContainer>
        <Button
          label="Send"
          accessibilityLabel="Send"
          onPress={() => this.handleSubmit()}
        />
      </RecoverFormContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  authdata: state.authdata,
});

const mapDispatchToProps = dispatch => ({
  setEmail(emailData) {
    dispatch(setEmail(emailData));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(RecoverForm);
