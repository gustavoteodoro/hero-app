import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { HeroIcon } from '../../../styles/fonts';
/* eslint-disable-next-line */
import { FontAwesome } from '@expo/vector-icons';

import {
  ProfileMenuContainer,
  ProfileMenuButton,
  ProfileMenuItem,
  ProfileMenuItemLabel,
  ProfileMenuItemText,
  ProfileMenuContent,
} from './styles';

class ProfileMenu extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    items: PropTypes.objectOf(PropTypes.string),
    name: PropTypes.string,
    email: PropTypes.string,
    onLogout: PropTypes.func,
  }

  static defaultProps = {
    navigation: null,
    items: null,
    name: null,
    email: null,
    onLogout: null,
  }

  render() {
    const {
      items,
      name,
      email,
      onLogout,
      notifications,
    } = this.props;

    return (
      <ProfileMenuContainer>
        <ProfileMenuButton onPress={() => this.props.navigation.navigate('UpdateUserName')}>
          <ProfileMenuItem>
            <ProfileMenuContent>
              <ProfileMenuItemLabel>{items.name}</ProfileMenuItemLabel>
              <ProfileMenuItemText black>{name}</ProfileMenuItemText>
            </ProfileMenuContent>
            <HeroIcon name="next" size={8} color="rgb(80,227,194)" />
          </ProfileMenuItem>
        </ProfileMenuButton>
        <ProfileMenuButton>
          <ProfileMenuItem>
            <ProfileMenuContent>
              <ProfileMenuItemLabel>{items.email}</ProfileMenuItemLabel>
              <ProfileMenuItemText black>{email}</ProfileMenuItemText>
            </ProfileMenuContent>
            <FontAwesome name="lock" size={16} color="rgb(247,167,0)" />
          </ProfileMenuItem>
        </ProfileMenuButton>
        <ProfileMenuButton onPress={() => this.props.navigation.navigate('UpdatePassword')}>
          <ProfileMenuItem>
            <ProfileMenuContent>
              <ProfileMenuItemText>{items.password}</ProfileMenuItemText>
            </ProfileMenuContent>
            <HeroIcon name="next" size={8} color="rgb(80,227,194)" />
          </ProfileMenuItem>
        </ProfileMenuButton>
        <ProfileMenuButton onPress={() => this.props.navigation.navigate('EditCourses')}>
          <ProfileMenuItem>
            <ProfileMenuContent>
              <ProfileMenuItemText>{items.courses}</ProfileMenuItemText>
            </ProfileMenuContent>
            <HeroIcon name="next" size={8} color="rgb(80,227,194)" />
          </ProfileMenuItem>
        </ProfileMenuButton>
        <ProfileMenuButton onPress={() => this.props.navigation.navigate('NotificationsSettings')}>
          <ProfileMenuItem>
            <ProfileMenuContent>
              <ProfileMenuItemText>{items.notifications}</ProfileMenuItemText>
            </ProfileMenuContent>
            <HeroIcon name="next" size={8} color="rgb(80,227,194)" />
          </ProfileMenuItem>
        </ProfileMenuButton>
        <ProfileMenuButton onPress={() => this.props.navigation.navigate('ProfileTermsAndConditions')}>
          <ProfileMenuItem>
            <ProfileMenuContent>
              <ProfileMenuItemText>{items.terms}</ProfileMenuItemText>
            </ProfileMenuContent>
            <HeroIcon name="next" size={8} color="rgb(80,227,194)" />
          </ProfileMenuItem>
        </ProfileMenuButton>
        <ProfileMenuButton onPress={() => onLogout()}>
          <ProfileMenuItem>
            <ProfileMenuContent>
              <ProfileMenuItemText>{items.logout}</ProfileMenuItemText>
            </ProfileMenuContent>
          </ProfileMenuItem>
        </ProfileMenuButton>
        <ProfileMenuButton onPress={() => this.props.navigation.navigate('DeleteProfileConfirm')}>
          <ProfileMenuItem>
            <ProfileMenuItemText delete>{items.delete}</ProfileMenuItemText>
          </ProfileMenuItem>
        </ProfileMenuButton>
      </ProfileMenuContainer>
    );
  }
}

export default ProfileMenu;
