import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { black, whiteFour, greyishBrown, orange } from '../../../styles/colors';
import { OpenSansRegular, OpenSansBold } from '../../../styles/fonts';

export const ProfileMenuContainer = styled.ScrollView`
  display: flex;
  width: ${width(100)};
`;

export const ProfileMenuButton = styled.TouchableWithoutFeedback`
`;

export const ProfileMenuContent = styled.View`

`;

export const ProfileMenuItem = styled.View`
  height: 85;
  flex-direction: row;
  padding-left: ${width(8)};
  padding-right: ${width(8)};
  justify-content: space-between;
  align-items: center;
  border-bottom-color: ${whiteFour};
  border-bottom-width: 1;
`;

export const ProfileMenuItemLabel = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 11;
  color: ${black};
`;

export const ProfileMenuItemText = styled.Text`
  font-family: ${OpenSansBold};
  font-size: 14;
  color: ${greyishBrown};
  ${props => props.black && css`
    color: ${black};
  `}
  ${props => props.delete && css`
    color: ${orange};
  `}
`;
