import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { warmGrey, lightGray } from '../../../styles/colors';
import { OpenSansRegular } from '../../../styles/fonts';

export const SignupFormContainer = styled.View`
  display: flex;
  width: ${width(84)};
  margin-left: ${width(8)};
  margin-top: 32;
  background-color: ${lightGray};
`;

export const FormError = styled.Text`
  font-family: ${OpenSansRegular};
  font-size: 14;
  color: ${warmGrey};
  width: ${width(60)};
  margin-left: ${width(5)};
`;

export const FormErrorIcon = styled.Image`
  display: flex;
  width: ${width(2.5)};
  height: ${height(5)};
  margin-left: ${width(1)};
  margin-top: 2;
`;

export const FormErrorContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const TextLinkContainer = styled.View`
  margin-top: 15;
`;

export const StatusContainer = styled.View`
  margin-top: 10;
  min-height: ${height(5)};
  margin-bottom: 15;
`;
