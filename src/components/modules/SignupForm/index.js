import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator, Keyboard } from 'react-native';
import PropTypes from 'prop-types';
import {
  SignupFormContainer,
  TextLinkContainer,
  StatusContainer,
} from './styles';
import Input from '../../elements/Input';
import Button from '../../elements/Button';
import TextLink from '../../elements/TextLink';
import { setUser } from '../../../actions/user';
import { setEmail } from '../../../actions/authData';
import { createAccount } from '../../../services/account';
import { checkValidPassword } from '../../../utils/validations/password';
import { checkValidEmail } from '../../../utils/validations/email';

class SignupForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }),
    setUser: PropTypes.func,
    setEmail: PropTypes.func,
    authdata: PropTypes.shape({
      email: PropTypes.string,
    }),
  }

  static defaultProps = {
    onSubmit: null,
    navigation: null,
    setUser: null,
    setEmail: null,
    authdata: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: null,
      errorEmail: null,
      errorPassword: null,
      loading: null,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.sendData = this.sendData.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }

  componentWillMount() {
    const {
      authdata,
    } = this.props;

    if (authdata) {
      this.setState({
        email: authdata.email ? authdata.email : '',
      });
    }
  }

  sendData = (email, password) => {
    this.setState({
      loading: true,
    });
    createAccount(email, password)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.props.setUser({ email: result.data.user.email, verified: false, token: null });
          this.props.navigation.navigate('VerifyEmail');
        } else if (result.error) {
          this.setState({
            error: result.error.message ? result.error.message : 'Failed to connect to server.',
            loading: false,
          });
        } else {
          this.setState({
            error: result.message ? result.message : 'Failed to connect to server.',
            loading: false,
          });
        }
      });
  }

  handleSubmit = () => {
    Keyboard.dismiss();
    if (this.state.email) {
      const { email, password } = this.state;
      const errorEmail = checkValidEmail(email);
      const errorPassword = checkValidPassword(password);
      this.setState({
        errorEmail,
        errorPassword,
      });
      if (!errorEmail && !errorPassword) {
        this.props.setEmail({ email });
        this.sendData(email, password);
      }
    } else {
      this.setState({
        error: 'You need to fill the form.',
      });
    }
  }

  handleChangeEmail(email) {
    this.setState({
      email,
    });
  }

  handleChangePassword(password) {
    this.setState({
      password,
    });
  }

  render() {
    const {
      email,
      password,
      error,
      errorEmail,
      errorPassword,
      loading,
    } = this.state;

    return (
      <SignupFormContainer>
        <Input
          name="email"
          error={error || errorEmail}
          value={email}
          label="Your institutional email"
          keyboardType="email-address"
          autoCapitalize="none"
          onChangeText={e => this.handleChangeEmail(e)}
        />
        <Input
          name="password"
          error={errorPassword}
          value={password}
          label="Choose password"
          secureTextEntry
          autoCapitalize="none"
          onChangeText={e => this.handleChangePassword(e)}
        />
        <StatusContainer>
          {loading &&
            (<ActivityIndicator size="large" color="#50e3c2" />)
          }
        </StatusContainer>
        <Button
          label="Create profile"
          accessibilityLabel="Create profile"
          onPress={() => this.handleSubmit()}
        />
        <TextLinkContainer>
          <TextLink {...this.props} text="Already signed up? Login here." page="Login" />
        </TextLinkContainer>
      </SignupFormContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  authdata: state.authdata,
});

const mapDispatchToProps = dispatch => ({
  setUser(userData) {
    dispatch(setUser(userData));
  },
  setEmail(emailData) {
    dispatch(setEmail(emailData));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SignupForm);
