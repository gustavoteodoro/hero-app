import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { NeuzeitBook, NeuzeitBookHeavy } from '../../../styles/fonts';
import { tealish } from '../../../styles/colors';

export const FeedHeaderContainer = styled.View`
  display: flex;
  width: ${width(100)};
`;

export const FeedHeaderCategory = styled.View`
  display: flex;
  width: ${width(100)};
  flex-direction: row;
  justify-content: space-around;
  background: rgb(239,233,225);
  padding-top: 35;
  padding-bottom: 24;
`;

export const CategoryText = styled.Text`
  text-align: center;
  font-family: ${NeuzeitBook};
  font-size: 15;
  ${props => props.active && css`
    font-family: ${NeuzeitBookHeavy};
    color: ${tealish};
  `}
`;

export const CategoryItem = styled.View`
  display: flex;
  position: relative;
  width: ${width(33.333333333333333)};
  align-items: center;
  border-right-width: 1;
  border-style: solid;
  border-right-color: rgb(151,151,151);
`;

export const CategoryItemTouch = styled.TouchableWithoutFeedback`
  
`;

export const CategoryItemPointer = styled.View`
  position: absolute;
  bottom: -31;
  width: 15;
  height: 15;
  transform: rotate(45deg);
  background: white;
`;

export const FeedHeaderFilter = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom-width: 4;
  border-bottom-color: rgb(231,223,223);
`;

export const FilterContent = styled.View`
  display: flex;
  flex-direction: row;
  margin-left: 20;
  margin-right: 10;
`;

export const FilterItem = styled.TouchableWithoutFeedback`
  display: flex;
`;

export const FilterText = styled.Text`
  font-family: ${NeuzeitBookHeavy};
  font-size: 12;
  padding-left: 10;
  padding-right: 10;
  padding-top: 15;
  padding-bottom: 13;

  ${props => props.active && css`
    color: ${tealish};
  `}
`;
