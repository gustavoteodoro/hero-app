import React, { Component } from 'react';
import { Animated } from 'react-native';
import { PropTypes } from 'prop-types';
import { HeroIcon } from '../../../styles/fonts';
import {
  FeedHeaderContainer,
  FeedHeaderCategory,
  CategoryItem,
  CategoryText,
  CategoryItemTouch,
  CategoryItemPointer,
  FeedHeaderFilter,
  FilterContent,
  FilterItem,
  FilterText,
} from './styles';

import { categories } from './data.json';

class FeedHeader extends Component {
  static propTypes = {
    minified: PropTypes.bool,
    category: PropTypes.string,
    isFeed: PropTypes.bool,
    isFire: PropTypes.bool,
    onChangeCategory: PropTypes.func,
    onChangeVisualization: PropTypes.func,
    onChangeSort: PropTypes.func,
  }

  static defaultProps = {
    minified: false,
    category: 'courses',
    isFeed: true,
    isFire: false,
    onChangeCategory: null,
    onChangeVisualization: null,
    onChangeSort: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      fadeIn: new Animated.Value(1),
      slideHeight: new Animated.Value(37),
    };
  }

  render() {
    const {
      minified,
      category,
      isFeed,
      isFire,
      onChangeCategory,
      onChangeVisualization,
      onChangeSort,
    } = this.props;

    const {
      slideHeight,
      fadeIn,
    } = this.state;

    Animated.timing(
      this.state.slideHeight,
      {
        toValue: minified ? 0 : 37,
        duration: 200,
      },
    ).start();

    Animated.timing(
      this.state.fadeIn,
      {
        toValue: minified ? 0 : 1,
        duration: 100,
      },
    ).start();

    return (
      <FeedHeaderContainer>
        <FeedHeaderCategory>
          {
            categories.map(item => (
              <CategoryItemTouch onPress={() => onChangeCategory(item.name)} key={item.name}>
                <CategoryItem active={category === item.name}>
                  <Animated.View
                    style={{
                        marginBottom: 8,
                        justifyContent: 'center',
                        alignItems: 'center',
                        overflow: 'hidden',
                        height: slideHeight,
                        opacity: fadeIn,
                      }}
                  >
                    <HeroIcon
                      name={item.icon}
                      size={30}
                      color={(category === item.name) ? 'rgb(58,210,176)' : 'rgb(74,74,74)'}
                    />
                  </Animated.View>
                  <CategoryText active={category === item.name}>{item.name.replace(/\b\w/g, l => l.toUpperCase())}</CategoryText>
                  {category === item.name &&
                    (<CategoryItemPointer />)
                  }
                </CategoryItem>
              </CategoryItemTouch>
            ))
          }
        </FeedHeaderCategory>
        <FeedHeaderFilter>
          <FilterContent>
            <FilterItem onPress={() => onChangeVisualization(false)}>
              <FilterText active={!isFeed}>
                List
              </FilterText>
            </FilterItem>
            <FilterItem onPress={() => onChangeVisualization(true)}>
              <FilterText active={isFeed}>
                Feed
              </FilterText>
            </FilterItem>
          </FilterContent>
          {isFeed &&
            <FilterContent>
              <FilterItem onPress={() => onChangeSort(true)}>
                <FilterText>
                  <HeroIcon name="fire" size={15} color={(isFire) ? 'rgb(58,210,176)' : 'rgb(74,74,74)'} />
                </FilterText>
              </FilterItem>
              <FilterItem onPress={() => onChangeSort(false)}>
                <FilterText>
                  <HeroIcon name="clock" size={15} color={(!isFire) ? 'rgb(58,210,176)' : 'rgb(74,74,74)'} />
                </FilterText>
              </FilterItem>
            </FilterContent>
          }
        </FeedHeaderFilter>
      </FeedHeaderContainer>
    );
  }
}

export default FeedHeader;
