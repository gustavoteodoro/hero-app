import { createStore } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';
import autoMergeLevel1 from 'redux-persist/lib/stateReconciler/autoMergeLevel1';

import authdata from '../reducers/authdata';
import user from '../reducers/user';
import courses from '../reducers/courses';
import services from '../reducers/services';
import campus from '../reducers/campus';
import category from '../reducers/category';
import discussions from '../reducers/discussions';
import currentDiscussion from '../reducers/currentDiscussion';
import currentUnit from '../reducers/currentUnit';

const config = {
  key: 'HeroApp',
  storage,
  stateReconciler: autoMergeLevel1,
  blacklist: ['category'],
};

const reducer = persistCombineReducers(config, {
  user,
  courses,
  services,
  campus,
  category,
  discussions,
  currentDiscussion,
  currentUnit,
  authdata,
});

export const store = createStore(reducer);
export const persistor = persistStore(store);
