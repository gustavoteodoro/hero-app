import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { StatusBar } from 'react-native';
import { Font } from 'expo';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistor, store } from './store';
import { GoogleAnalyticsTracker } from './utils/googleAnalytics';
import Loading from './components/pages/Loading';
import MainNavigator from './components/pages/MainNavigator';
import OpenSansRegular from './assets/fonts/OpenSans-Regular.ttf';
import OpenSansBold from './assets/fonts/OpenSans-Bold.ttf';
import NeuzeitBook from './assets/fonts/NeuzeitSLTStd-Book.otf';
import NeuzeitBookHeavy from './assets/fonts/NeuzeitSLTStd-BookHeavy.otf';
import HeroIconsFont from './assets/fonts/heroicons.ttf';

/* eslint-disable-next-line */
class App extends Component {

  state = {
    fontLoaded: false,
  };

  async componentWillMount() {
    await Font.loadAsync({
      'open-sans-regular': OpenSansRegular,
      'open-sans-bold': OpenSansBold,
      'neuzeit-book': NeuzeitBook,
      'neuzeit-book-heavy': NeuzeitBookHeavy,
      'hero-icons': HeroIconsFont,
    });

    this.setState({ fontLoaded: true });
  }

  getCurrentRouteName = (navigationState) => {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
      return this.getCurrentRouteName(route);
    }
    return route.routeName;
  }

  render() {
    const tracker = new GoogleAnalyticsTracker('UA-107124352-3');

    return (
      <Provider store={store}>
        <PersistGate loading={<Loading />} persistor={persistor}>
          <StatusBar
            barStyle="dark-content"
          />
          {this.state.fontLoaded ? (
            <MainNavigator
              onNavigationStateChange={(prevState, currentState) => {
                const currentScreen = this.getCurrentRouteName(currentState);
                const prevScreen = this.getCurrentRouteName(prevState);

                if (prevScreen !== currentScreen) {
                  tracker.trackScreenView(currentScreen);
                }
              }}
            />
          ) : null}
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
