export const UPDATE_DISCUSSIONS = 'DISCUSSIONS/UPDATE_DISCUSSIONS';

export function updateDiscussions(discussions) {
  return { type: UPDATE_DISCUSSIONS, discussions };
}
