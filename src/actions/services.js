export const UPDATE_SERVICES = 'DISCUSSIONS/UPDATE_SERVICES';

export function updateServices(services) {
  return { type: UPDATE_SERVICES, services };
}
