export const UPDATE_COURSES = 'DISCUSSIONS/UPDATE_COURSES';

export function updateCourses(courses) {
  return { type: UPDATE_COURSES, courses };
}
