export const UPDATE_DISCUSSION = 'DISCUSSIONS/UPDATE_DISCUSSION';

export function updateCurrentDiscussion(discussion) {
  return { type: UPDATE_DISCUSSION, discussion };
}
