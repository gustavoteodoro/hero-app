export const SET_EMAIL = 'USER/SET_EMAIL';
export const CLEAR_EMAIL = 'USER/CLEAR_EMAIL';

export function setEmail(email) {
  return { type: SET_EMAIL, email };
}

export function clearEmail() {
  return { type: CLEAR_EMAIL };
}
