export const UPDATE_UNIT = 'DISCUSSIONS/UPDATE_UNIT';

export function updateCurrentUnit(unit) {
  return { type: UPDATE_UNIT, unit };
}
