export const UPDATE_CAMPUS = 'DISCUSSIONS/UPDATE_CAMPUS';

export function updateCampus(campus) {
  return { type: UPDATE_CAMPUS, campus };
}
