export const checkValidVerificationCode = (verificationCode) => {
  if (!verificationCode) {
    return 'The verification code field is required';
  }
};

