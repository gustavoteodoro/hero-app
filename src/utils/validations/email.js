/* eslint-disable */
const regularEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const checkValidEmail = (email) => {
  if (!email) {
    return "The email field is required";
  } else if (!regularEmail.test(email)) {
    return "The email address isn't a valid e-mail";
  }
}