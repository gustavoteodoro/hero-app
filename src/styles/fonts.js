/* eslint-disable-next-line */
import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import icoMoonConfig from '../assets/fonts/hero-icons-config.json';

export const HeroIcon = createIconSetFromIcoMoon(icoMoonConfig, 'hero-icons');
export const OpenSansRegular = 'open-sans-regular';
export const OpenSansBold = 'open-sans-bold';
export const NeuzeitBook = 'neuzeit-book';
export const NeuzeitBookHeavy = 'neuzeit-book-heavy';
