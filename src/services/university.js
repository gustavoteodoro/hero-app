import { API_URL } from '../utils/constants';

export const universityByDomain = (token, domain) => fetch(
  `${API_URL}/universities/${domain}`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);
