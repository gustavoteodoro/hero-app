import { API_URL } from '../utils/constants';

export const listComments = (token, discussionId) => fetch(
  `${API_URL}/discussions/${discussionId}/comments`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const createComment = (token, discussionId, comment, anonymous) => fetch(
  `${API_URL}/discussions/${discussionId}/comments`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'POST',
    body: JSON.stringify({
      attributes: {
        comment,
        anonymous,
      },
    }),
  },
);

export const likeComment = (token, discussionId, commentId) => fetch(
  `${API_URL}/discussions/${discussionId}/comments/${commentId}/like`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'POST',
  },
);

export const dislikeComment = (token, discussionId, commentId) => fetch(
  `${API_URL}/discussions/${discussionId}/comments/${commentId}/like`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'DELETE',
  },
);

export const deleteComment = (token, discussionId, commentId) => fetch(
  `${API_URL}/discussions/${discussionId}/comments/${commentId}`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'DELETE',
  },
);
