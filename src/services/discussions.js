import { API_URL } from '../utils/constants';

export const createDiscussion = (token, unit, text, anonymous) => fetch(
  `${API_URL}/discussions`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'POST',
    body: JSON.stringify({
      unit,
      attributes: {
        text,
        anonymous,
      },
    }),
  },
);

export const getDiscussion = (token, discussionUuid) => fetch(
  `${API_URL}/discussions/${discussionUuid}`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const deleteDiscussion = (token, unitId) => fetch(
  `${API_URL}/discussions/${unitId}`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'DELETE',
  },
);

export const ListSortedDiscussionsBySlot = (token, slot, sort) => fetch(
  `${API_URL}/discussions/${slot}/all/sort-by/${sort}`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const ListSortedDiscussionsByUnit = (token, unit, sort) => fetch(
  `${API_URL}/units/${unit}/discussions/all/sort-by/${sort}`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const likeDiscussion = (token, discussionId) => fetch(
  `${API_URL}/discussions/${discussionId}/like`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'POST',
  },
);

export const dislikeDiscussion = (token, discussionId) => fetch(
  `${API_URL}/discussions/${discussionId}/like`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'DELETE',
  },
);
