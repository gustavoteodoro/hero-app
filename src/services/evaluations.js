import { API_URL } from '../utils/constants';

export const createEvaluation = (token, unitId, aspects, value, comment) => fetch(
  `${API_URL}/evaluations`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'POST',
    body: JSON.stringify({
      units: [{
        uuid: unitId,
        comment,
        aspects,
        value,
      }],
    }),
  },
);
