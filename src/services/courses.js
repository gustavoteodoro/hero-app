import { API_URL } from '../utils/constants';

export const followCourse = (token, courses) => fetch(
  `${API_URL}/user/courses/follow`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'POST',
    body: JSON.stringify({
      courses,
    }),
  },
);

export const unfollowCourse = (token, courses) => fetch(
  `${API_URL}/user/courses/follow`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'DELETE',
    body: JSON.stringify({
      courses,
    }),
  },
);

export const getUnit = (token, unitUuid) => fetch(
  `${API_URL}/units/${unitUuid}`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const listCoursesFollowed = token => fetch(
  `${API_URL}/user/courses/followed`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const unitByDeeplink = (token, deepLink) => fetch(
  `${API_URL}/units`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
      deepLink,
    },
    method: 'GET',
  },
);

export const coursesByDepartment = (token, departmentUuid) => fetch(
  `${API_URL}/user/departments/${departmentUuid}/courses`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);
