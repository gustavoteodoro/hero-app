import { API_URL } from '../utils/constants';

export const allDepartments = token => fetch(
  `${API_URL}/user/departments`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const allCampusDepartments = token => fetch(
  `${API_URL}/user/campus/departments`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const allServicesDepartments = token => fetch(
  `${API_URL}/user/services/departments`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const followedDepartments = token => fetch(
  `${API_URL}/user/departments/followed`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const departmentCourses = (token, uuid) => fetch(
  `${API_URL}/user/departments/${uuid}/courses`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);
