import { API_URL } from '../utils/constants';

export const userProfile = token => fetch(
  `${API_URL}/user/profile`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const setUserName = (token, name) => fetch(
  `${API_URL}/user/name`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'PUT',
    body: JSON.stringify({
      name,
    }),
  },
);

export const userNotifications = token => fetch(
  `${API_URL}/user/notifications`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'GET',
  },
);

export const switchNotifications = token => fetch(
  `${API_URL}/user/enable-notifications`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'PUT',
  },
);
