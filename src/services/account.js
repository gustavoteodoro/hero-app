import { API_URL } from '../utils/constants';

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const createAccount = (email, password) => fetch(
  `${API_URL}/register`,
  {
    headers,
    method: 'POST',
    body: JSON.stringify({
      email,
      password,
    }),
  },
);

export const loginAccount = (email, password) => fetch(
  `${API_URL}/login`,
  {
    headers,
    method: 'POST',
    body: JSON.stringify({
      email,
      password,
    }),
  },
);

export const confirmAccount = (email, verificationCode) => fetch(
  `${API_URL}/register/token`,
  {
    headers,
    method: 'POST',
    body: JSON.stringify({
      email,
      token: verificationCode,
    }),
  },
);

export const recoverPassword = email => fetch(
  `${API_URL}/password/forgot`,
  {
    headers,
    method: 'POST',
    body: JSON.stringify({
      email,
    }),
  },
);

export const changePassword = (token, new_password) => fetch(
  `${API_URL}/user/password`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'PUT',
    body: JSON.stringify({
      new_password,
    }),
  },
);

export const deleteUser = token => fetch(
  `${API_URL}/user/delete`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    method: 'POST',
  },
);
