# Hero

## Requirements

- Node.js 6 + NPM
- Yarn
  `npm i -g yarn`
- Expo
  `npm i -g exp`
- [Watchman](https://facebook.github.io/watchman/docs/install.html)
- Expo Client
  - [AppStore](https://itunes.apple.com/app/apple-store/id982107779?ct=www&mt=8)
  - [PlayStore](https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www)

## Installing

`yarn`

## Running

`yarn start`

## Debugging

`yarn react-devtools`

## Building

Before Build, you need to increase version number at file `app.json`. Increase `version` and `versionCode`. 
> Example: If version is *1.0.56* and versionCode is *56*, change to `version: 1.0.57` and `versionCode: 57`.

### Android

- `exp build:android`
- Wait the build process and download the APK file
- Go to [Play Console](https://play.google.com/apps/publish)
- Version manager >  App versions > Closed alpha > Manager > Create version
- Upload APK File
- Insert version changes notes
- Review > Save
- Send to test
> You need to wait the app propagation to check at PlayStore on your phone.

### iOS

> Before first build, you need to configure your apple certificates. [More info here](https://docs.expo.io/versions/latest/distribution/building-standalone-apps)

`exp build:ios`
- Wait the build process and download the IPA file
- Go to XCODE
- Xcode > Open developer tools > Application Loader
- Select the IPA file and upload it
> Important: You need to wait the app processing. Between upload and avaiable at Testflight is almost 1 hour.
- Go to [AppStore connect](http://appstoreconnect.apple.com)
- My apps > Hero Feedback > Testflight > [Your version] > Missing compliance
- Answer the form
- Check your Testflight app